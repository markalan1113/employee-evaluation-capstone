﻿CREATE TABLE [dbo].[Employee]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [FirstName] NVARCHAR(256) NOT NULL, 
    [LastName] NVARCHAR(256) NOT NULL, 
    [Email] NVARCHAR(256) NOT NULL, 
    [MaillingAddress] NVARCHAR(256) NOT NULL, 
    [Phone] NCHAR(10) NOT NULL, 
    [CohortID] INT NULL
	CONSTRAINT [FK_dbo.Employee_dbo.Cohort_CohortID] FOREIGN KEY ([CohortID]) 
        REFERENCES [dbo].[Cohort] ([Id]) ON DELETE CASCADE, 
    [PasswordHash] NVARCHAR(MAX) NULL
)
