﻿CREATE TABLE [dbo].[Admin]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [Email] NVARCHAR(256) NOT NULL, 
    [PasswordHash] NVARCHAR(MAX) NOT NULL 
)
