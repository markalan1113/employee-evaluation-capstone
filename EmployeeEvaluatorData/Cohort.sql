﻿CREATE TABLE [dbo].[Cohort]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [Name] NVARCHAR(256) NOT NULL, 
    [Type] NVARCHAR(256) NOT NULL, 
    [Stage] NVARCHAR(256) NOT NULL, 
    [StartDate] DATETIME NOT NULL, 
    [EndDate] DATETIME NOT NULL
)
