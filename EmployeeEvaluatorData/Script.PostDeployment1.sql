﻿MERGE INTO Admin AS Target 
USING (VALUES 
        (1, 'user1@mailinator.com', PWDENCRYPT('Asdf1591!')), 
        (2, 'user2@mailinator.com', PWDENCRYPT('Asdf1591!'))
) 
AS Source (Id, Email, PasswordHash) 
ON Target.Id = Source.Id 
WHEN NOT MATCHED BY TARGET THEN 
INSERT (Email, PasswordHash) 
VALUES (Email, PasswordHash);

MERGE INTO Cohort AS Target
USING (VALUES 
        (1, 'Cohort1', 'Type2', 'Formative', '2017-02-20', '2017-02-27'), 
        (2, 'Cohort2', 'Type1', 'Baseline', '2017-03-20', '2017-03-27')
)
AS Source (Id, Name, Type, Stage, StartDate, EndDate)
ON Target.Id = Source.Id
WHEN NOT MATCHED BY TARGET THEN
INSERT (Name, Type, Stage, StartDate, EndDate)
VALUES (Name, Type, Stage, StartDate, EndDate);

MERGE INTO Employee AS Target
USING (VALUES 
		(1, 'John', 'Smith', 'employee1@mailinator.com', '123 Street', '6785621254', 1, PWDENCRYPT('Asdf1591!')),
		(2, 'Bob', 'Jones', 'employee2@mailinator.com', '1234 Street', '6785411287', 2, PWDENCRYPT('Asdf1591!')),
		(3, 'Sally', 'Smith', 'employee3@mailinator.com', '12345 Street', '6787515486', 1, PWDENCRYPT('Asdf1591!'))
)
AS Source (Id, FirstName, LastName, Email, MailingAddress, Phone, CohortID, PasswordHash)
ON Target.Id = Source.Id
WHEN NOT MATCHED BY TARGET THEN
INSERT (FirstName, LastName, Email, MailingAddress, Phone, CohortID, PasswordHash)
VALUES (FirstName, LastName, Email, MailingAddress, Phone, CohortID, PasswordHash);