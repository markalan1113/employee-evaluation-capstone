﻿using EmployeeEvaluator.DAL;
using EmployeeEvaluator.Models;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeEvaluatorTests.TestData
{
    class TestRepositoryData
    {
        InMemoryAdminRepository adminRepository;
        InMemoryCategoryRepository categoryRepository;
        InMemoryCohortRepository cohortRepository;
        InMemoryEmployeeRepository employeeRepository;
        InMemoryEvalStageRepository evalStageRepository;
        InMemoryEvalTypeRepository evalTypeRepository;
        InMemoryEvaluationRepository evaluationRepository;
        InMemoryMaxRaterRepository maxRaterRepository;
        InMemoryQuestionRepository questionRepository;
        InMemoryRaterRepository raterRepository;
        InMemoryRaterRoleRepository raterRoleRepository;

        internal InMemoryAdminRepository AdminRepository
        {
            get
            {
                return adminRepository;
            }

            set
            {
                adminRepository = value;
            }
        }

        internal InMemoryCategoryRepository CategoryRepository
        {
            get
            {
                return categoryRepository;
            }

            set
            {
                categoryRepository = value;
            }
        }

        internal InMemoryCohortRepository CohortRepository
        {
            get
            {
                return cohortRepository;
            }

            set
            {
                cohortRepository = value;
            }
        }

        internal InMemoryEmployeeRepository EmployeeRepository
        {
            get
            {
                return employeeRepository;
            }

            set
            {
                employeeRepository = value;
            }
        }

        internal InMemoryEvalStageRepository EvalStageRepository
        {
            get
            {
                return evalStageRepository;
            }

            set
            {
                evalStageRepository = value;
            }
        }

        internal InMemoryEvalTypeRepository EvalTypeRepository
        {
            get
            {
                return evalTypeRepository;
            }

            set
            {
                evalTypeRepository = value;
            }
        }

        internal InMemoryEvaluationRepository EvaluationRepository
        {
            get
            {
                return evaluationRepository;
            }

            set
            {
                evaluationRepository = value;
            }
        }

        internal InMemoryMaxRaterRepository MaxRaterRepository
        {
            get
            {
                return maxRaterRepository;
            }

            set
            {
                maxRaterRepository = value;
            }
        }

        internal InMemoryQuestionRepository QuestionRepository
        {
            get
            {
                return questionRepository;
            }

            set
            {
                questionRepository = value;
            }
        }

        internal InMemoryRaterRepository RaterRepository
        {
            get
            {
                return raterRepository;
            }

            set
            {
                raterRepository = value;
            }
        }

        internal InMemoryRaterRoleRepository RaterRoleRepository
        {
            get
            {
                return raterRoleRepository;
            }

            set
            {
                raterRoleRepository = value;
            }
        }

        public TestRepositoryData()
        {
            AdminRepository = new InMemoryAdminRepository(new EvaluatorContext());
            CategoryRepository = new InMemoryCategoryRepository(new EvaluatorContext());
            CohortRepository = new InMemoryCohortRepository(new EvaluatorContext());
            EmployeeRepository = new InMemoryEmployeeRepository(new EvaluatorContext());
            EvalStageRepository = new InMemoryEvalStageRepository(new EvaluatorContext());
            EvalTypeRepository = new InMemoryEvalTypeRepository(new EvaluatorContext());
            EvaluationRepository = new InMemoryEvaluationRepository(new EvaluatorContext());
            MaxRaterRepository = new InMemoryMaxRaterRepository(new EvaluatorContext());
            QuestionRepository = new InMemoryQuestionRepository(new EvaluatorContext());
            RaterRepository = new InMemoryRaterRepository(new EvaluatorContext());
            RaterRoleRepository = new InMemoryRaterRoleRepository(new EvaluatorContext());
        }
        public void IntializeTestData()
        {
            string salt = "92429d82a41e930486c6de5ebda9602d55c39986";
            var password = "password";
            var bytes = Encoding.UTF8.GetBytes(password + salt);
            var hashedPassword = Convert.ToBase64String((new SHA256Managed()).ComputeHash(bytes));

            Admin admin1 = new Admin
            {
                AdminID = 1,
                Email = "admin@admin.com",
                HashedPassword = hashedPassword
            };
            Admin admin2 = new Admin
            {
                AdminID = 2,
                Email = "user2@mailinator.com",
                HashedPassword = hashedPassword
            };

            AdminRepository.Insert(admin1);
            AdminRepository.Insert(admin2);

            Cohort cohort1 = new Cohort
            {
                CohortID = 1,
                Name = "cohort1",
                Stable = true
            };
            Cohort cohort2 = new Cohort
            {
                CohortID = 2,
                Name = "cohort2",
                Stable = false
            };

            CohortRepository.Insert(cohort1);
            CohortRepository.Insert(cohort2);

            Employee employee1 = new Employee
            {
                EmployeeID = 1,
                Email = "employee1@mailinator.com",
                HashedPassword = hashedPassword,
                FirstName = "John",
                LastName = "Smith",
                MailingAddress = "123 Street",
                Phone = "6785235415",
                CohortID = cohort1.CohortID,
                Active = true
            };
            Employee employee2 = new Employee
            {
                EmployeeID = 2,
                Email = "employee2@mailinator.com",
                HashedPassword = hashedPassword,
                FirstName = "Bob",
                LastName = "Jones",
                MailingAddress = "234 Street",
                Phone = "6784523645",
                CohortID = cohort1.CohortID,
                Active = true
            };

            RaterRole role1 = new RaterRole
            {
                RaterRoleID = 1,
                RoleName = "Supervisor",
            };
            RaterRole role2 = new RaterRole
            {
                RaterRoleID = 2,
                RoleName = "Coworker"
            };
            RaterRole role3 = new RaterRole
            {
                RaterRoleID = 3,
                RoleName = "Supervisee"
            };

            RaterRoleRepository.Insert(role1);
            RaterRoleRepository.Insert(role2);
            RaterRoleRepository.Insert(role3);

            MaxRater maxSupervisor = new MaxRater
            {
                MaxRaterID = 1,
                RaterRoleID = role1.RaterRoleID,
                Max = 1
            };
            MaxRater maxSupervisee = new MaxRater
            {
                MaxRaterID = 2,
                RaterRoleID = role3.RaterRoleID,
                Max = 2
            };
            MaxRater maxCoworker = new MaxRater
            {
                MaxRaterID = 3,
                RaterRoleID = role2.RaterRoleID,
                Max = 2
            };

            MaxRaterRepository.Insert(maxSupervisor);
            MaxRaterRepository.Insert(maxSupervisee);
            MaxRaterRepository.Insert(maxCoworker);

            Rater rater1 = new Rater
            {
                RaterID = 1,
                Email = "rater1@mailinator.com",
                FirstName = "Rater",
                LastName = "One",
                Active = true,
                RaterRoleID = role1.RaterRoleID
            };
            Rater rater2 = new Rater
            {
                RaterID = 2,
                Email = "rater2@mailinator.com",
                FirstName = "Rater",
                LastName = "Two",
                Active = true,
                RaterRoleID = role2.RaterRoleID
            };
            Rater rater3 = new Rater
            {
                RaterID = 3,
                Email = "rater3@mailinator.com",
                FirstName = "Rater",
                LastName = "Three",
                Active = true,
                RaterRoleID = role2.RaterRoleID
            };
            Rater rater4 = new Rater
            {
                RaterID = 4,
                Email = "rater4@mailinator.com",
                FirstName = "Rater",
                LastName = "Four",
                Active = true,
                RaterRoleID = role3.RaterRoleID
            };
            Rater rater5 = new Rater
            {
                RaterID = 5,
                Email = "rater5@mailinator.com",
                FirstName = "Rater",
                LastName = "Five",
                Active = true,
                RaterRoleID = role3.RaterRoleID
            };
            Rater rater6 = new Rater
            {
                RaterID = 6,
                Email = "rater5@mailinator.com",
                FirstName = "Rater",
                LastName = "Five",
                Active = false,
                RaterRoleID = role3.RaterRoleID
            };


            employee1.Raters.Add(rater1);
            employee1.Raters.Add(rater2);
            employee1.Raters.Add(rater3);
            employee1.Raters.Add(rater4);
            employee1.Raters.Add(rater5);
            employee1.Raters.Add(rater6);
            rater1.Employees.Add(employee1);
            rater2.Employees.Add(employee1);
            rater3.Employees.Add(employee1);
            rater4.Employees.Add(employee1);
            rater5.Employees.Add(employee1);
            rater6.Employees.Add(employee1);

            EmployeeRepository.Insert(employee1);
            EmployeeRepository.Insert(employee2);

            RaterRepository.Insert(rater1);
            RaterRepository.Insert(rater2);
            RaterRepository.Insert(rater3);
            RaterRepository.Insert(rater4);
            RaterRepository.Insert(rater5);
            RaterRepository.Insert(rater6);

            EvalStage baseline = new EvalStage
            {
                EvalStageID = 1,
                StageName = "Baseline"
            };
            EvalStage formative = new EvalStage
            {
                EvalStageID = 2,
                StageName = "Formative"
            };
            EvalStage summative = new EvalStage
            {
                EvalStageID = 3,
                StageName = "Summative"
            };

            EvalType type1 = new EvalType
            {
                EvalTypeID = 1,
                TypeName = "Type 1"
            };
            EvalType type2 = new EvalType
            {
                EvalTypeID = 2,
                TypeName = "Type 2"
            };

            EvalTypeRepository.Insert(type1);
            EvalTypeRepository.Insert(type2);

            EvalStageRepository.Insert(baseline);
            EvalStageRepository.Insert(formative);
            EvalStageRepository.Insert(summative);

            var evaluation = new Evaluation();
            evaluation.EvaluationID = 6;
            evaluation.CohortID = 1;
            evaluation.EmployeeID = 1;
            evaluation.RaterID = 1;
            evaluation.EvalTypeID = 1;
            evaluation.EvalStageID = 1;
            evaluation.EvalStage = baseline;
            evaluation.EvalType = type1;
            evaluation.Employee = employee1;
            evaluation.StartDate = new DateTime(2017, 05, 29);
            evaluation.EndDate = new DateTime(2017, 06, 29);
            evaluation.Completed = false;

            EvaluationRepository.Insert(evaluation);
            employee1.Evaluations.Add(evaluation);
            rater1.Evaluations.Add(evaluation);
            rater2.Evaluations.Add(evaluation);
            rater3.Evaluations.Add(evaluation);
            rater4.Evaluations.Add(evaluation);
            rater5.Evaluations.Add(evaluation);

            Category type1category1 = new Category
            {
                EvalTypeID = type1.EvalTypeID,
                CategoryName = "Leadership",
                CategoryDescription = "Questions about Leadership."
            };
            Category type1category2 = new Category
            {
                EvalTypeID = type1.EvalTypeID,
                CategoryName = "Work Ethic",
                CategoryDescription = "Questions about Work Ethic."
            };
            Category type1category3 = new Category
            {
                EvalTypeID = type1.EvalTypeID,
                CategoryName = "Punctuality",
                CategoryDescription = "Questions about Punctuality."
            };
            Category type1category4 = new Category
            {
                EvalTypeID = type1.EvalTypeID,
                CategoryName = "Type 1 Category 4",
                CategoryDescription = "Questions about Type 1 Category 4."
            };
            Category type1category5 = new Category
            {
                EvalTypeID = type1.EvalTypeID,
                CategoryName = "Type 1 Category 5",
                CategoryDescription = "Questions about Type 1 Category 5."
            };

            Category type2category1 = new Category
            {
                EvalTypeID = type2.EvalTypeID,
                CategoryName = "Type 2 Category 1",
                CategoryDescription = "Questions about Type 2 Category 1."
            };
            Category type2category2 = new Category
            {
                EvalTypeID = type2.EvalTypeID,
                CategoryName = "Type 2 Category 2",
                CategoryDescription = "Questions about Type 2 Category 2."
            };
            Category type2category3 = new Category
            {
                EvalTypeID = type2.EvalTypeID,
                CategoryName = "Type 2 Category 3",
                CategoryDescription = "Questions about Type 2 Category 3."
            };
            Category type2category4 = new Category
            {
                EvalTypeID = type2.EvalTypeID,
                CategoryName = "Type 2 Category 4",
                CategoryDescription = "Questions about Type 2 Category 4."
            };
            Category type2category5 = new Category
            {
                EvalTypeID = type2.EvalTypeID,
                CategoryName = "Type 2 Category 5",
                CategoryDescription = "Questions about Type 2 Category 5."
            };
            Category type2category6 = new Category
            {
                EvalTypeID = type2.EvalTypeID,
                CategoryName = "Type 2 Category 6",
                CategoryDescription = "Questions about Type 2 Category 6."
            };
            Category type2category7 = new Category
            {
                EvalTypeID = type2.EvalTypeID,
                CategoryName = "Type 2 Category 7",
                CategoryDescription = "Questions about Type 2 Category 7."
            };

            CategoryRepository.Insert(type1category1);
            CategoryRepository.Insert(type1category2);
            CategoryRepository.Insert(type1category3);
            CategoryRepository.Insert(type1category4);
            CategoryRepository.Insert(type1category5);
            CategoryRepository.Insert(type2category1);
            CategoryRepository.Insert(type2category2);
            CategoryRepository.Insert(type2category3);
            CategoryRepository.Insert(type2category4);
            CategoryRepository.Insert(type2category5);
            CategoryRepository.Insert(type2category6);
            CategoryRepository.Insert(type2category6);


            Question type1category1question1 = new Question
            {
                QuestionText = "T1C1Q1",
                CategoryID = type1category1.CategoryID,
                InEvaluation = false
            };
            Question type1category1question2 = new Question
            {
                QuestionText = "T1C1Q2",
                CategoryID = type1category1.CategoryID,
                InEvaluation = false
            };
            Question type1category1question3 = new Question
            {
                QuestionText = "T1C1Q3",
                CategoryID = type1category1.CategoryID,
                InEvaluation = false
            };
            Question type1category2question1 = new Question
            {
                QuestionText = "T1C2Q1",
                CategoryID = type1category2.CategoryID,
                InEvaluation = false
            };
            Question type1category2question2 = new Question
            {
                QuestionText = "T1C2Q2",
                CategoryID = type1category2.CategoryID,
                InEvaluation = false
            };
            Question type1category2question3 = new Question
            {
                QuestionText = "T1C2Q3",
                CategoryID = type1category2.CategoryID,
                InEvaluation = false
            };
            Question type1category3question1 = new Question
            {
                QuestionText = "T1C3Q1",
                CategoryID = type1category3.CategoryID,
                InEvaluation = false
            };
            Question type1category3question2 = new Question
            {
                QuestionText = "T1C3Q2",
                CategoryID = type1category3.CategoryID,
                InEvaluation = false
            };
            Question type1category3question3 = new Question
            {
                QuestionText = "T1C3Q3",
                CategoryID = type1category3.CategoryID,
                InEvaluation = false
            };
            Question type1category4question1 = new Question
            {
                QuestionText = "T1C4Q1",
                CategoryID = type1category4.CategoryID,
                InEvaluation = false
            };
            Question type1category4question2 = new Question
            {
                QuestionText = "T1C4Q2",
                CategoryID = type1category4.CategoryID,
                InEvaluation = false
            };
            Question type1category4question3 = new Question
            {
                QuestionText = "T1C4Q3",
                CategoryID = type1category4.CategoryID,
                InEvaluation = false
            };
            Question type1category5question1 = new Question
            {
                QuestionText = "T1C5Q1",
                CategoryID = type1category5.CategoryID,
                InEvaluation = false
            };
            Question type1category5question2 = new Question
            {
                QuestionText = "T1C5Q2",
                CategoryID = type1category5.CategoryID,
                InEvaluation = false
            };
            Question type1category5question3 = new Question
            {
                QuestionText = "T1C5Q3",
                CategoryID = type1category5.CategoryID,
                InEvaluation = false
            };
            Question type2category1question1 = new Question
            {
                QuestionText = "T2C1Q1",
                CategoryID = type2category1.CategoryID,
                InEvaluation = false
            };
            Question type2category1question2 = new Question
            {
                QuestionText = "T2C1Q2",
                CategoryID = type2category1.CategoryID,
                InEvaluation = false
            };
            Question type2category1question3 = new Question
            {
                QuestionText = "T2C1Q3",
                CategoryID = type2category1.CategoryID,
                InEvaluation = false
            };
            Question type2category1question4 = new Question
            {
                QuestionText = "T2C1Q4",
                CategoryID = type2category1.CategoryID,
                InEvaluation = false
            };
            Question type2category2question1 = new Question
            {
                QuestionText = "T2C2Q1",
                CategoryID = type2category2.CategoryID,
                InEvaluation = false
            };
            Question type2category2question2 = new Question
            {
                QuestionText = "T2C2Q2",
                CategoryID = type2category2.CategoryID,
                InEvaluation = false
            };
            Question type2category2question3 = new Question
            {
                QuestionText = "T2C2Q3",
                CategoryID = type2category2.CategoryID,
                InEvaluation = false
            };
            Question type2category2question4 = new Question
            {
                QuestionText = "T2C2Q4",
                CategoryID = type2category2.CategoryID,
                InEvaluation = false
            };

            Question type2category3question1 = new Question
            {
                QuestionText = "T2C3Q1",
                CategoryID = type2category3.CategoryID,
                InEvaluation = false
            };
            Question type2category3question2 = new Question
            {
                QuestionText = "T2C3Q2",
                CategoryID = type2category3.CategoryID,
                InEvaluation = false
            };
            Question type2category3question3 = new Question
            {
                QuestionText = "T2C3Q3",
                CategoryID = type2category3.CategoryID,
                InEvaluation = false
            };
            Question type2category3question4 = new Question
            {
                QuestionText = "T2C3Q4",
                CategoryID = type2category3.CategoryID,
                InEvaluation = false
            };

            Question type2category4question1 = new Question
            {
                QuestionText = "T2C4Q1",
                CategoryID = type2category4.CategoryID,
                InEvaluation = false
            };
            Question type2category4question2 = new Question
            {
                QuestionText = "T2C4Q2",
                CategoryID = type2category4.CategoryID,
                InEvaluation = false
            };
            Question type2category4question3 = new Question
            {
                QuestionText = "T2C4Q3",
                CategoryID = type2category4.CategoryID,
                InEvaluation = false
            };
            Question type2category4question4 = new Question
            {
                QuestionText = "T2C4Q4",
                CategoryID = type2category4.CategoryID,
                InEvaluation = false
            };

            Question type2category5question1 = new Question
            {
                QuestionText = "T2C5Q1",
                CategoryID = type2category5.CategoryID,
                InEvaluation = false
            };
            Question type2category5question2 = new Question
            {
                QuestionText = "T2C5Q2",
                CategoryID = type2category5.CategoryID,
                InEvaluation = false
            };
            Question type2category5question3 = new Question
            {
                QuestionText = "T2C5Q3",
                CategoryID = type2category5.CategoryID,
                InEvaluation = false
            };
            Question type2category5question4 = new Question
            {
                QuestionText = "T2C5Q4",
                CategoryID = type2category5.CategoryID,
                InEvaluation = false
            };

            Question type2category6question1 = new Question
            {
                QuestionText = "T2C6Q1",
                CategoryID = type2category6.CategoryID,
                InEvaluation = false
            };
            Question type2category6question2 = new Question
            {
                QuestionText = "T2C6Q2",
                CategoryID = type2category6.CategoryID,
                InEvaluation = false
            };
            Question type2category6question3 = new Question
            {
                QuestionText = "T2C6Q3",
                CategoryID = type2category6.CategoryID,
                InEvaluation = false
            };
            Question type2category6question4 = new Question
            {
                QuestionText = "T2C6Q4",
                CategoryID = type2category6.CategoryID,
                InEvaluation = false
            };

            Question type2category7question1 = new Question
            {
                QuestionText = "T2C7Q1",
                CategoryID = type2category7.CategoryID,
                InEvaluation = false
            };
            Question type2category7question2 = new Question
            {
                QuestionText = "T2C7Q2",
                CategoryID = type2category7.CategoryID,
                InEvaluation = false
            };
            Question type2category7question3 = new Question
            {
                QuestionText = "T2C7Q3",
                CategoryID = type2category7.CategoryID,
                InEvaluation = false
            };
            Question type2category7question4 = new Question
            {
                QuestionText = "T2C7Q4",
                CategoryID = type2category7.CategoryID,
                InEvaluation = false
            };

            QuestionRepository.Insert(type1category1question1);
            QuestionRepository.Insert(type1category1question2);
            QuestionRepository.Insert(type1category1question3);
            QuestionRepository.Insert(type1category2question1);
            QuestionRepository.Insert(type1category2question2);
            QuestionRepository.Insert(type1category2question3);
            QuestionRepository.Insert(type1category3question1);
            QuestionRepository.Insert(type1category3question2);
            QuestionRepository.Insert(type1category3question3);
            QuestionRepository.Insert(type1category4question1);
            QuestionRepository.Insert(type1category4question2);
            QuestionRepository.Insert(type1category4question3);
            QuestionRepository.Insert(type1category5question1);
            QuestionRepository.Insert(type1category5question2);
            QuestionRepository.Insert(type1category5question3);
            QuestionRepository.Insert(type2category1question1);
            QuestionRepository.Insert(type2category1question2);
            QuestionRepository.Insert(type2category1question3);
            QuestionRepository.Insert(type2category2question1);
            QuestionRepository.Insert(type2category2question2);
            QuestionRepository.Insert(type2category2question3);
            QuestionRepository.Insert(type2category3question1);
            QuestionRepository.Insert(type2category3question2);
            QuestionRepository.Insert(type2category3question3);
            QuestionRepository.Insert(type2category4question1);
            QuestionRepository.Insert(type2category4question2);
            QuestionRepository.Insert(type2category4question3);
            QuestionRepository.Insert(type2category5question1);
            QuestionRepository.Insert(type2category5question2);
            QuestionRepository.Insert(type2category5question3);
            QuestionRepository.Insert(type2category6question1);
            QuestionRepository.Insert(type2category6question2);
            QuestionRepository.Insert(type2category6question3);
            QuestionRepository.Insert(type2category7question1);
            QuestionRepository.Insert(type2category7question2);
            QuestionRepository.Insert(type2category7question3);
        }
    }
}
