﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.Entity;
using EmployeeEvaluator.Models;
using System.Linq.Expressions;
using EmployeeEvaluator.DAL;


namespace Model
{
    class InMemoryQuestionRepository : IRepository<Question>
    {
        private EvaluatorContext context;
        private DbSet<Question> dbSet;

        public InMemoryQuestionRepository(EvaluatorContext context)
        {
            this.context = context;
            this.dbSet = new TestDbSet<Question>();
        }

        public virtual IEnumerable<Question> Get(
            Expression<Func<Question, bool>> filter = null,
            Func<IQueryable<Question>, IOrderedQueryable<Question>> orderBy = null,
            string includeProperties = "")
        {
            IQueryable<Question> query = dbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }

            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            if (orderBy != null)
            {
                return orderBy(query).ToList();
            }
            else
            {
                return query.ToList();
            }
        }

        public virtual Question GetByID(object id)
        {
            return dbSet.Where(q => q.QuestionID == (int)id).FirstOrDefault();
        }

        public virtual void Insert(Question entity)
        {
            dbSet.Add(entity);
        }

        public virtual void Delete(object id)
        {
            Question entityToDelete = dbSet.Find(id);
            Delete(entityToDelete);
        }

        public virtual void Delete(Question entityToDelete)
        {
            if (context.Entry(entityToDelete).State == EntityState.Detached)
            {
                dbSet.Attach(entityToDelete);
            }
            dbSet.Remove(entityToDelete);
        }

        public virtual void Update(Question entityToUpdate)
        {
            dbSet.Attach(entityToUpdate);
            context.Entry(entityToUpdate).State = EntityState.Modified;
        }

    }
}
