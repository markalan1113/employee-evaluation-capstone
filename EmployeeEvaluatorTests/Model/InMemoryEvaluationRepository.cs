﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using EmployeeEvaluator.DAL;
using EmployeeEvaluator.Models;


namespace Model
{
    class InMemoryEvaluationRepository : IRepository<Evaluation>
    {
        private EvaluatorContext context;
        private TestDbSet<Evaluation> dbSet;

        public InMemoryEvaluationRepository(EvaluatorContext context)
        {
            this.context = context;
            this.dbSet = new TestDbSet<Evaluation>();
        }

        public virtual IEnumerable<Evaluation> Get(
            Expression<Func<Evaluation, bool>> filter = null,
            Func<IQueryable<Evaluation>, IOrderedQueryable<Evaluation>> orderBy = null,
            string includeProperties = "")
        {
            IQueryable<Evaluation> query = dbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }

            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            if (orderBy != null)
            {
                return orderBy(query).ToList();
            }
            else
            {
                return query.ToList();
            }
        }

        public virtual Evaluation GetByID(object id)
        {
            return dbSet.Where(e => e.EvaluationID == (int)id).FirstOrDefault();

        }

        public virtual void Insert(Evaluation entity)
        {
            dbSet.Add(entity);
        }

        public virtual void Delete(object id)
        {
            Evaluation entityToDelete = dbSet.Find(id);
            Delete(entityToDelete);
        }

        public virtual void Delete(Evaluation entityToDelete)
        {
            if (context.Entry(entityToDelete).State == EntityState.Detached)
            {
                dbSet.Attach(entityToDelete);
            }
            dbSet.Remove(entityToDelete);
        }

        public virtual void Update(Evaluation entityToUpdate)
        {
            dbSet.Attach(entityToUpdate);
            //context.Entry(entityToUpdate).State = EntityState.Modified;
        }

    }
}