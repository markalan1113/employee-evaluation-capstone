﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.Entity;
using EmployeeEvaluator.Models;
using System.Linq.Expressions;
using EmployeeEvaluator.DAL;


namespace Model
{
    class InMemoryRaterRoleRepository : IRepository<RaterRole>
    {
        private EvaluatorContext context;
        private DbSet<RaterRole> dbSet;

        public InMemoryRaterRoleRepository(EvaluatorContext context)
        {
            this.context = context;
            this.dbSet = new TestDbSet<RaterRole>();
        }

        public virtual IEnumerable<RaterRole> Get(
            Expression<Func<RaterRole, bool>> filter = null,
            Func<IQueryable<RaterRole>, IOrderedQueryable<RaterRole>> orderBy = null,
            string includeProperties = "")
        {
            IQueryable<RaterRole> query = dbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }

            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            if (orderBy != null)
            {
                return orderBy(query).ToList();
            }
            else
            {
                return query.ToList();
            }
        }

        public virtual RaterRole GetByID(object id)
        {
            return dbSet.Where(r => r.RaterRoleID == (int)id).FirstOrDefault();
        }

        public virtual void Insert(RaterRole entity)
        {
            dbSet.Add(entity);
        }

        public virtual void Delete(object id)
        {
            RaterRole entityToDelete = dbSet.Find(id);
            Delete(entityToDelete);
        }

        public virtual void Delete(RaterRole entityToDelete)
        {
            if (context.Entry(entityToDelete).State == EntityState.Detached)
            {
                dbSet.Attach(entityToDelete);
            }
            dbSet.Remove(entityToDelete);
        }

        public virtual void Update(RaterRole entityToUpdate)
        {
            dbSet.Attach(entityToUpdate);
            context.Entry(entityToUpdate).State = EntityState.Modified;
        }

    }
}
