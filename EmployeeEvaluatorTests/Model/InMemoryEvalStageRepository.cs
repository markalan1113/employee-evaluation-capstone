﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.Entity;
using EmployeeEvaluator.Models;
using System.Linq.Expressions;
using EmployeeEvaluator.DAL;
using Model;

namespace Model
{
    class InMemoryEvalStageRepository : IRepository<EvalStage>
    {
        private EvaluatorContext context;
        private DbSet<EvalStage> dbSet;

        public InMemoryEvalStageRepository(EvaluatorContext context)
        {
            this.context = context;
            this.dbSet = new TestDbSet<EvalStage>();
        }

        public virtual IEnumerable<EvalStage> Get(
            Expression<Func<EvalStage, bool>> filter = null,
            Func<IQueryable<EvalStage>, IOrderedQueryable<EvalStage>> orderBy = null,
            string includeProperties = "")
        {
            IQueryable<EvalStage> query = dbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }

            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            if (orderBy != null)
            {
                return orderBy(query).ToList();
            }
            else
            {
                return query.ToList();
            }
        }

        public virtual EvalStage GetByID(object id)
        {
            return dbSet.Where(s => s.EvalStageID == (int)id).FirstOrDefault();
        }

        public virtual void Insert(EvalStage entity)
        {
            dbSet.Add(entity);
        }

        public virtual void Delete(object id)
        {
            EvalStage entityToDelete = dbSet.Find(id);
            Delete(entityToDelete);
        }
        public virtual void Delete(EvalStage entityToDelete)
        {
            if (context.Entry(entityToDelete).State == EntityState.Detached)
            {
                dbSet.Attach(entityToDelete);
            }
            dbSet.Remove(entityToDelete);
        }

        public virtual void Update(EvalStage entityToUpdate)
        {
            dbSet.Attach(entityToUpdate);
            context.Entry(entityToUpdate).State = EntityState.Modified;
        }

    }
}
