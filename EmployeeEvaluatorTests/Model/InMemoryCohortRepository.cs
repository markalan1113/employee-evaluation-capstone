﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using EmployeeEvaluator.DAL;
using EmployeeEvaluator.Models;


namespace Model
{
    class InMemoryCohortRepository : IRepository<Cohort>
    {
        private EvaluatorContext context;
        private DbSet<Cohort> dbSet;

        public InMemoryCohortRepository(EvaluatorContext context)
        {
            this.context = context;
            this.dbSet = new TestDbSet<Cohort>();
        }

        public virtual IEnumerable<Cohort> Get(
            Expression<Func<Cohort, bool>> filter = null,
            Func<IQueryable<Cohort>, IOrderedQueryable<Cohort>> orderBy = null,
            string includeProperties = "")
        {
            IQueryable<Cohort> query = dbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }

            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            if (orderBy != null)
            {
                return orderBy(query).ToList();
            }
            else
            {
                return query.ToList();
            }
        }

        public virtual Cohort GetByID(object id)
        {
            return dbSet.Where(c => c.CohortID == (int)id).FirstOrDefault();
        }

        public virtual void Insert(Cohort entity)
        {
            dbSet.Add(entity);
        }

        public virtual void Delete(object id)
        {
            Cohort entityToDelete = dbSet.Find(id);
            Delete(entityToDelete);
        }

        public virtual void Delete(Cohort entityToDelete)
        {
            if (context.Entry(entityToDelete).State == EntityState.Detached)
            {
                dbSet.Attach(entityToDelete);
            }
            dbSet.Remove(entityToDelete);
        }

        public virtual void Update(Cohort entityToUpdate)
        {
            dbSet.Attach(entityToUpdate);
            context.Entry(entityToUpdate).State = EntityState.Modified;
        }

    }
}
