﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.Entity;
using EmployeeEvaluator.Models;
using System.Linq.Expressions;
using EmployeeEvaluator.DAL;


namespace Model
{
    class InMemoryCategoryRepository : IRepository<Category>
    {
        private EvaluatorContext context;
        private DbSet<Category> dbSet;

        public InMemoryCategoryRepository(EvaluatorContext context)
        {
            this.context = context;
            this.dbSet = new TestDbSet<Category>();
        }

        public virtual IEnumerable<Category> Get(
            Expression<Func<Category, bool>> filter = null,
            Func<IQueryable<Category>, IOrderedQueryable<Category>> orderBy = null,
            string includeProperties = "")
        {
            IQueryable<Category> query = dbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }

            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            if (orderBy != null)
            {
                return orderBy(query).ToList();
            }
            else
            {
                return query.ToList();
            }
        }

        public virtual Category GetByID(object id)
        {
            return dbSet.Where(c => c.CategoryID == (int)id).FirstOrDefault();

        }

        public virtual void Insert(Category entity)
        {
            dbSet.Add(entity);
        }

        public virtual void Delete(object id)
        {
            Category entityToDelete = dbSet.Find(id);
            Delete(entityToDelete);
        }

        public virtual void Delete(Category entityToDelete)
        {
            if (context.Entry(entityToDelete).State == EntityState.Detached)
            {
                dbSet.Attach(entityToDelete);
            }
            dbSet.Remove(entityToDelete);
        }

        public virtual void Update(Category entityToUpdate)
        {
            dbSet.Attach(entityToUpdate);
            //context.Entry(entityToUpdate).State = EntityState.Modified;
        }

    }
}
