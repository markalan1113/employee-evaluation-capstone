﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.Entity;
using EmployeeEvaluator.Models;
using System.Linq.Expressions;
using EmployeeEvaluator.DAL;


namespace Model
{
    class InMemoryAdminRepository : IRepository<Admin>
    {
        private EvaluatorContext context;
        private DbSet<Admin> dbSet;

        public InMemoryAdminRepository(EvaluatorContext context)
        {
            this.context = context;
            this.dbSet = new TestDbSet<Admin>();
        }

        public virtual IEnumerable<Admin> Get(
            Expression<Func<Admin, bool>> filter = null,
            Func<IQueryable<Admin>, IOrderedQueryable<Admin>> orderBy = null,
            string includeProperties = "")
        {
            IQueryable<Admin> query = dbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }

            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            if (orderBy != null)
            {
                return orderBy(query).ToList();
            }
            else
            {
                return query.ToList();
            }
        }

        public virtual Admin GetByID(object id)
        {
            return dbSet.Where(a => a.AdminID == (int)id).FirstOrDefault();

        }

        public virtual void Insert(Admin entity)
        {
            dbSet.Add(entity);
        }

        public virtual void Delete(object id)
        {
            Admin entityToDelete = dbSet.Find(id);
            Delete(entityToDelete);
        }

        public virtual void Delete(Admin entityToDelete)
        {
            if (context.Entry(entityToDelete).State == EntityState.Detached)
            {
                dbSet.Attach(entityToDelete);
            }
            dbSet.Remove(entityToDelete);
        }

        public virtual void Update(Admin entityToUpdate)
        {
            dbSet.Attach(entityToUpdate);
            context.Entry(entityToUpdate).State = EntityState.Modified;
        }

    }
}
