﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Reflection;

namespace Model
{
    public class TestDbSet<T> : DbSet<T>, IQueryable, IEnumerable<T>
        where T : class
    {
        List<T> _data;
        IQueryable _query;
        private IEnumerable<PropertyInfo> keys;

        public TestDbSet()
        {
            _data = new List<T>();
            _query = _data.AsQueryable();
            keys = new List<PropertyInfo>();
        }

        public override T Add(T item)
        {
            _data.Add(item);
            return item;
        }

        public override T Find(params object[] keyValues)
        {
            /*
             if (keyValues == null)
                 throw new ArgumentNullException("keyValues");
             if (keyValues.Any(k => k == null))
                 throw new ArgumentOutOfRangeException("keyValues");
             if (keyValues.Length != keys.Count())
                 throw new ArgumentOutOfRangeException("keyValues");
             return _data.SingleOrDefault(i =>
                 keys.Zip(keyValues, (k, v) => v.Equals(k.GetValue(i)))
                     .All(r => r)
             );
             */
            return null;
        }
        public override T Remove(T item)
        {
            _data.Remove(item);
            return item;
        }

        public override T Attach(T item)
        {
            _data.Add(item);
            return item;
        }

        public override T Create()
        {
            return Activator.CreateInstance<T>();
        }

        public override TDerivedEntity Create<TDerivedEntity>()
        {
            return Activator.CreateInstance<TDerivedEntity>();
        }

        public override ObservableCollection<T> Local
        {
            get { return new ObservableCollection<T>(_data); }
        }

        Type IQueryable.ElementType
        {
            get { return _query.ElementType; }
        }

        System.Linq.Expressions.Expression IQueryable.Expression
        {
            get { return _query.Expression; }
        }

        IQueryProvider IQueryable.Provider
        {
            get { return _query.Provider; }
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return _data.GetEnumerator();
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return _data.GetEnumerator();
        }
    }
}