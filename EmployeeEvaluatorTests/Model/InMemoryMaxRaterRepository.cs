﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.Entity;
using EmployeeEvaluator.Models;
using System.Linq.Expressions;
using EmployeeEvaluator.DAL;


namespace Model
{
    class InMemoryMaxRaterRepository : IRepository<MaxRater>
    {
        private EvaluatorContext context;
        private DbSet<MaxRater> dbSet;

        public InMemoryMaxRaterRepository(EvaluatorContext context)
        {
            this.context = context;
            this.dbSet = new TestDbSet<MaxRater>();
        }

        public virtual IEnumerable<MaxRater> Get(
            Expression<Func<MaxRater, bool>> filter = null,
            Func<IQueryable<MaxRater>, IOrderedQueryable<MaxRater>> orderBy = null,
            string includeProperties = "")
        {
            IQueryable<MaxRater> query = dbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }

            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            if (orderBy != null)
            {
                return orderBy(query).ToList();
            }
            else
            {
                return query.ToList();
            }
        }

        public virtual MaxRater GetByID(object id)
        {
            return dbSet.Where(m => m.MaxRaterID == (int)id).FirstOrDefault();

        }

        public virtual void Insert(MaxRater entity)
        {
            dbSet.Add(entity);
        }

        public virtual void Delete(object id)
        {
            MaxRater entityToDelete = dbSet.Find(id);
            Delete(entityToDelete);
        }

        public virtual void Delete(MaxRater entityToDelete)
        {
            if (context.Entry(entityToDelete).State == EntityState.Detached)
            {
                dbSet.Attach(entityToDelete);
            }
            dbSet.Remove(entityToDelete);
        }

        public virtual void Update(MaxRater entityToUpdate)
        {
            dbSet.Attach(entityToUpdate);
            context.Entry(entityToUpdate).State = EntityState.Modified;
        }

    }
}

