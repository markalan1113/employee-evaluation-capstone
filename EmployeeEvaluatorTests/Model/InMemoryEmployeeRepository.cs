﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using EmployeeEvaluator.DAL;
using EmployeeEvaluator.Models;


namespace Model
{
    class InMemoryEmployeeRepository : IRepository<Employee>
    {
        private EvaluatorContext context;
        private DbSet<Employee> dbSet;

        public InMemoryEmployeeRepository(EvaluatorContext context)
        {
            this.context = context;
            this.dbSet = new TestDbSet<Employee>();
        }

        public virtual IEnumerable<Employee> Get(
            Expression<Func<Employee, bool>> filter = null,
            Func<IQueryable<Employee>, IOrderedQueryable<Employee>> orderBy = null,
            string includeProperties = "")
        {
            IQueryable<Employee> query = dbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }

            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            if (orderBy != null)
            {
                return orderBy(query).ToList();
            }
            else
            {
                return query.ToList();
            }
        }

        public virtual Employee GetByID(object id)
        {
            return dbSet.Where(e => e.EmployeeID == (int)id).FirstOrDefault();
        }

        public virtual void Insert(Employee entity)
        {
            dbSet.Add(entity);
        }

        public virtual void Delete(object id)
        {
            Employee entityToDelete = dbSet.Find(id);
            Delete(entityToDelete);
        }

        public virtual void Delete(Employee entityToDelete)
        {
            if (context.Entry(entityToDelete).State == EntityState.Detached)
            {
                dbSet.Attach(entityToDelete);
            }
            dbSet.Remove(entityToDelete);
        }

        public virtual void Update(Employee entityToUpdate)
        {
            
            dbSet.Attach(entityToUpdate);
            //context.Entry(entityToUpdate).State = EntityState.Modified;
            
        }

    }
}