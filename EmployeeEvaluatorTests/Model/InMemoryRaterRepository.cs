﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using EmployeeEvaluator.DAL;
using EmployeeEvaluator.Models;


namespace Model
{
    class InMemoryRaterRepository : IRepository<Rater>
    {
        private EvaluatorContext context;
        private DbSet<Rater> dbSet;

        public InMemoryRaterRepository(EvaluatorContext context)
        {
            this.context = context;
            this.dbSet = new TestDbSet<Rater>();
        }

        public virtual IEnumerable<Rater> Get(
            Expression<Func<Rater, bool>> filter = null,
            Func<IQueryable<Rater>, IOrderedQueryable<Rater>> orderBy = null,
            string includeProperties = "")
        {
            IQueryable<Rater> query = dbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }

            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            if (orderBy != null)
            {
                return orderBy(query).ToList();
            }
            else
            {
                return query.ToList();
            }
        }

        public virtual Rater GetByID(object id)
        {
            return dbSet.Where(r => r.RaterID == (int)id).FirstOrDefault();
        }

        public virtual void Insert(Rater entity)
        {
            dbSet.Add(entity);
        }

        public virtual void Delete(object id)
        {
            Rater entityToDelete = dbSet.Find(id);
            Delete(entityToDelete);
        }

        public virtual void Delete(Rater entityToDelete)
        {
            if (context.Entry(entityToDelete).State == EntityState.Detached)
            {
                dbSet.Attach(entityToDelete);
            }
            dbSet.Remove(entityToDelete);
        }

        public virtual void Update(Rater entityToUpdate)
        {
            dbSet.Attach(entityToUpdate);
            //context.Entry(entityToUpdate).State = EntityState.Modified;
        }

    }
}