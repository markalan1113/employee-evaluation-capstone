﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.Entity;
using EmployeeEvaluator.Models;
using System.Linq.Expressions;
using EmployeeEvaluator.DAL;
using Model;

namespace Model
{
    class InMemoryEvalTypeRepository : IRepository<EvalType>
    {
        private EvaluatorContext context;
        private DbSet<EvalType> dbSet;

        public InMemoryEvalTypeRepository(EvaluatorContext context)
        {
            this.context = context;
            this.dbSet = new TestDbSet<EvalType>();
        }

        public virtual IEnumerable<EvalType> Get(
            Expression<Func<EvalType, bool>> filter = null,
            Func<IQueryable<EvalType>, IOrderedQueryable<EvalType>> orderBy = null,
            string includeProperties = "")
        {
            IQueryable<EvalType> query = dbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }

            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            if (orderBy != null)
            {
                return orderBy(query).ToList();
            }
            else
            {
                return query.ToList();
            }
        }

        public virtual EvalType GetByID(object id)
        {
            return dbSet.Where(e => e.EvalTypeID == (int)id).FirstOrDefault();
        }

        public virtual void Insert(EvalType entity)
        {
            dbSet.Add(entity);
        }

        public virtual void Delete(object id)
        {
            EvalType entityToDelete = dbSet.Find(id);
            Delete(entityToDelete);
        }
        public virtual void Delete(EvalType entityToDelete)
        {
            if (context.Entry(entityToDelete).State == EntityState.Detached)
            {
                dbSet.Attach(entityToDelete);
            }
            dbSet.Remove(entityToDelete);
        }

        public virtual void Update(EvalType entityToUpdate)
        {
            dbSet.Attach(entityToUpdate);
            context.Entry(entityToUpdate).State = EntityState.Modified;
        }

    }
}
