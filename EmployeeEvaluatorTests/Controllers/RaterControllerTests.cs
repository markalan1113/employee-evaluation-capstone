﻿using EmployeeEvaluator.Controllers;
using EmployeeEvaluator.DAL;
using EmployeeEvaluator.Models;
using EmployeeEvaluatorTests.TestData;
using EmployeeEvaluatorTests.Utility;
using Model;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.SessionState;

namespace EmployeeEvaluatorTests.Controllers
{

    [TestFixture()]
    class RaterControllerTests
    {
        RaterController controller;
        SessionStateItemCollection sessionItems;
        TestRepositoryData testData;

        [SetUp]
        public void SetUp()
        {
            testData = new TestRepositoryData();
            testData.IntializeTestData();
            sessionItems = new SessionStateItemCollection();
            sessionItems["AdminLoggedIn"] = true;
            sessionItems["EmployeeLoggedIn"] = true;
            sessionItems["LoggedInEmployeeID"] = 1;
            controller = new RaterController(testData.EvaluationRepository, testData.CohortRepository, testData.EmployeeRepository, testData.EvalStageRepository, testData.MaxRaterRepository, testData.RaterRoleRepository, testData.CategoryRepository, testData.EvalTypeRepository, testData.RaterRepository);
            controller.ControllerContext = new FakeControllerContext(controller, sessionItems);
        }

        [Test]
        public void Index()
        {
            ViewResult result = controller.Index() as ViewResult;
            Assert.IsNotNull(result);
        }

        [Test]
        public void Create()
        {
            ViewResult result = controller.Create() as ViewResult;
            Assert.IsNotNull(result);
        }

        [Test]
        public void AddRater()
        {
            var employee = new Employee();
            var rater = new Rater();
            employee.EmployeeID = 5;
            employee.Email = "employee@employee.com";
            employee.HashedPassword = "iPl7Ts91pbiHdVLzz/8cotF/rUEkCg9cvW2Xzy+IXWM=";
            employee.FirstName = "Test";
            employee.LastName = "Test";
            employee.MailingAddress = "123 Fake Street";
            employee.Phone = "1231231234";
            employee.CohortID = 1;
            rater.RaterID = 7;
            rater.FirstName = "Test";
            rater.LastName = "Testington";
            rater.Email = "rater@rater.com";
            rater.RaterRoleID = 327;

            ViewResult result = controller.Create(rater) as ViewResult;
            var raters = testData.RaterRepository.Get().ToList();
            var employees = testData.EmployeeRepository.Get().ToList();
            foreach (var raterTest in raters)
            {
                if (raterTest.RaterID == 7)
                {
                    Assert.AreEqual(7, raterTest.RaterID);
                }
            }
            foreach (var employeeTest in employees)
            {
                foreach (var employeeTest2 in employeeTest.Raters)
                {
                    if (employeeTest2.RaterID == 7)
                    {
                        Assert.AreEqual(7, employeeTest2.RaterID);
                    }
                }
            }
        }

        [Test]
        public void DoesNotAddNoFirstNameRater()
        {
            var employee = new Employee();
            var rater = new Rater();
            employee.EmployeeID = 5;
            employee.Email = "employee@employee.com";
            employee.HashedPassword = "iPl7Ts91pbiHdVLzz/8cotF/rUEkCg9cvW2Xzy+IXWM=";
            employee.FirstName = "Test";
            employee.LastName = "Test";
            employee.MailingAddress = "123 Fake Street";
            employee.Phone = "1231231234";
            employee.CohortID = 1;
            rater.RaterID = 8;
            rater.FirstName = "";
            rater.LastName = "Testington";
            rater.Email = "rater@rater.com";
            rater.RaterRoleID = 327;

            ViewResult result = controller.Create(rater) as ViewResult;
            var raters = testData.RaterRepository.Get().ToList();
            var employees = testData.EmployeeRepository.Get().ToList();
            foreach (var raterTest in raters)
            {
                if (raterTest.RaterID == 8)
                {
                    Assert.Fail();
                }
            }
            foreach (var employeeTest in employees)
            {
                foreach (var employeeTest2 in employeeTest.Raters)
                {
                    if (employeeTest2.RaterID == 8)
                    {
                        Assert.Fail();
                    }
                }
            }
        }

            [Test]
        public void DoesNotAddNoLastNameRater()
        {
            var employee = new Employee();
            var rater = new Rater();
            employee.EmployeeID = 5;
            employee.Email = "employee@employee.com";
            employee.HashedPassword = "iPl7Ts91pbiHdVLzz/8cotF/rUEkCg9cvW2Xzy+IXWM=";
            employee.FirstName = "Test";
            employee.LastName = "Test";
            employee.MailingAddress = "123 Fake Street";
            employee.Phone = "1231231234";
            employee.CohortID = 1;
            rater.RaterID = 8;
            rater.FirstName = "Test";
            rater.LastName = "";
            rater.Email = "rater@rater.com";
            rater.RaterRoleID = 327;

            ViewResult result = controller.Create(rater) as ViewResult;
            var raters = testData.RaterRepository.Get().ToList();
            var employees = testData.EmployeeRepository.Get().ToList();
            foreach (var raterTest in raters)
            {
                if (raterTest.RaterID == 8)
                {
                    Assert.Fail();
                }
            }
            foreach (var employeeTest in employees)
            {
                foreach (var employeeTest2 in employeeTest.Raters)
                {
                    if (employeeTest2.RaterID == 8)
                    {
                        Assert.Fail();
                    }
                }
            }

        }

        public void DoesNotAddNoEmailRater()
        {
            var employee = new Employee();
            var rater = new Rater();
            employee.EmployeeID = 5;
            employee.Email = "employee@employee.com";
            employee.HashedPassword = "iPl7Ts91pbiHdVLzz/8cotF/rUEkCg9cvW2Xzy+IXWM=";
            employee.FirstName = "Test";
            employee.LastName = "Test";
            employee.MailingAddress = "123 Fake Street";
            employee.Phone = "1231231234";
            employee.CohortID = 1;
            rater.RaterID = 8;
            rater.FirstName = "Test";
            rater.LastName = "Testington";
            rater.Email = "";
            rater.RaterRoleID = 327;

            ViewResult result = controller.Create(rater) as ViewResult;
            var raters = testData.RaterRepository.Get().ToList();
            var employees = testData.EmployeeRepository.Get().ToList();
            foreach (var raterTest in raters)
            {
                if (raterTest.RaterID == 8)
                {
                    Assert.Fail();
                }
            }
            foreach (var employeeTest in employees)
            {
                foreach (var employeeTest2 in employeeTest.Raters)
                {
                    if (employeeTest2.RaterID == 8)
                    {
                        Assert.Fail();
                    }
                }
            }

        }


        [Test]
        public void Delete()
        {
            ViewResult result = controller.Delete(1) as ViewResult;
            Assert.IsNotNull(result);
        }

        [Test]
        public void DeleteZero()
        {
            ViewResult result = controller.Delete(0) as ViewResult;
            Assert.IsNotNull(result);
        }

        [Test]
        public void DeleteNegative()
        {
            ViewResult result = controller.Delete(-1) as ViewResult;
            Assert.IsNotNull(result);
        }

        [Test]
        public void Details()
        {
            ViewResult result = controller.Details(1) as ViewResult;
            Assert.IsNotNull(result);
        }

        [Test]
        public void DetailsZero()
        {
            ViewResult result = controller.Details(0) as ViewResult;
            Assert.IsNotNull(result);
        }

        [Test]
        public void DetailsNegative()
        {
            ViewResult result = controller.Details(-1) as ViewResult;
            Assert.IsNotNull(result);
        }

        [Test]
        public void Edit()
        {
            ViewResult result = controller.Edit(1) as ViewResult;
            Assert.IsNotNull(result);
        }

        [Test]
        public void EditZero()
        {
            ViewResult result = controller.Edit(0) as ViewResult;
            Assert.IsNotNull(result);
        }


        [Test]
        public void EditNegative()
        {
            ViewResult result = controller.Edit(-1) as ViewResult;
            Assert.IsNotNull(result);
        }


        [Test]
        public void EditRater()
        {
            var rater = testData.RaterRepository.GetByID(6);
            ViewResult result = controller.Edit(rater) as ViewResult;
            Assert.True(rater.Active);
        }
    }
}
