﻿using EmployeeEvaluator.Controllers;
using EmployeeEvaluatorTests.Utility;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.SessionState;
using System.Web;
using EmployeeEvaluator.DAL;
using EmployeeEvaluator.Models;
using Model;
using EmployeeEvaluatorTests.TestData;

namespace EmployeeEvaluatorTests.Controllers
{
    [TestFixture()]
    class EmployeeControllerTests
    {
        EmployeeController controller;
        SessionStateItemCollection sessionItems;
        TestRepositoryData testData;

        [SetUp]
        public void SetUp()
        {
            testData = new TestRepositoryData();
            testData.IntializeTestData();
            controller = new EmployeeController(testData.EvaluationRepository, testData.CohortRepository, testData.EmployeeRepository, testData.EvalStageRepository, testData.MaxRaterRepository, testData.RaterRoleRepository, testData.CategoryRepository, testData.EvalTypeRepository, testData.RaterRepository);
            sessionItems = new SessionStateItemCollection();
            sessionItems["AdminLoggedIn"] = true;
            sessionItems["EmployeeLoggedIn"] = true;
            sessionItems["LoggedInEmployeeID"] = 1;
            controller.ControllerContext = new FakeControllerContext(controller, sessionItems);
        }

        [Test]
        public void Index()
        {
            ViewResult result = controller.Index() as ViewResult;
            Assert.IsNotNull(result);
        }

        [Test]
        public void IndexEmployeeDoesNotExist()
        {
            ViewResult result = controller.Index() as ViewResult;
            Assert.IsNotNull(result);
        }

        [Test]
        public void IndexNull()
        {
            ViewResult result = controller.Index() as ViewResult;
            Assert.IsNotNull(result);
        }

        [Test]
        public void List()
        {
            var repo = new InMemoryEmployeeRepository(new EvaluatorContext());
            var employee = new Employee();
            repo.Insert(employee);
            controller = new EmployeeController(repo);
            var sessionItems = new SessionStateItemCollection();
            sessionItems["AdminLoggedIn"] = true;
            controller.ControllerContext = new FakeControllerContext(controller, sessionItems);
            ViewResult result = controller.List() as ViewResult;
            Assert.IsNotNull(result);
        }

        [Test]
        public void LogOff()
        {
            ActionResult result = controller.LogOff() as ActionResult;
            Assert.IsNotNull(result);
        }

        [Test]
        public void AssignToCohort()
        {
            ActionResult result = controller.AssignToCohort(1) as ActionResult;
            Assert.IsNotNull(result);
        }

        [Test]
        public void AssignToCohortZero()
        {
            ActionResult result = controller.AssignToCohort(0) as ActionResult;
            Assert.IsNotNull(result);
        }

        [Test]
        public void AssignToCohortNegative()
        {
            ActionResult result = controller.AssignToCohort(-1) as ActionResult;
            Assert.IsNotNull(result);
        }

        [Test]
        public void Edit()
        {
            ActionResult result = controller.Edit(1) as ActionResult;
            Assert.IsNotNull(result);
        }

        [Test]
        public void EditZero()
        {
            ActionResult result = controller.Edit(0) as ActionResult;
            Assert.IsNotNull(result);
        }

        [Test]
        public void EditNegative()
        {
            ActionResult result = controller.Edit(-1) as ActionResult;
            Assert.IsNotNull(result);
        }

        [Test]
        public void ViewRaters()
        {
            ActionResult result = controller.ViewRaters() as ActionResult;
            Assert.IsNotNull(result);
        }
        [Test]
        public void DisableRaters()
        {
            ActionResult result = controller.DisableRater(new FormCollection()) as ActionResult;
            Assert.IsNotNull(result);
        }

        [Test]
        public void DisableRatersNull()
        {
            ActionResult result = controller.DisableRater(null) as ActionResult;
            Assert.IsNotNull(result);
        }
    }
}
