﻿using NUnit.Framework;
using EmployeeEvaluator.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EmployeeEvaluator.Models;
using System.Web.Mvc;
using EmployeeEvaluatorTests.Utility;
using System.Web;
using System.Web.SessionState;
using EmployeeEvaluator.DAL;
using Model;

namespace EmployeeEvaluator.Controllers.Tests
{

    [TestFixture()]
    public class HomeControllerTests
    {
        private HomeController controller;

        [SetUp]
        public void SetUp()
        {
            controller = new HomeController();
            var sessionItems = new SessionStateItemCollection();
            controller.ControllerContext = new FakeControllerContext(controller, sessionItems);
        }

        [Test]
        public void About()
        {
            ViewResult result = controller.About() as ViewResult;
            Assert.IsNotNull(result);
        }

        [Test]
        public void Contact()
        {
            ViewResult result = controller.Contact() as ViewResult;
            Assert.IsNotNull(result);
        }

        [Test]
        public void EmployeeLoginTestEmptyEmail()
        {
            var sessionItems = new SessionStateItemCollection();
            sessionItems["EmployeeLoggedIn"] = false;
            var login = new LoginViewModel();
            login.Email = "";
            login.Password = "password";
            var employeeRepository = new InMemoryEmployeeRepository(new EvaluatorContext());
            var employee = new Employee();
            employee.EmployeeID = 1;
            employee.Email = "employee@employee.com";
            employee.HashedPassword = "iPl7Ts91pbiHdVLzz/8cotF/rUEkCg9cvW2Xzy+IXWM=";
            employeeRepository.Insert(employee);

            controller = new HomeController(employeeRepository);
            controller.ControllerContext = new FakeControllerContext(controller, sessionItems);


            ViewResult result = controller.EmployeeLogin(login, null) as ViewResult;


            Assert.IsNotNull(result);
            Assert.IsFalse((bool)sessionItems["EmployeeLoggedIn"]);
        }
        [Test]
        public void EmployeeLoginTestEmptyPassword()
        {
            var sessionItems = new SessionStateItemCollection();
            sessionItems["EmployeeLoggedIn"] = false;
            var login = new LoginViewModel();
            login.Email = "employee@employee.com";
            login.Password = "";
            var employeeRepository = new InMemoryEmployeeRepository(new EvaluatorContext());
            var employee = new Employee();
            employee.EmployeeID = 1;
            employee.Email = "employee@employee.com";
            employee.HashedPassword = "iPl7Ts91pbiHdVLzz/8cotF/rUEkCg9cvW2Xzy+IXWM=";
            employeeRepository.Insert(employee);

            controller = new HomeController(employeeRepository);
            controller.ControllerContext = new FakeControllerContext(controller, sessionItems);


            ViewResult result = controller.EmployeeLogin(login, null) as ViewResult;


            Assert.IsNotNull(result);
            Assert.IsFalse((bool)sessionItems["EmployeeLoggedIn"]);
        }
        [Test]
        public void EmployeeLoginTestNullEmail()
        {
            var sessionItems = new SessionStateItemCollection();
            sessionItems["EmployeeLoggedIn"] = false;
            var login = new LoginViewModel();
            login.Email = null;
            login.Password = "password";
            var employeeRepository = new InMemoryEmployeeRepository(new EvaluatorContext());
            var employee = new Employee();
            employee.EmployeeID = 1;
            employee.Email = "employee@employee.com";
            employee.HashedPassword = "iPl7Ts91pbiHdVLzz/8cotF/rUEkCg9cvW2Xzy+IXWM=";
            employeeRepository.Insert(employee);

            controller = new HomeController(employeeRepository);
            controller.ControllerContext = new FakeControllerContext(controller, sessionItems);


            ViewResult result = controller.EmployeeLogin(login, null) as ViewResult;


            Assert.IsNotNull(result);
            Assert.IsFalse((bool)sessionItems["EmployeeLoggedIn"]);
        }

        [Test]
        public void EmployeeLoginTestNullPassword()
        {
            var sessionItems = new SessionStateItemCollection();
            sessionItems["EmployeeLoggedIn"] = false;
            var login = new LoginViewModel();
            login.Email = "employee@employee.com";
            login.Password = null;
            var employeeRepository = new InMemoryEmployeeRepository(new EvaluatorContext());
            var employee = new Employee();
            employee.EmployeeID = 1;
            employee.Email = "employee@employee.com";
            employee.HashedPassword = "iPl7Ts91pbiHdVLzz/8cotF/rUEkCg9cvW2Xzy+IXWM=";
            employeeRepository.Insert(employee);

            controller = new HomeController(employeeRepository);
            controller.ControllerContext = new FakeControllerContext(controller, sessionItems);


            ViewResult result = controller.EmployeeLogin(login, null) as ViewResult;


            Assert.IsNotNull(result);
            Assert.IsFalse((bool)sessionItems["EmployeeLoggedIn"]);
        }

        [Test]
        public void EmployeeLoginTestBothNull()
        {
            var sessionItems = new SessionStateItemCollection();
            sessionItems["EmployeeLoggedIn"] = false;
            var login = new LoginViewModel();
            login.Email = null;
            login.Password = null;
            var employeeRepository = new InMemoryEmployeeRepository(new EvaluatorContext());
            var employee = new Employee();
            employee.EmployeeID = 1;
            employee.Email = "employee@employee.com";
            employee.HashedPassword = "iPl7Ts91pbiHdVLzz/8cotF/rUEkCg9cvW2Xzy+IXWM=";
            employeeRepository.Insert(employee);

            controller = new HomeController(employeeRepository);
            controller.ControllerContext = new FakeControllerContext(controller, sessionItems);


            ViewResult result = controller.EmployeeLogin(login, null) as ViewResult;


            Assert.IsNotNull(result);
            Assert.IsFalse((bool)sessionItems["EmployeeLoggedIn"]);
        }

        [Test]
        public void EmployeeLoginTestNullEmployee()
        {
            var sessionItems = new SessionStateItemCollection();
            sessionItems["EmployeeLoggedIn"] = false;
            var login = new LoginViewModel();
            login.Email = "employee@employee.com";
            login.Password = "adsfasdf";
            var employeeRepository = new InMemoryEmployeeRepository(new EvaluatorContext());
            var employee = new Employee();
            employee.EmployeeID = 1;
            employee.Email = "employee@employee.com";
            employee.HashedPassword = "iPl7Ts91pbiHdVLzz/8cotF/rUEkCg9cvW2Xzy+IXWM=";
            employeeRepository.Insert(employee);

            controller = new HomeController(employeeRepository);
            controller.ControllerContext = new FakeControllerContext(controller, sessionItems);


            ViewResult result = controller.EmployeeLogin(login, null) as ViewResult;


            Assert.IsNotNull(result);
            Assert.IsFalse((bool)sessionItems["EmployeeLoggedIn"]);
        }


        [Test]
        public void EmployeeLoginSuccessful()
        {
            var sessionItems = new SessionStateItemCollection();
            sessionItems["EmployeeLoggedIn"] = false;
            var login = new LoginViewModel();
            login.Email = "employee1@mailinator.com";
            login.Password = "password";
            var employeeRepository = new InMemoryEmployeeRepository(new EvaluatorContext());
            var employee = new Employee();
            employee.EmployeeID = 1;
            employee.Email = "employee1@mailinator.com";
            employee.HashedPassword = "iPl7Ts91pbiHdVLzz/8cotF/rUEkCg9cvW2Xzy+IXWM=";
            employee.Active = true;
            employeeRepository.Insert(employee);

            controller = new HomeController(employeeRepository);
            controller.ControllerContext = new FakeControllerContext(controller, sessionItems);


            RedirectToRouteResult result = controller.EmployeeLogin(login, null) as RedirectToRouteResult;


            Assert.IsNotNull(result);
            Assert.IsTrue((bool)sessionItems["EmployeeLoggedIn"]);
        }
    
        [Test]
        public void AdminLoginEmptyEmail()
        {
            var sessionItems = new SessionStateItemCollection();
            sessionItems["AdminLoggedIn"] = false;
            var login = new LoginViewModel();
            login.Email = "";
            login.Password = "password";
            var adminRepository = new InMemoryAdminRepository(new EvaluatorContext());
            var admin = new Admin();
            admin.AdminID = 1;
            admin.Email = "admin@admin.com";
            admin.HashedPassword = "iPl7Ts91pbiHdVLzz/8cotF/rUEkCg9cvW2Xzy+IXWM=";
            adminRepository.Insert(admin);

            controller = new HomeController(adminRepository);
            controller.ControllerContext = new FakeControllerContext(controller, sessionItems);


            ViewResult result = controller.AdminLogin(login, null) as ViewResult;


            Assert.IsNotNull(result);
            Assert.IsFalse((bool)sessionItems["AdminLoggedIn"]);
        }

        [Test]
        public void AdminLoginEmptyPassword()
        {
            var sessionItems = new SessionStateItemCollection();
            sessionItems["AdminLoggedIn"] = false;
            var login = new LoginViewModel();
            login.Email = "admin@admin.com";
            login.Password = "";
            var adminRepository = new InMemoryAdminRepository(new EvaluatorContext());
            var admin = new Admin();
            admin.AdminID = 1;
            admin.Email = "admin@admin.com";
            admin.HashedPassword = "iPl7Ts91pbiHdVLzz/8cotF/rUEkCg9cvW2Xzy+IXWM=";
            adminRepository.Insert(admin);

            controller = new HomeController(adminRepository);
            controller.ControllerContext = new FakeControllerContext(controller, sessionItems);


            ViewResult result = controller.AdminLogin(login, null) as ViewResult;


            Assert.IsNotNull(result);
            Assert.IsFalse((bool)sessionItems["AdminLoggedIn"]);
        }

        [Test]
        public void AdminLoginNullEmail()
        {
            var sessionItems = new SessionStateItemCollection();
            sessionItems["AdminLoggedIn"] = false;
            var login = new LoginViewModel();
            login.Email = null;
            login.Password = "password";
            var adminRepository = new InMemoryAdminRepository(new EvaluatorContext());
            var admin = new Admin();
            admin.AdminID = 1;
            admin.Email = "admin@admin.com";
            admin.HashedPassword = "iPl7Ts91pbiHdVLzz/8cotF/rUEkCg9cvW2Xzy+IXWM=";
            adminRepository.Insert(admin);

            controller = new HomeController(adminRepository);
            controller.ControllerContext = new FakeControllerContext(controller, sessionItems);


            ViewResult result = controller.AdminLogin(login, null) as ViewResult;


            Assert.IsNotNull(result);
            Assert.IsFalse((bool)sessionItems["AdminLoggedIn"]);
        }

        [Test]
        public void AdminLoginNullPassword()
        {
            var sessionItems = new SessionStateItemCollection();
            sessionItems["AdminLoggedIn"] = false;
            var login = new LoginViewModel();
            login.Email = "admin@admin.com";
            login.Password = null;
            var adminRepository = new InMemoryAdminRepository(new EvaluatorContext());
            var admin = new Admin();
            admin.AdminID = 1;
            admin.Email = "admin@admin.com";
            admin.HashedPassword = "iPl7Ts91pbiHdVLzz/8cotF/rUEkCg9cvW2Xzy+IXWM=";
            adminRepository.Insert(admin);

            controller = new HomeController(adminRepository);
            controller.ControllerContext = new FakeControllerContext(controller, sessionItems);


            ViewResult result = controller.AdminLogin(login, null) as ViewResult;


            Assert.IsNotNull(result);
            Assert.IsFalse((bool)sessionItems["AdminLoggedIn"]);
        }

        [Test]
        public void AdminLoginNullAdmin()
        {
            var sessionItems = new SessionStateItemCollection();
            sessionItems["AdminLoggedIn"] = false;
            var login = new LoginViewModel();
            login.Email = "admin@admin.com";
            login.Password = "asdfasdg";
            var adminRepository = new InMemoryAdminRepository(new EvaluatorContext());
            var admin = new Admin();
            admin.AdminID = 1;
            admin.Email = "admin@admin.com";
            admin.HashedPassword = "iPl7Ts91pbiHdVLzz/8cotF/rUEkCg9cvW2Xzy+IXWM=";
            adminRepository.Insert(admin);

            controller = new HomeController(adminRepository);
            controller.ControllerContext = new FakeControllerContext(controller, sessionItems);


            ViewResult result = controller.AdminLogin(login, null) as ViewResult;


            Assert.IsNotNull(result);
            Assert.IsFalse((bool)sessionItems["AdminLoggedIn"]);
        }

        [Test]
        public void AdminLoginBothNull()
        {
            var sessionItems = new SessionStateItemCollection();
            sessionItems["AdminLoggedIn"] = false;
            var login = new LoginViewModel();
            login.Email = null;
            login.Password = null;
            var adminRepository = new InMemoryAdminRepository(new EvaluatorContext());
            var admin = new Admin();
            admin.AdminID = 1;
            admin.Email = "admin@admin.com";
            admin.HashedPassword = "iPl7Ts91pbiHdVLzz/8cotF/rUEkCg9cvW2Xzy+IXWM=";
            adminRepository.Insert(admin);

            controller = new HomeController(adminRepository);
            controller.ControllerContext = new FakeControllerContext(controller, sessionItems);


            ViewResult result = controller.AdminLogin(login, null) as ViewResult;


            Assert.IsNotNull(result);
            Assert.IsFalse((bool)sessionItems["AdminLoggedIn"]);
        }

        [Test]
        public void AdminLoginSuccessful()
        {
            var sessionItems = new SessionStateItemCollection();
            sessionItems["AdminLoggedIn"] = false;
            var login = new LoginViewModel();
            login.Email = "admin@admin.com";
            login.Password = "password";
            var adminRepository = new InMemoryAdminRepository(new EvaluatorContext());
            var admin = new Admin();
            admin.AdminID = 1;
            admin.Email = "admin@admin.com";
            admin.HashedPassword = "iPl7Ts91pbiHdVLzz/8cotF/rUEkCg9cvW2Xzy+IXWM=";
            adminRepository.Insert(admin);

            controller = new HomeController(adminRepository);
            controller.ControllerContext = new FakeControllerContext(controller, sessionItems);


            RedirectToRouteResult result = controller.AdminLogin(login, null) as RedirectToRouteResult;


            Assert.IsNotNull(result);
            Assert.IsTrue((bool)sessionItems["AdminLoggedIn"]);
        }
    }
}