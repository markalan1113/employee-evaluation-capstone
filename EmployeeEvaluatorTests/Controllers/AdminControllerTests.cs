﻿using EmployeeEvaluator.Controllers;
using EmployeeEvaluatorTests.TestData;
using EmployeeEvaluatorTests.Utility;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.SessionState;

namespace EmployeeEvaluatorTests.Controllers
{
    [TestFixture()]
    class AdminControllerTests
    {
        AdminController controller;
        SessionStateItemCollection sessionItems;
        TestRepositoryData testData;

        [SetUp]
        public void SetUp()
        {
            testData = new TestRepositoryData();
            testData.IntializeTestData();
            controller = new AdminController(testData.EvaluationRepository, testData.CohortRepository, testData.EmployeeRepository, testData.EvalStageRepository, testData.MaxRaterRepository, testData.RaterRoleRepository, testData.CategoryRepository, testData.EvalTypeRepository, testData.RaterRepository);
            sessionItems = new SessionStateItemCollection();
            sessionItems["AdminLoggedIn"] = true;
            sessionItems["LoggedInEmployeeID"] = 1;
            controller.ControllerContext = new FakeControllerContext(controller, sessionItems);
        }

        [Test]
        public void Index()
        {

            ViewResult result = controller.Index() as ViewResult;
            Assert.IsNotNull(result);
        }

        [Test]
        public void LogOff()
        {
            controller.LogOff();
            Assert.AreEqual(false, controller.HttpContext.Session["AdminLoggedIn"]);
        }

        [Test]
        public void LoadEmployees()
        {
            ViewResult result = controller.LoadEmployees() as ViewResult;
            Assert.IsNotNull(result);
        }

        [Test]
        public void ViewEmployeeCompletedEval()
        {
            ViewResult result = controller.ViewEmployeeCompletedEval(6) as ViewResult;
            Assert.IsNotNull(result);
        }

        [Test]
        public void ViewEmployeeCompletedEvalDoesNotExist()
        {
            ViewResult result = controller.ViewEmployeeCompletedEval(1) as ViewResult;
            Assert.IsNotNull(result);
        }

        [Test]
        public void ViewEmployeeCompletedEvalNegative()
        {
            ViewResult result = controller.ViewEmployeeCompletedEval(-1) as ViewResult;
            Assert.IsNotNull(result);
        }

        [Test]
        public void CreatePDFReportExistsAndTrue()
        {
            var employee1 = this.testData.EmployeeRepository.GetByID(1);
            foreach(var rater in employee1.Raters.ToList()) {
                if(rater.Evaluations.ToList().Count == 0)
                {
                    break;
                }
                rater.Evaluations.ToList()[0].Completed = true;
            }
            ViewResult result = controller.CreatePDFReport(6, true) as ViewResult;
            Assert.IsNotNull(result);
        }

        [Test]
        public void CreatePDFReportExistsAndFalse()
        {
            ViewResult result = controller.CreatePDFReport(6, false) as ViewResult;
            Assert.IsNotNull(result);
        }

        [Test]
        public void CreatePDFReportDoesNotExistTrue()
        {
            ViewResult result = controller.CreatePDFReport(1, true) as ViewResult;
            Assert.IsNotNull(result);
        }

        [Test]
        public void CreatePDFReportDoesNotExistFalse()
        {
            ViewResult result = controller.CreatePDFReport(1, false) as ViewResult;
            Assert.IsNotNull(result);
        }

        [Test]
        public void CreatePDFReportNegativeAndTrue()
        {
            ViewResult result = controller.CreatePDFReport(-1, true) as ViewResult;
            Assert.IsNotNull(result);
        }

        [Test]
        public void CreatePDFReportNegativeAndFalse()
        {
            ViewResult result = controller.CreatePDFReport(-1, false) as ViewResult;
            Assert.IsNotNull(result);
        }

        [Test]
        public void ViewEmployeeCompletedEvaluation()
        {
            ViewResult result = controller.ViewEmployeeCompletedEval(1) as ViewResult;
            Assert.IsNotNull(result);
        }

        [Test]
        public void ViewEmployeeCompletedEvaluationZero()
        {
            ViewResult result = controller.ViewEmployeeCompletedEval(0) as ViewResult;
            Assert.IsNotNull(result);
        }

        [Test]
        public void ViewEmployeeCompletedEvaluationNegative()
        {
            ViewResult result = controller.ViewEmployeeCompletedEval(-1) as ViewResult;
            Assert.IsNotNull(result);
        }
    }
}
