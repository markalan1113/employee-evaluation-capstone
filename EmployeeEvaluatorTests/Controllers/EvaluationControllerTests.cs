﻿using EmployeeEvaluator.Controllers;
using EmployeeEvaluator.DAL;
using EmployeeEvaluator.Models;
using EmployeeEvaluatorTests.TestData;
using EmployeeEvaluatorTests.Utility;
using Model;
using NUnit.Framework;
using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using System.Web.SessionState;

namespace EmployeeEvaluatorTests.Controllers
{

    [TestFixture()]
    class EvaluationControllerTests
    {
        EvaluationController controller;
        SessionStateItemCollection sessionItems;
        TestRepositoryData testData;

        [SetUp]
        public void SetUp()
        {

            testData = new TestRepositoryData();
            testData.IntializeTestData();
            controller = new EvaluationController(testData.EvaluationRepository, testData.CohortRepository, testData.EmployeeRepository, testData.EvalStageRepository, testData.MaxRaterRepository, testData.RaterRoleRepository, testData.CategoryRepository, testData.EvalTypeRepository, testData.RaterRepository);
            sessionItems = new SessionStateItemCollection();
            sessionItems["AdminLoggedIn"] = true;
            sessionItems["EmployeeLoggedIn"] = true;
            sessionItems["LoggedInEmployeeID"] = 1;
            controller.ControllerContext = new FakeControllerContext(controller, sessionItems);
        }

        [Test]
        public void Index()
        {
            ViewResult result = controller.Index() as ViewResult;
            Assert.IsNotNull(result);
        }

        [Test]
        public void Details()
        {
            ViewResult result = controller.Details(1) as ViewResult;
            Assert.IsNotNull(result);
        }

        [Test]
        public void DetailsZero()
        {
            ViewResult result = controller.Details(0) as ViewResult;
            Assert.IsNotNull(result);
        }

        [Test]
        public void DetailsNegative()
        {
            ViewResult result = controller.Details(-1) as ViewResult;
            Assert.IsNotNull(result);
        }

        [Test]
        public void CreateGet()
        {
            ViewResult result = controller.Create(1) as ViewResult;
            Assert.IsNotNull(result);
        }

        [Test]
        public void CreateGetZero()
        {
            ViewResult result = controller.Create(0) as ViewResult;
            Assert.IsNotNull(result);
        }

        [Test]
        public void CreateGetNegative()
        {
            ViewResult result = controller.Create(-1) as ViewResult;
            Assert.IsNotNull(result);
        }

        [Test]
        public void CreateEvaluation()
        {
            var evaluation = new Evaluation();
            evaluation.EvaluationID = 1;
            evaluation.CohortID = 1;
            evaluation.EmployeeID = 1;
            evaluation.RaterID = 1;
            evaluation.EvalTypeID = 1;
            evaluation.EvalStageID = 1;
            evaluation.StartDate = new DateTime(2017, 07, 29);
            evaluation.EndDate = new DateTime(2017, 08, 29);
            evaluation.Completed = false;
            var result = controller.Create(evaluation).Result;
            Assert.IsNotNull(result);
            var eval = testData.EmployeeRepository.GetByID(1);
            Assert.AreEqual(1, eval.Evaluations.Count);
        }

        [Test]
        public void CreateEvaluationWithoutStartDate()
        {
            var evaluation = new Evaluation();
            evaluation.EvaluationID = 2;
            evaluation.CohortID = 1;
            evaluation.EmployeeID = 1;
            evaluation.RaterID = 1;
            evaluation.EvalTypeID = 1;
            evaluation.EvalStageID = 1;
            evaluation.EndDate = new DateTime(2017, 06, 29);
            evaluation.Completed = false;
            var result = controller.Create(evaluation).Result;
            Assert.IsNotNull(result);
            var eval = testData.EmployeeRepository.GetByID(1);
            Assert.AreEqual(1, eval.Evaluations.Count);
        }

        [Test]
        public void CreateEvaluationWithoutEndDate()
        {
            var evaluation = new Evaluation();
            evaluation.EvaluationID = 3;
            evaluation.CohortID = 1;
            evaluation.EmployeeID = 1;
            evaluation.RaterID = 1;
            evaluation.EvalTypeID = 1;
            evaluation.EvalStageID = 1;
            evaluation.StartDate = new DateTime(2017, 05, 29);
            evaluation.Completed = false;
            var result = controller.Create(evaluation).Result;
            Assert.IsNotNull(result);
            var eval = testData.EmployeeRepository.GetByID(1);
            Assert.AreEqual(1, eval.Evaluations.Count);
        }

        [Test]
        public void CreateEvaluationWithOneAlreadyInProgressEndDate()
        {
            var evaluation = new Evaluation();
            evaluation.EvaluationID = 3;
            evaluation.CohortID = 1;
            evaluation.EmployeeID = 1;
            evaluation.RaterID = 1;
            evaluation.EvalTypeID = 1;
            evaluation.EvalStageID = 1;
            evaluation.StartDate = new DateTime(2017, 05, 29);
            evaluation.EndDate = new DateTime(2017, 06, 29);
            evaluation.Completed = false;
            var result = controller.Create(evaluation).Result;
            Assert.IsNotNull(result);
            var eval = testData.EmployeeRepository.GetByID(1);
            Assert.AreEqual(1, eval.Evaluations.Count);
        }

        [Test]
        public void StartGet()
        {
            ViewResult result = controller.Start("1") as ViewResult;
            Assert.IsNotNull(result);
        }

        [Test]
        public void StartGetZero()
        {
            ViewResult result = controller.Start("0") as ViewResult;
            Assert.IsNotNull(result);
        }

        [Test]
        public void StartGetNegative()
        {
            ViewResult result = controller.Start("-1") as ViewResult;
            Assert.IsNotNull(result);
        }

        [Test]
        public void Start()
        {
            var evaluation = new Evaluation();
            evaluation.EvaluationID = 6;
            evaluation.CohortID = 1;
            evaluation.EmployeeID = 1;
            evaluation.RaterID = 1;
            evaluation.EvalTypeID = 1;
            evaluation.EvalStageID = 1;
            evaluation.StartDate = new DateTime(2017, 05, 29);
            evaluation.EndDate = new DateTime(2017, 06, 29);
            evaluation.Completed = false;
            string salt = "92429dsdaa41e930d86c6de4ebda9602d55c39986";
            var bytes = Encoding.UTF8.GetBytes(evaluation.EvaluationID + salt);
            var hashedID = Convert.ToBase64String((new SHA256Managed()).ComputeHash(bytes));

            Regex rgx = new Regex("[^a-zA-Z0-9 -]");
            hashedID = rgx.Replace(hashedID, "");
            ViewResult result = controller.Start(hashedID) as ViewResult;
            Assert.IsNotNull(result);
        }


        [Test]
        public void Submit()
        {
            var evaluation = new Evaluation();
            evaluation.EvaluationID = 6;
            evaluation.CohortID = 1;
            evaluation.EmployeeID = 1;
            evaluation.RaterID = 1;
            evaluation.EvalTypeID = 1;
            evaluation.EvalStageID = 1;
            evaluation.StartDate = new DateTime(2017, 05, 29);
            evaluation.EndDate = new DateTime(2017, 06, 29);
            evaluation.Completed = false;
            var result = controller.Submit(evaluation);
            Assert.IsNotNull(result);
            Assert.True(testData.EvaluationRepository.Get().ToList()[1].Completed);
        }
    }

}
