﻿using EmployeeEvaluator.Controllers;
using EmployeeEvaluatorTests.TestData;
using EmployeeEvaluatorTests.Utility;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.SessionState;

namespace EmployeeEvaluatorTests.Controllers
{
    [TestFixture()]
    class CohortControllerTests
    {
        CohortController controller;
        SessionStateItemCollection sessionItems;
        TestRepositoryData testData;

        [SetUp]
        public void SetUp()
        {
            testData = new TestRepositoryData();
            testData.IntializeTestData();
            controller = new CohortController(testData.EvaluationRepository, testData.CohortRepository, testData.EmployeeRepository, testData.EvalStageRepository, testData.MaxRaterRepository, testData.RaterRoleRepository, testData.CategoryRepository, testData.EvalTypeRepository, testData.RaterRepository);
            sessionItems = new SessionStateItemCollection();
            sessionItems["AdminLoggedIn"] = true;
            sessionItems["EmployeeLoggedIn"] = true;
            sessionItems["LoggedInEmployeeID"] = 6;
            controller.ControllerContext = new FakeControllerContext(controller, sessionItems);
        }

        [Test]
        public void Index()
        {
            ActionResult result = controller.Index(1) as ActionResult;
            Assert.IsNotNull(result);
        }

        [Test]
        public void IndexZero()
        {
            ActionResult result = controller.Index(0) as ActionResult;
            Assert.IsNotNull(result);
        }

        [Test]
        public void IndexNegative()
        {
            ActionResult result = controller.Index(-1) as ActionResult;
            Assert.IsNotNull(result);
        }

        [Test]
        public void Edit()
        {
            ActionResult result = controller.Edit(1) as ActionResult;
            Assert.IsNotNull(result);
        }

        [Test]
        public void EditZero()
        {
            ActionResult result = controller.Edit(0) as ActionResult;
            Assert.IsNotNull(result);
        }

        [Test]
        public void EditNegative()
        {
            ActionResult result = controller.Edit(-1) as ActionResult;
            Assert.IsNotNull(result);
        }


        [Test]
        public void Create()
        {
            ActionResult result = controller.Create() as ActionResult;
            Assert.IsNotNull(result);
        }

        [Test]
        public void Delete()
        {
            ActionResult result = controller.Delete(1) as ActionResult;
            Assert.IsNotNull(result);
        }

        [Test]
        public void DeleteZero()
        {
            ActionResult result = controller.Delete(0) as ActionResult;
            Assert.IsNotNull(result);
        }

        [Test]
        public void DeleteNegative()
        {
            ActionResult result = controller.Delete(-1) as ActionResult;
            Assert.IsNotNull(result);
        }

        [Test]
        public void Details()
        {
            ActionResult result = controller.Details(1) as ActionResult;
            Assert.IsNotNull(result);
        }

        [Test]
        public void DetailsZero()
        {
            ActionResult result = controller.Details(0) as ActionResult;
            Assert.IsNotNull(result);
        }

        [Test]
        public void DetailsNegative()
        {
            ActionResult result = controller.Details(-1) as ActionResult;
            Assert.IsNotNull(result);
        }

        [Test]
        public void LoadEmployees()
        {
            ActionResult result = controller.LoadEmployees() as ActionResult;
            Assert.IsNotNull(result);
        }
    }
}
