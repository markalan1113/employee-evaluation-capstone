﻿CREATE TABLE [dbo].[Admins] ( 
    [AdminID] INT IDENTITY (1, 1) NOT NULL, 
    [Email] NVARCHAR (256) NOT NULL, 
    [HashedPassword]  NVARCHAR (256) NOT NULL, 
    CONSTRAINT [PK_dbo.Admins] PRIMARY KEY CLUSTERED ([AdminID] ASC) 
); 

CREATE TABLE [dbo].[Cohorts] ( 
    [CohortID] INT IDENTITY (1, 1) NOT NULL, 
    [Name] NVARCHAR (256) NOT NULL,
	[Type] NVARCHAR (256) NOT NULL, 
	[Stage] NVARCHAR (256) NOT NULL,
	[StartDate] NVARCHAR (256) NOT NULL,
	[EndDate] NVARCHAR (256) NOT NULL,
    CONSTRAINT [PK_dbo.Cohorts] PRIMARY KEY CLUSTERED ([CohortID] ASC), 
);
 
CREATE TABLE [dbo].[Employees] ( 
    [EmployeeID] INT IDENTITY (1, 1) NOT NULL, 
    [Email] NVARCHAR (256) NOT NULL, 
    [HashedPassword]  NVARCHAR (256) NULL, 
	[FirstName]  NVARCHAR (256) NOT NULL, 
	[LastName]  NVARCHAR (256) NOT NULL, 
	[MailingAddress]  NVARCHAR (256) NOT NULL, 
	[Phone]  NCHAR (10) NOT NULL, 
	[CohortID]  INT NULL, 
    CONSTRAINT [PK_dbo.Employees] PRIMARY KEY CLUSTERED ([EmployeeID] ASC), 
    CONSTRAINT [FK_dbo.Employees_dbo.Cohorts_CohortID] FOREIGN KEY ([CohortID]) REFERENCES [dbo].[Cohorts] ([CohortID]) ON DELETE CASCADE 
);