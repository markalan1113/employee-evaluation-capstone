﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Mvc;
using EmployeeEvaluator.DAL;
using EmployeeEvaluator.Migrations;
using EmployeeEvaluator.Models;
using SendGrid;

namespace EmployeeEvaluator.Controllers
{
    public class EvaluationController : Controller
    {
        private UnitOfWork unitOfWork;
        private EvaluatorContext db;
        private int RaterId;
        private Boolean isTest = false;

        public EvaluationController()
        {
            unitOfWork = new UnitOfWork();
            this.db = new EvaluatorContext();
            this.RaterId = -1;
        }

        public EvaluationController(IRepository<Evaluation> repository)
        {
            unitOfWork = new UnitOfWork();
            unitOfWork.EvaluationRepository = repository;
            this.RaterId = -1;
        }

        public EvaluationController(IRepository<Evaluation> evaluationRepository, IRepository<Cohort> cohortRepository, IRepository<Employee> employeeRepository, IRepository<EvalStage> stageRepository, IRepository<MaxRater> maxRaterRepository, IRepository<RaterRole> raterRoleRepository, IRepository<Category> categoryRepository, IRepository<EvalType> typeRepository, IRepository<Rater> raterRepository)
        {
            unitOfWork = new UnitOfWork();
            unitOfWork.EvaluationRepository = evaluationRepository;
            unitOfWork.CohortRepository = cohortRepository;
            unitOfWork.EmployeeRepository = employeeRepository;
            unitOfWork.StageRepository = stageRepository;
            unitOfWork.MaxRaterRepository = maxRaterRepository;
            unitOfWork.RaterRoleRepository = raterRoleRepository;
            unitOfWork.CategoryRepository = categoryRepository;
            unitOfWork.TypeRepository = typeRepository;
            unitOfWork.RaterRepository = raterRepository;
            this.RaterId = -1;
            this.isTest = true;
        }

        // GET: Evaluation
        public ActionResult Index()
        {
            if (authenticatedAsEmployee())
            {
                using (unitOfWork)
                {
                    var evaluationViewModels = new List<EvaluationViewModel>();
                    var currentEmployee = unitOfWork.EmployeeRepository.GetByID((int)Session["LoggedInEmployeeID"]);
                    var evaluations = new List<Evaluation>();

                    var numberOfSupervisors = 0;
                    var numberOfCoworkers = 0;
                    var numberOfSupervisees = 0;

                    if (currentEmployee.Raters == null)
                    {
                        TempData["Error"] = "You do not have enough raters to complete an evaluation.";
                        return View(evaluationViewModels);
                    }
                    else
                    {
                        evaluations = currentEmployee.Evaluations.Where(e => e.Completed == false && e.RaterID == null).ToList();
                    }

                    foreach (var rater in currentEmployee.Raters)
                    {
                        if (rater.Active)
                        {
                            var raterRole = unitOfWork.RaterRoleRepository.GetByID(rater.RaterRoleID);
                            if (raterRole.RoleName != null)
                            {
                                if (raterRole.RoleName.Equals("Supervisor"))
                                {
                                    numberOfSupervisors++;
                                }
                                else if (raterRole.RoleName.Equals("Coworker"))
                                {
                                    numberOfCoworkers++;
                                }
                                else if (raterRole.RoleName.Equals("Supervisee"))
                                {
                                    numberOfSupervisees++;
                                }
                            }
                        }

                    }
                    var maxRaters = unitOfWork.MaxRaterRepository.Get().ToList();

                    if (numberOfSupervisors < maxRaters[0].Max)
                    {
                        TempData["Error"] = "You do not have enough " + maxRaters[0].RaterRole.RoleName + "s to complete an evaluation.";
                        return View(evaluationViewModels);
                    }
                    if (numberOfSupervisees < maxRaters[1].Max)
                    {
                        TempData["Error"] = "You do not have enough " + maxRaters[1].RaterRole.RoleName + "s to complete an evaluation.";
                        return View(evaluationViewModels);
                    }
                    if (numberOfCoworkers < maxRaters[2].Max)
                    {
                        TempData["Error"] = "You do not have enough " + maxRaters[2].RaterRole.RoleName + "s to complete an evaluation.";
                        return View(evaluationViewModels);
                    }

                    var cohort = unitOfWork.CohortRepository.GetByID(currentEmployee.CohortID);
                    if (!cohort.Stable)
                    {
                        TempData["Error"] = "Your cohort is not stable.  Please contact your administrator.";
                        return View(evaluationViewModels);
                    }
                    if (evaluations != null)
                    {
                        foreach (var evaluation in evaluations)
                        {
                            var evaluationViewModel = new EvaluationViewModel();
                            evaluationViewModel.Evaluation = evaluation;
                            evaluationViewModel.EvalType = unitOfWork.TypeRepository.GetByID(evaluation.EvalTypeID);
                            evaluationViewModel.EvalStage = unitOfWork.StageRepository.GetByID(evaluation.EvalStageID);
                            evaluationViewModels.Add(evaluationViewModel);
                        }
                        return View(evaluationViewModels);
                    }
                }


                return View();
            }
            return RedirectToAction("EmployeeLogin", "Home");

        }

        // GET: Evaluation/Details/5
        public ActionResult Details(int? id)
        {
            using (unitOfWork)
            {
                if (id == null)
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                var evaluationViewModel = new EvaluationViewModel();
                var evaluation = unitOfWork.EvaluationRepository.GetByID(id);

                if (evaluation == null)
                {
                    TempData["Error"] = "Your cohort is not stable.  Please contact your administrator.";
                    return View();
                }
                evaluationViewModel.Evaluation = evaluation;
                evaluationViewModel.EvalType = unitOfWork.TypeRepository.GetByID(evaluation.EvalTypeID);
                return View(evaluationViewModel);
            }
            
        }

        // GET: Evaluation/Create
        [HttpGet]
        public ActionResult Create(int id)
        {
            if (this.authenticatedAsAdmin())
            {
                using (unitOfWork)
                {
                    if (TempData.ContainsKey("ModelState"))
                        ModelState.Merge((ModelStateDictionary)TempData["ModelState"]);
                    var evaluationViewModel = new EvaluationViewModel();
                    evaluationViewModel.Evaluation = new Evaluation();
                    evaluationViewModel.Evaluation.CohortID = id;
                    evaluationViewModel.EvalTypes =
                        unitOfWork.TypeRepository.Get().ToList().Select(x => new SelectListItem
                        {
                            Value = x.EvalTypeID.ToString(),
                            Text = x.TypeName
                        });
                    evaluationViewModel.EvalStages =
                        unitOfWork.StageRepository.Get().ToList().Select(x => new SelectListItem
                        {
                            Value = x.EvalStageID.ToString(),
                            Text = x.StageName
                        });
                    evaluationViewModel.Evaluation.StartDate = DateTime.Now;
                    evaluationViewModel.Evaluation.EndDate = DateTime.Now;


                    return View(evaluationViewModel);
                }
            }
            return RedirectToAction("AdminLogin", "Home");
        }

        // POST: Evaluation/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "EvalTypeID,EvalStageID,Type,Stage,CohortID,Completed,StartDate,EndDate")] Evaluation evaluation)
        {
            if (authenticatedAsAdmin())
            {
                using (unitOfWork)
                {
                    var cohort = unitOfWork.CohortRepository.GetByID(evaluation.CohortID);
                    var evaluations = unitOfWork.EvaluationRepository.Get()
                        .Where(e => e.CohortID == evaluation.CohortID && e.Completed && e.Pending && e.EvalTypeID == evaluation.EvalTypeID).ToList();
                    var employees =
                        unitOfWork.EmployeeRepository.Get().Where(e => e.CohortID == evaluation.CohortID).ToList();
                    var stage = unitOfWork.StageRepository.GetByID(evaluation.EvalStageID);
                    var evaluationsInProgress =
                        unitOfWork.EvaluationRepository.Get()
                            .Where(e => e.CohortID == evaluation.CohortID && e.EvalTypeID == evaluation.EvalTypeID && !e.Completed && !e.Pending)
                            .ToList();
                    Evaluation previousEval = null;
                    var previousEvals = unitOfWork.EvaluationRepository.Get()
                                .Where(
                                    e =>
                                        e.CohortID == evaluation.CohortID && e.Completed &&
                                        e.EvalTypeID == evaluation.EvalTypeID &&
                                        e.Pending)
                                .ToList();
                    evaluation.EvalStage = unitOfWork.StageRepository.GetByID(evaluation.EvalStageID);
                    if (!evaluation.EvalStage.StageName.Equals("Baseline"))
                    {
                        if (evaluation.EvalStage.StageName.Equals("Formative"))
                        {
                            previousEval =
                            unitOfWork.EvaluationRepository.Get()
                                .Where(
                                    e =>
                                        e.CohortID == evaluation.CohortID && e.Completed &&
                                        e.EvalTypeID == evaluation.EvalTypeID &&
                                        e.EvalStage.StageName.Equals("Baseline"))
                                .FirstOrDefault();
                        }
                        if (evaluation.EvalStage.StageName.Equals("Summative"))
                        {
                            previousEval =
                            unitOfWork.EvaluationRepository.Get()
                                .Where(
                                    e =>
                                        e.CohortID == evaluation.CohortID && e.Completed &&
                                        e.EvalTypeID == evaluation.EvalTypeID &&
                                        e.EvalStage.StageName.Equals("Formative"))
                                .FirstOrDefault();
                        }
                        
                    }

                    if (previousEval != null)
                    {
                        if (DateTime.Compare(previousEval.EndDate, evaluation.StartDate) > 0)
                        {
                            TempData["ModelState"] = ModelState;
                            TempData["Confirmation"] = "Start date cannot be before the previous stage's end date.";
                            return RedirectToAction("Create", "Evaluation", new { id = evaluation.CohortID });
                        }
                    }

                    foreach (var eval in evaluationsInProgress)
                    {
                        if (eval.EvalTypeID == evaluation.EvalTypeID)
                        {
                            TempData["Confirmation"] =
                                "This cohort has an evaluation of this type in progress. Please wait for current evaluation to end.";
                            return RedirectToAction("Index", "Cohort");
                        }
                    }

                    if (cohort.Stable == false)
                    {
                        TempData["Confirmation"] = "This cohort is currently unstable. Cannot start evaluation";
                        return RedirectToAction("Index", "Cohort");
                    }

                    if (DateTime.Compare(evaluation.StartDate.Date, DateTime.Now.Date) < 0)
                    {
                        TempData["ModelState"] = ModelState;
                        TempData["Confirmation"] = "Start date cannot be before the current date.";
                        return RedirectToAction("Create", "Evaluation", new {id = evaluation.CohortID});
                    }
                    if (DateTime.Compare(evaluation.StartDate.Date, evaluation.EndDate.Date) == 0 ||
                        DateTime.Compare(evaluation.StartDate.Date, evaluation.EndDate.Date) > 0)
                    {
                        TempData["ModelState"] = ModelState;
                        TempData["Confirmation"] = "Start date cannot be after or the same as end date.";
                        return RedirectToAction("Create", "Evaluation", new {id = evaluation.CohortID});
                    }

                    foreach (var eval in evaluations)
                    {
                        if (eval.EvalTypeID == evaluation.EvalTypeID)
                        {
                            if (eval.EvalStage.StageName.Equals("Baseline") && !stage.StageName.Equals("Formative"))
                            {
                                TempData["ModelState"] = ModelState;
                                TempData["Confirmation"] =
                                    "The evaluation of that type is currently in the Baseline stage and can only be set to Formative.";
                                return RedirectToAction("Create", "Evaluation", new {id = evaluation.CohortID});
                            }
                            if (eval.EvalStage.StageName.Equals("Formative") && !stage.StageName.Equals("Summative"))
                            {
                                TempData["ModelState"] = ModelState;
                                TempData["Confirmation"] =
                                    "The evaluation of that type is currently in the Formative stage and can only be set to Summative.";
                                return RedirectToAction("Create", "Evaluation", new {id = evaluation.CohortID});
                            }
                        }
                    }

                    evaluation.Completed = false;

                    if (evaluations.Count == 0)
                    {
                        if (!stage.StageName.Equals("Baseline"))
                        {
                            TempData["ModelState"] = ModelState;
                            TempData["Confirmation"] =
                                "An evaluation of that type is currently not in the database and can only be set to Baseline.";
                            return RedirectToAction("Create", "Evaluation", new {id = evaluation.CohortID});
                        }
                    }
                    else
                    {
                        evaluation.EvalStageID =
                            unitOfWork.StageRepository.Get()
                                .Where(s => s.StageName.Equals(stage.StageName))
                                .FirstOrDefault()
                                .EvalStageID;
                    }

                    evaluation.Pending = false;

                    foreach (var employee in employees.ToList())
                    {
                        if (employee.Active)
                        {
                            employee.Evaluations.Add(copyEvaluation(evaluation));
                            unitOfWork.EmployeeRepository.Update(employee);
                            unitOfWork.Save();
                        }
                    }

                    foreach (var employee in employees)
                    {
                        if (!this.isTest) {
                            await this.SendEmailEmployee(employee, evaluation);
                        }
                    }

                    foreach (var eval in previousEvals)
                    {
                        eval.Pending = false;
                        unitOfWork.EvaluationRepository.Update(eval);
                        unitOfWork.Save();
                    }

                    TempData["Confirmation"] = "Evaluation successfully updated.  Stage set to " + stage.StageName;

                    return RedirectToAction("Index", "Cohort");

                }
            }
            else
            {
                return RedirectToAction("AdminLogin", "Home");
            }
            

        }

        // Probably won't be used.


        // GET: Evaluation/Edit/5
        //public ActionResult Edit(int? id)
        //{
        //    using (unitOfWork)
        //    {
        //        if (id == null)
        //            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //        var evaluationViewModel = new EvaluationViewModel();
        //        evaluationViewModel.Evaluation = unitOfWork.EvaluationRepository.GetByID(id);
        //        evaluationViewModel.EvalType = unitOfWork.TypeRepository.GetByID(evaluationViewModel.Evaluation.EvalTypeID);
        //        evaluationViewModel.EvalTypes = unitOfWork.TypeRepository.Get().ToList().Select(x => new SelectListItem
        //        {
        //            Value = x.EvalTypeID.ToString(),
        //            Text = x.TypeName
        //        });
        //        if (evaluationViewModel.Evaluation == null)
        //            return HttpNotFound();
        //        return View(evaluationViewModel);
        //    }

        //}

        //// POST: Evaluation/Edit/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit([Bind(Include = "EvaluationID,Type,Stage")] Evaluation evaluation)
        //{
        //    using (unitOfWork)
        //    {
        //        if (ModelState.IsValid)
        //        {
        //            unitOfWork.EvaluationRepository.Update(evaluation);
        //            unitOfWork.Save();
        //            return RedirectToAction("Index");
        //        }
        //        return View(evaluation);
        //    }

        //}

        // GET: Evaluation/Delete/5
        //public ActionResult Delete(int? id)
        //{
        //    using (unitOfWork)
        //    {
        //        if (id == null)
        //            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //        var evaluationViewModel = new EvaluationViewModel();
        //        evaluationViewModel.Evaluation = unitOfWork.EvaluationRepository.GetByID(id);
        //        evaluationViewModel.EvalType = unitOfWork.TypeRepository.GetByID(evaluationViewModel.Evaluation.EvalTypeID);
        //        if (evaluationViewModel.Evaluation == null)
        //            return HttpNotFound();
        //        return View(evaluationViewModel);
        //    }
        //}

        //// POST: Evaluation/Delete/5
        //[HttpPost]
        //[ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    using (unitOfWork)
        //    {
        //        var evaluation = unitOfWork.EvaluationRepository.GetByID(id);
        //        unitOfWork.EvaluationRepository.Delete(evaluation);
        //        unitOfWork.Save();
        //        return RedirectToAction("Index");
        //    }

        //}

        [HttpGet]
        public ActionResult Start(string id)
        {
            Evaluation evaluation = null;
            string salt = "92429dsdaa41e930d86c6de4ebda9602d55c39986";
            foreach (var eval in unitOfWork.EvaluationRepository.Get().ToList())
            {
                var bytes = Encoding.UTF8.GetBytes(eval.EvaluationID + salt);
                var hashedID = Convert.ToBase64String((new SHA256Managed()).ComputeHash(bytes));

                Regex rgx = new Regex("[^a-zA-Z0-9 -]");
                hashedID = rgx.Replace(hashedID, "");

                if (hashedID == id)
                {
                    evaluation = eval;
                    id = Convert.ToString(evaluation.EvaluationID);
                    break;
                }
            }
            if (evaluation == null)
            {
                return View("Error");
            }
            var viewModel = new StartEvaluationViewModel();
            
            viewModel.Evaluation = evaluation;
            if (evaluation.Completed)
            {
                TempData["Error"] = "This evaluation has already been completed.";
                return View(viewModel);
            }
            var categories = unitOfWork.CategoryRepository.Get().Where(c => c.EvalTypeID == evaluation.EvalTypeID).ToList();

            

            var categoriesList = new List<Category>();
            var questionsList = new List<Question>();

            foreach (var category in categories)
            {
                categoriesList.Add(category);
                foreach (var question in category.Questions)
                {
                    questionsList.Add(question);
                }
            }

            if (evaluation != null)
            {
                if (evaluation.Questions.Count == 0)
                {
                    foreach (var category in categoriesList)
                    {
                        category.EvalType = unitOfWork.TypeRepository.GetByID(category.EvalTypeID);
                        foreach (var question in questionsList.Where(q => q.CategoryID == category.CategoryID))
                        {
                            if (!question.InEvaluation)
                            {
                                var questionToAdd = new Question();

                                questionToAdd.CategoryID = category.CategoryID;
                                questionToAdd.QuestionText = question.QuestionText;
                                questionToAdd.InEvaluation = true;
                                unitOfWork.QuestionRepository.Insert(questionToAdd);
                                evaluation.Questions.Add(questionToAdd);
                                category.Questions.Add(questionToAdd);
                            }
                        }
                        unitOfWork.CategoryRepository.Update(category);
                        unitOfWork.EvaluationRepository.Update(evaluation);
                        unitOfWork.Save();

                    }
                }
                else
                {
                    foreach (var category in categoriesList)
                    {
                        category.EvalType = unitOfWork.TypeRepository.GetByID(category.EvalTypeID);
                    }
                }
                
            }
            else if (evaluation == null)
            {
                return View("~/Views/Shared/Error.cshtml");
            }

            var displayCategories = new List<Category>();
            foreach (var category in unitOfWork.CategoryRepository.Get().Where(c => c.EvalTypeID == evaluation.EvalTypeID).ToList())
            {
                var categoryToDisplay = category;
                categoryToDisplay.EvalType = category.EvalType;
                categoryToDisplay.Questions = evaluation.Questions.Where(q => q.CategoryID == category.CategoryID).ToList();
                displayCategories.Add(category);
            }

            viewModel.Evaluation = evaluation;
            viewModel.Categories = displayCategories;
            this.RaterId = Convert.ToInt32(id);
            return View(viewModel);

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Start(FormCollection form)
        {
            using (unitOfWork)
            {
                var evaluation = unitOfWork.EvaluationRepository.GetByID(Convert.ToInt32(form["Evaluation.EvaluationID"]));
                var questions = evaluation.Questions;

                foreach (var question in questions)
                {
                    var category = unitOfWork.CategoryRepository.GetByID(question.CategoryID);
                    if (evaluation.EvalType.TypeName.Equals("Type 1") && form[category.CategoryName + question.QuestionText] != null)
                    {
                        question.SelectedAnswerValue = Convert.ToInt32(form[category.CategoryName + question.QuestionText]);
                    }
                    else if (evaluation.EvalType.TypeName.Equals("Type 2") && form[category.CategoryName + question.QuestionText] != null)
                    {
                        question.SelectedAnswerValue = Convert.ToInt32(form[category.CategoryName + question.QuestionText]);
                    }
                }
                unitOfWork.EvaluationRepository.Update(evaluation);
                unitOfWork.Save();
                if (form.Count - 2 <= questions.Count)
                {
                    TempData["Error"] = "Please select an answer for every question.";
                    string salt = "92429dsdaa41e930d86c6de4ebda9602d55c39986";
                    var bytes = Encoding.UTF8.GetBytes(evaluation.EvaluationID + salt);
                    var hashedID = Convert.ToBase64String((new SHA256Managed()).ComputeHash(bytes));

                    Regex rgx = new Regex("[^a-zA-Z0-9 -]");
                    hashedID = rgx.Replace(hashedID, "");
                    return RedirectToAction("Start", new { id = hashedID });
                }

                unitOfWork.Save();
                evaluation.Completed = true;
                unitOfWork.EvaluationRepository.Update(evaluation);
                unitOfWork.Save();

                var employeesInCohort =
                    unitOfWork.EmployeeRepository.Get()
                        .Where(e => e.CohortID == Convert.ToInt32(form["Evaluation.CohortID"]))
                        .ToList();
                var evaluationsAdded = new List<Evaluation>();
                var employeeToUpdate = new Employee();
                if (evaluation.RaterID == null)
                {
                    foreach (var employee in employeesInCohort)
                    {
                        foreach (var eval in employee.Evaluations)
                        {
                            if (eval.EvaluationID == Convert.ToInt32(form["Evaluation.EvaluationID"]))
                            {
                                employeeToUpdate = employee;
                                foreach (var rater in employee.Raters)
                                {
                                    var evalToAdd = new Evaluation();
                                    var originalEval =
                                        unitOfWork.EvaluationRepository.GetByID(
                                            Convert.ToInt32(form["Evaluation.EvaluationID"]));
                                    evalToAdd.CohortID = originalEval.CohortID;
                                    evalToAdd.StartDate = originalEval.StartDate;
                                    evalToAdd.EndDate = originalEval.EndDate;
                                    evalToAdd.EvalTypeID = originalEval.EvalTypeID;
                                    evalToAdd.EvalStageID = originalEval.EvalStageID;
                                    evalToAdd.Completed = false;
                                    evalToAdd.Pending = false;
                                    evalToAdd.Questions.Clear();
                                    unitOfWork.EvaluationRepository.Insert(evalToAdd);
                                    rater.Evaluations.Add(evalToAdd);
                                    unitOfWork.Save();
                                    evaluationsAdded.Add(evalToAdd);
                                }
                            }
                        }
                    }

                    foreach (var eval in evaluationsAdded)
                    {
                        eval.EmployeeID = employeeToUpdate.EmployeeID;
                        unitOfWork.EvaluationRepository.Update(eval);
                        unitOfWork.Save();
                    }
                    
                    return RedirectToAction("Submit", evaluation);
                }
                else
                {
                    var uncompletedEvaluations =
                        unitOfWork.EvaluationRepository.Get()
                            .Where(
                                e =>
                                    e.CohortID == evaluation.CohortID && !e.Completed &&
                                    e.EvalTypeID == evaluation.EvalTypeID && e.StartDate == evaluation.StartDate && e.EndDate == evaluation.EndDate).ToList();
                    if (uncompletedEvaluations.Count() == 0)
                    {
                        foreach (var eval in unitOfWork.EvaluationRepository.Get().Where(e => e.CohortID == evaluation.CohortID && e.EvalTypeID == evaluation.EvalTypeID && e.StartDate == evaluation.StartDate && e.EndDate == evaluation.EndDate && e.Completed).ToList())
                        {
                            if (eval.EvalStage.StageName.Equals("Summative"))
                            {
                                eval.Pending = false;
                                unitOfWork.EvaluationRepository.Update(eval);
                                unitOfWork.Save();
                            }
                            else
                            {
                                eval.Pending = true;
                                unitOfWork.EvaluationRepository.Update(eval);
                                unitOfWork.Save();
                            }
                        }
                    }
                    return View("Submit");
                }
                
            }
            return View();

        }
        public async Task<ActionResult> Submit([Bind(Include = "EvaluationID, EmployeeID, EvalStageID, EvalTypeID")]Evaluation evaluation)
        {
            using (unitOfWork)
            {
                var evaluationsToSend =
                    unitOfWork.EvaluationRepository.Get()
                        .Where(e => !e.Completed && e.EmployeeID == evaluation.EmployeeID && e.RaterID != null && e.EvalStageID == evaluation.EvalStageID && e.EvalTypeID == evaluation.EvalTypeID)
                        .ToList();
                var employee = unitOfWork.EmployeeRepository.GetByID(Int32.Parse(Session["LoggedInEmployeeID"].ToString()));
                if (employee == null)
                {
                    TempData["Failed"] = "Sorry no raters could be found.";
                    return RedirectToAction("Index", "Evaluation");
                }
                evaluation = unitOfWork.EvaluationRepository.GetByID(evaluation.EvaluationID);
                evaluation.Completed = true;
                unitOfWork.EvaluationRepository.Update(evaluation);
                unitOfWork.Save();
                foreach (var eval in evaluationsToSend)
                {
                    var rater = unitOfWork.RaterRepository.GetByID(eval.RaterID);
                    if (!this.isTest) {
                        await SendEmailRater(rater, eval);
                    }
                }
                    TempData["Success"] = "Emails successfully sent!";
                return RedirectToAction("Index", "Employee");
            }
        }

        private void helper(Evaluation evaluation)
        {
            unitOfWork.EvaluationRepository.Insert(evaluation);
        }

        public async Task<ActionResult> SendEmailRater([Bind(Include = "EvaulationID")] Rater rater, Evaluation evaluation)
        {
            var message = new SendGridMessage();
            message.AddTo(rater.Email);

            string salt = "92429dsdaa41e930d86c6de4ebda9602d55c39986";
            var bytes = Encoding.UTF8.GetBytes(evaluation.EvaluationID + salt);
            var hashedID = Convert.ToBase64String((new SHA256Managed()).ComputeHash(bytes));

            Regex rgx = new Regex("[^a-zA-Z0-9 -]");
            hashedID = rgx.Replace(hashedID, "");
            var callbackUrl = Url.Action("Start", "Evaluation", new { id = hashedID }, protocol: Request.Url.Scheme);
            message.From = new MailAddress(
                "CapstoneEmployeeEvaluation@gmail.com", "Employee Evaluations");
            message.Subject = "Attention Rater! Employee Has Completed an Evaluation.";

            // var body =
            //    "Hello,\r\n\r\n An employee that you are a rater for has completed their evaluation. Please login and fill out the evaluation by the posted deadline date. Thank you. <a href=\"" +
            //    callbackUrl + "\">Complete Evaluation</a>. \r\n";
            var body =
                    "Hello,<br><br> Please complete the evaluation by the due date. Thank you." +
                    "<br>" + "Evaluation Type: " + evaluation.EvalType.TypeName +
                    "<br>" + "Evaluation Stage: " + evaluation.EvalStage.StageName +
                    "<br>" + "Start Date: " + evaluation.StartDate.Date +
                    "<br>" + "End Date: " + evaluation.EndDate.Date + "<br><a href=\"" +
                    callbackUrl + "\">Complete Evaluation</a>. <br>";
            message.Text = body;
            message.Html = body;

            var credentials = new NetworkCredential(
                ConfigurationManager.AppSettings["mailAccount"],
                ConfigurationManager.AppSettings["mailPassword"]
            );

            // Create a Web transport for sending email.
            var transportWeb = new Web(credentials);

            // Send the email.
            if (transportWeb != null)
                await transportWeb.DeliverAsync(message);
            return RedirectToAction("Submit", "Evaluation");

        }

        public async Task<ActionResult> SendEmailEmployee([Bind(Include = "EmployeeID")] Employee employee, Evaluation evaluation)
        {
                evaluation.EvalStage = unitOfWork.StageRepository.GetByID(evaluation.EvalStageID);
                evaluation.EvalType = unitOfWork.TypeRepository.GetByID(evaluation.EvalTypeID);
                var message = new SendGridMessage();
                message.AddTo(employee.Email);
                var callbackUrl = Url.Action("Index", "Employee", null, protocol: Request.Url.Scheme);
                message.From = new MailAddress(
                    "CapstoneEmployeeEvaluation@gmail.com", "Employee Evaluations");
                message.Subject = "Attention Employee! There is a new evaluation for you to complete!";
                var body =
                    "Hello,<br><br> Please complete the evaluation by the due date. As soon as you complete the evaluation your raters will be notified. Thank you." +
                    "<br>" + "Evaluation Type: " + evaluation.EvalType.TypeName +
                    "<br>" + "Evaluation Stage: " + evaluation.EvalStage.StageName +
                    "<br>" + "Start Date: " + evaluation.StartDate.Date +
                    "<br>" + "End Date: " + evaluation.EndDate.Date + "<br><a href=\"" +
                    callbackUrl + "\">Complete Evaluation</a>. <br>";
            message.Text = body;
                message.Html = body;

                var credentials = new NetworkCredential(
                    ConfigurationManager.AppSettings["mailAccount"],
                    ConfigurationManager.AppSettings["mailPassword"]
                );

                // Create a Web transport for sending email.
                var transportWeb = new Web(credentials);

                // Send the email.
                if (transportWeb != null)
                    await transportWeb.DeliverAsync(message);
                return RedirectToAction("Submit", "Evaluation");
        }

        public ActionResult Completed()
        {
            using (unitOfWork)
            {
                var evaluationViewModels = new List<EvaluationViewModel>();
                var currentEmployee = db.Employees.Find((int)Session["LoggedInEmployeeID"]);
                var evaluations = currentEmployee.Evaluations.Where(e => e.Completed == true).ToList();

                if (evaluations != null && evaluations.Count > 0)
                {
                    foreach (var evaluation in evaluations)
                    {
                        var evaluationViewModel = new EvaluationViewModel();
                        evaluationViewModel.Evaluation = evaluation;
                        evaluationViewModel.EvalType = unitOfWork.TypeRepository.GetByID(evaluation.EvalTypeID);
                        evaluationViewModels.Add(evaluationViewModel);
                    }
                    return View(evaluationViewModels);
                } else
                {
                    TempData["Error"] = "You do not have any completed evaluations.";
                    return View(evaluationViewModels);
                }
            }

        }


        public ActionResult OpenCompleted(int id)
        {
            using (unitOfWork)
            {
                var evaluation = db.Evaluations.Where(e => e.EvaluationID == id).FirstOrDefault();
                return View(evaluation.Questions);
            }
        }

        private Evaluation copyEvaluation(Evaluation evaluation)
        {
            var raterEvaluation = new Evaluation
            {
                CohortID = evaluation.CohortID,
                EvalStageID = evaluation.EvalStageID,
                Questions = evaluation.Questions,
                EvalTypeID = evaluation.EvalTypeID,
                StartDate = evaluation.StartDate,
                EndDate = evaluation.EndDate,
                Completed = false
            };

            return raterEvaluation;
        }



        protected override void Dispose(bool disposing)
        {
            using (unitOfWork)
            {
                if (disposing)
                    unitOfWork.Dispose();
                base.Dispose(disposing);
            }

        }

        private bool authenticatedAsAdmin()
        {
            if (!(bool)Session["AdminLoggedIn"])
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private bool authenticatedAsEmployee()
        {
            if (!(bool)Session["EmployeeLoggedIn"])
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
