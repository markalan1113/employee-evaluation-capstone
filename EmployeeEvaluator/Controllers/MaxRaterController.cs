﻿using System.Linq;
using System.Net;
using System.Web.Mvc;
using EmployeeEvaluator.DAL;
using EmployeeEvaluator.Models;

namespace EmployeeEvaluator.Controllers
{
    public class MaxRaterController : Controller
    {
        private static UnitOfWork unitOfWork;

        public MaxRaterController()
        {
            unitOfWork = new UnitOfWork();
        }
        // GET: MaxRater
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ChangeMaximums()
        {
            if (this.authenticatedAsAdmin())
            {
                return View();
            }
            return RedirectToAction("AdminLogin", "Home");
        }

      
        public ActionResult ChangeMaximumSupervisors()
        {
            if (this.authenticatedAsAdmin())
            {
                var maxSupervisors = unitOfWork.MaxRaterRepository.Get().FirstOrDefault(
                  m => m.RaterRole.RoleName == "Supervisor");

                return View(maxSupervisors);
            }
            return RedirectToAction("AdminLogin", "Home");
        }

        [HttpPost]
        public ActionResult ChangeMaximumSupervisors([Bind(Include = "MaxRaterID, Max, RaterRoleID")] MaxRater maxSupervisors)
        {
            if (!this.isEvaluationInProgress())
            {
                unitOfWork.MaxRaterRepository.Update(maxSupervisors);
                unitOfWork.Save();
                TempData["Confirmation"] ="Max supervisors successfully updated.";
                return RedirectToAction("ChangeMaximumSupervisors", "MaxRater");
            }
            TempData["Confirmation"] =
                "Evaluations are currently in progress. Please wait for them to end before changing maximum raters.";
            return RedirectToAction("ChangeMaximumSupervisors", "MaxRater");
        }

        public ActionResult ChangeMaximumSupervisees()
        {
            if (this.authenticatedAsAdmin())
            {
                var maxSupervisees= unitOfWork.MaxRaterRepository.Get().FirstOrDefault(
                  m => m.RaterRole.RoleName == "Supervisee");

                return View(maxSupervisees);
            }
            return RedirectToAction("AdminLogin", "Home");
        }

        [HttpPost]
        public ActionResult ChangeMaximumSupervisees([Bind(Include = "MaxRaterID, Max, RaterRoleID")] MaxRater maxSupervisees)
        {
            if (!this.isEvaluationInProgress())
            {
                unitOfWork.MaxRaterRepository.Update(maxSupervisees);
                unitOfWork.Save();
                TempData["Confirmation"] = "Max supervisees successfully updated.";
                return RedirectToAction("ChangeMaximumSupervisees", "MaxRater");
            }

            TempData["Confirmation"] = "Evaluations are currently in progress. Please wait for them to end before changing maximum raters.";
            return RedirectToAction("ChangeMaximumSupervisees", "MaxRater");
        }

        public ActionResult ChangeMaximumCoworkers()
        {
            if (this.authenticatedAsAdmin())
            {
                var maxCoworkers = unitOfWork.MaxRaterRepository.Get().FirstOrDefault(
                  m => m.RaterRole.RoleName == "Coworker");

                return View(maxCoworkers);
            }
            return RedirectToAction("AdminLogin", "Home");
        }

        [HttpPost]
        public ActionResult ChangeMaximumCoworkers([Bind(Include = "MaxRaterID, Max, RaterRoleID")] MaxRater maxCoworkers)
        {
            if (!this.isEvaluationInProgress())
            {
                unitOfWork.MaxRaterRepository.Update(maxCoworkers);
                unitOfWork.Save();
                TempData["Confirmation"] = "Max coworkers successfully updated.";
                return RedirectToAction("ChangeMaximumCoworkers", "MaxRater");
            }

            TempData["Confirmation"] = "Evaluations are currently in progress. Please wait for them to end before changing maximum raters.";
            return RedirectToAction("ChangeMaximumCoworkers", "MaxRater");
        }

        private bool isEvaluationInProgress()
        {
            var evaluations = unitOfWork.EvaluationRepository.Get().ToList();

            foreach (var eval in evaluations)
            {
                if (eval.Pending || !eval.Completed)
                {
                    return true;
                }
            }
            return false;
        }

        private bool authenticatedAsAdmin()
        {
            if (!(bool)Session["AdminLoggedIn"])
            {
                return false;
            }
            return true;   
        }
    }
}