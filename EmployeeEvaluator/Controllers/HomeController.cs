﻿using EmployeeEvaluator.DAL;
using EmployeeEvaluator.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace EmployeeEvaluator.Controllers
{
    public class HomeController : Controller
    {
        public UnitOfWork unitOfWork;

        public HomeController()
        {
            this.unitOfWork = new UnitOfWork();
        }

        public HomeController(IRepository<Admin> repository)
        {
            unitOfWork = new UnitOfWork();
            this.unitOfWork.AdminRepository = repository;
        }

        public HomeController(IRepository<Employee> repository)
        {
            unitOfWork = new UnitOfWork();
            this.unitOfWork.EmployeeRepository = repository;
        }

        public ActionResult Index()
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetNoStore();
            Response.Cache.SetExpires(DateTime.MinValue);
            return View();
        }

        public ActionResult AdminLogin()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AdminLogin(LoginViewModel model, string returnUrl)
        {
            if (model == null)
            {
                ModelState.AddModelError("", "There has been an internal error. Please try again.");
                return View();
            }
            string salt = "92429d82a41e930486c6de5ebda9602d55c39986";
            var bytes = Encoding.UTF8.GetBytes(model.Password + salt);
            var password = Convert.ToBase64String((new SHA256Managed()).ComputeHash(bytes));
            using (unitOfWork)
            {
                Admin admin = null;
                try
                {
                    admin = unitOfWork.AdminRepository.Get().Where(e => e.Email == model.Email).FirstOrDefault();
                }
                catch (Exception e)
                {
                    ModelState.AddModelError("", "There has been an internal error with the database. Please try again.");
                    return View();
                }
                if (admin == null)
                {
                    ModelState.AddModelError("", "The email you entered does not exist in our database.");
                } else if (admin.HashedPassword != password)
                {
                    ModelState.AddModelError("", "The password you entered is incorrect.");
                } else
                {
                    this.signInUser("Admin");
                    return RedirectToAction("Index", "Admin");
                }
            }
            return View();
        }

        public ActionResult EmployeeLogin()
        {
            return View();
        }

        [HttpPost]
        public ActionResult EmployeeLogin(LoginViewModel model, string returnUrl)
        {
            if(model == null)
            {
                ModelState.AddModelError("", "There has been an internal error. Please try again.");
                return View();
            }
            string salt = "92429d82a41e930486c6de5ebda9602d55c39986";
            var bytes = Encoding.UTF8.GetBytes(model.Password + salt);
            var password = Convert.ToBase64String((new SHA256Managed()).ComputeHash(bytes));
            using (unitOfWork)
            {
                Employee employee = null;
                try
                {
                   employee = unitOfWork.EmployeeRepository.Get(e => e.Email == model.Email).FirstOrDefault();
                } catch (Exception)
                {
                    ModelState.AddModelError("", "There has been an internal error with the database. Please try again.");
                    return View();
                }
                if (employee == null)
                {
                    ModelState.AddModelError("", "The email you entered does not exist in our database.");
                }
                else if (employee.HashedPassword != password)
                {
                    ModelState.AddModelError("", "The password you entered is incorrect.");
                } else if (!employee.Active)
                {
                    ModelState.AddModelError("", "The account you are attempting to access has been disabled");
                }
                else
                {
                    this.signInUser("Employee");
                    Session["LoggedInEmployeeID"] = employee.EmployeeID;
                    return RedirectToAction("Index", "Employee");
                }
            }
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpGet]
        public ActionResult SetPassword(string id)
        {
            var employeeList = unitOfWork.EmployeeRepository.Get().ToList();
            Employee employee = null;
            foreach (var emp in employeeList)
            {
                string salt = "92429dsdaa41e930d86c6de4ebda9602d55c39986";
                var bytes = Encoding.UTF8.GetBytes(emp.EmployeeID + salt);
                var hashedID = Convert.ToBase64String((new SHA256Managed()).ComputeHash(bytes));

                Regex rgx = new Regex("[^a-zA-Z0-9 -]");
                hashedID = rgx.Replace(hashedID, "");

                if (hashedID == id)
                {
                    employee = emp;
                }
            }

            if (employee != null)
            {
                ViewBag.ConfirmPassword = "";

                var editModel = new EditEmployeeViewModel();
                editModel.Employee = employee;
                return View(employee);
            }

            return View("Error");

        }

        [HttpPost]
        public ActionResult SetPassword([Bind(Include = "EmployeeID, HashedPassword, Email, FirstName, LastName, MailingAddress, Phone, CohortID")]Employee employee, String confirmedPassword)
        {
            TempData["Error"] = "";
            employee.Cohort = unitOfWork.CohortRepository.Get().FirstOrDefault(c => c.CohortID == employee.CohortID);
            if (ModelState.IsValid && confirmedPassword == employee.HashedPassword)
            {
                if (Regex.IsMatch(employee.HashedPassword, "^[a-zA-Z][a-zA-Z0-9]{6,14}$"))
                {
                    string salt = "92429d82a41e930486c6de5ebda9602d55c39986";
                    var bytes = Encoding.UTF8.GetBytes(employee.HashedPassword + salt);
                    var password = Convert.ToBase64String((new SHA256Managed()).ComputeHash(bytes));
                    employee.HashedPassword = password;
                    employee.Active = true;
                    unitOfWork.EmployeeRepository.Update(employee);
                    unitOfWork.Save();


                    return RedirectToAction("EmployeeLogin", "Home");

                }

                    TempData["Error"] =
                        "Password must be 6-14 characters long and can only contain alphanumeric characters or underscores.";
                    return View(employee);
            }

            TempData["Error"] = "Passwords do not match.";
            return View(employee);
        }

        public void signInUser(string type)
        {
            if (type == "Admin")
            {
                Session["AdminLoggedIn"] = true;
            }
            if (type == "Employee")
            {
                Session["EmployeeLoggedIn"] = true;
            }
        }

    }
}