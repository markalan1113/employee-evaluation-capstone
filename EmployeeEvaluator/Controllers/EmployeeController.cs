﻿using EmployeeEvaluator.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using EmployeeEvaluator.DAL;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;
using SendGrid;

namespace EmployeeEvaluator.Controllers
{ 
    public class EmployeeController : Controller
    {
        private static UnitOfWork unitOfWork;
        private bool isTest = false;

        public EmployeeController()
        {
            unitOfWork = new UnitOfWork();
        }

        public EmployeeController(IRepository<Employee> repository)
        {
            unitOfWork = new UnitOfWork();
            unitOfWork.EmployeeRepository = repository;
        }

        public EmployeeController(IRepository<Evaluation> evaluationRepository, IRepository<Cohort> cohortRepository, IRepository<Employee> employeeRepository, IRepository<EvalStage> stageRepository, IRepository<MaxRater> maxRaterRepository, IRepository<RaterRole> raterRoleRepository, IRepository<Category> categoryRepository, IRepository<EvalType> typeRepository, IRepository<Rater> raterRepository)
        {
            unitOfWork = new UnitOfWork();
            unitOfWork.EvaluationRepository = evaluationRepository;
            unitOfWork.CohortRepository = cohortRepository;
            unitOfWork.EmployeeRepository = employeeRepository;
            unitOfWork.StageRepository = stageRepository;
            unitOfWork.MaxRaterRepository = maxRaterRepository;
            unitOfWork.RaterRoleRepository = raterRoleRepository;
            unitOfWork.CategoryRepository = categoryRepository;
            unitOfWork.TypeRepository = typeRepository;
            unitOfWork.RaterRepository = raterRepository;
            this.isTest = true;
        }

        public ActionResult Index()
        {
            if (authenticatedAsEmployee())
            {
                var employee = unitOfWork.EmployeeRepository.GetByID((int)Session["LoggedInEmployeeID"]);
                return View(employee);
            }
            return RedirectToAction("EmployeeLogin", "Home");
        }

        public ActionResult List()
        {
            if (this.authenticatedAsAdmin())
            {
                var employees = unitOfWork.EmployeeRepository.Get(includeProperties: "Cohort");
                return View(employees.ToList());
            }
            return RedirectToAction("AdminLogin", "Home");

        }

        public ActionResult LogOff()
        {
            Session["EmployeeLoggedIn"] = false;
            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public ActionResult Edit(int? id)
        {
            if (authenticatedAsAdmin())
            {
                if (TempData.ContainsKey("ModelState"))
                    ModelState.Merge((ModelStateDictionary)TempData["ModelState"]);
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                Employee employee = unitOfWork.EmployeeRepository.GetByID(id);

                if (employee == null)
                {
                    return RedirectToAction("List", "Employee");
                }
                employee.EmployeeID = (int) id;
                var editModel = new EditEmployeeViewModel();
                editModel.Employee = employee;
                editModel.Cohorts = unitOfWork.CohortRepository.Get().ToList().Select(x => new SelectListItem
                {
                    Value = x.CohortID.ToString(),
                    Text = x.Name
                });

                return View(editModel);
            }

            return RedirectToAction("AdminLogin", "Home");
        }

        [HttpGet]
        public ActionResult AssignToCohort(int? id)
        {
            if (authenticatedAsAdmin())
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Employee employee =
                    unitOfWork.EmployeeRepository.Get(includeProperties: "Cohort")
                        .ToList()
                        .Where(e => e.EmployeeID == id)
                        .FirstOrDefault();
                if (employee == null)
                {
                    return RedirectToAction("List", "Employee");
                }
                if (employee.Cohort != null)
                {
                    var cohort = unitOfWork.CohortRepository.GetByID(employee.CohortID);
                    if (cohort.Stable)
                    {
                     TempData["Confirmation"] = "Employee is already in a stable cohort and cannot be reassigned";
                     return RedirectToAction("List", "Employee");
                    }
                }
                employee.EmployeeID = (int) id;
                var editModel = new EditEmployeeViewModel();
                editModel.Employee = employee;
                editModel.Cohorts = unitOfWork.CohortRepository.Get().ToList().Select(x => new SelectListItem
                {
                    Value = x.CohortID.ToString(),
                    Text = x.Name
                });

                return View(editModel);
            }

            return RedirectToAction("AdminLogin", "Home");
            
        }

        [HttpPost]
        public ActionResult Edit([Bind(Include = "CohortID, EmployeeID, Email, FirstName, LastName, MailingAddress, Phone, Active")]Employee employee)
        {
            TempData["Confirmation"] = "";
            if (ModelState.IsValid)
            {
                if (employee.Active == false)
                {
                    var cohort = unitOfWork.CohortRepository.GetByID(employee.CohortID);
                    var evaluations =
                        unitOfWork.EvaluationRepository.Get()
                            .Where(e => e.CohortID == employee.CohortID && (e.Pending || !e.Completed));
                    if (evaluations.Count() != 0)
                    {
                        TempData["Confirmation"] = "Cannot deactivate an employee in the middle of an evaluation";
                        return RedirectToAction("Edit", "Employee");
                    }
                    if (cohort.Stable)
                    {
                        TempData["Confirmation"] = "Cannot deactivate an employee in a stable cohort";
                        return RedirectToAction("Edit", "Employee");
                    }
                }
                
                employee.Cohort = unitOfWork.CohortRepository.Get().FirstOrDefault(c => c.CohortID == employee.CohortID);
                if (employee.CohortID == 0)
                {
                    employee.CohortID = null;
                }

                try
                {
                    unitOfWork.EmployeeRepository.Update(employee);
                    unitOfWork.Save();
                }
                catch (Exception)
                {
                    return View("Error");
                }
                TempData["Confirmation"] = "Employee successfully updated.";
                return RedirectToAction("Edit", "Employee");
            }
            TempData["ModelState"] = ModelState;
            return RedirectToAction("Edit", "Employee");
        }

        [HttpPost]
        public ActionResult AssignToCohort([Bind(Include = "EmployeeID, CohortID, Email")]Employee employee)
        {

            TempData["Confirmation"] = "";

            var cohort = unitOfWork.CohortRepository.GetByID(employee.CohortID);
            employee = unitOfWork.EmployeeRepository.GetByID(employee.EmployeeID);
            employee.Cohort = cohort;

            if (cohort.Stable)
            {
                TempData["Confirmation"] = "Cannot assign employee to a stable cohort";
                return RedirectToAction("AssignToCohort", "Employee");
            }

            try
            {
                unitOfWork.EmployeeRepository.Update(employee);
                unitOfWork.CohortRepository.Update(cohort);
                unitOfWork.Save();
            }
            catch (Exception e)
            {
                return View("Error");
            }

            TempData["Confirmation"] = "Employee successfully assigned to " + cohort.Name + ".";
            return RedirectToAction("List", "Employee");

        }

        public ActionResult ViewRaters()
        {
            if (authenticatedAsEmployee())
            {
                var viewModels = new List<RaterRoleViewModel>();
                var employee = unitOfWork.EmployeeRepository.GetByID((int)Session["LoggedInEmployeeID"]);
                var raters = employee.Raters.Where(r => r.Active);
                foreach(var rater in raters)
                {
                    var viewModel = new RaterRoleViewModel();
                    viewModel.Rater = rater;
                    viewModel.RaterRole = unitOfWork.RaterRoleRepository.GetByID(rater.RaterRoleID);
                    viewModels.Add(viewModel);
                }
                return View(viewModels);
            }
            return RedirectToAction("EmployeeLogin", "Home");
        }

        [HttpGet]
        public ActionResult DisableRater(int id)
        {
            if (authenticatedAsEmployee())
            {
                var viewModel = new RaterRoleViewModel();
                var rater = unitOfWork.RaterRepository.GetByID(id);
                var raterRole = unitOfWork.RaterRoleRepository.GetByID(rater.RaterRoleID);
                viewModel.Rater = rater;
                viewModel.RaterRole = raterRole;
                return View(viewModel);
            }
            return RedirectToAction("EmployeeLogin", "Home");
        }

        [HttpPost]
        public ActionResult DisableRater(FormCollection form)
        {
            if (authenticatedAsEmployee() && form != null)
            {
                var raterToUpdate = unitOfWork.RaterRepository.GetByID(Convert.ToInt32(form["Rater.RaterID"]));
                if(raterToUpdate == null)
                {
                    return RedirectToAction("EmployeeLogin", "Home");
                }
                if (
                    unitOfWork.EvaluationRepository.Get()
                        .Where(e => !e.Completed && e.RaterID == raterToUpdate.RaterID)
                        .FirstOrDefault() != null)
                {
                    TempData["Confirmation"] = "This rater has an active evaluation and cannot be disabled.";
                    return RedirectToAction("ViewRaters", "Employee");
                }
                raterToUpdate.Active = false;
                unitOfWork.RaterRepository.Update(raterToUpdate);
                unitOfWork.Save();
                return RedirectToAction("ViewRaters", "Employee");
            }
            return RedirectToAction("EmployeeLogin", "Home");
        }
        public async Task<ActionResult> Resend(int EmployeeID, int RaterID)
        {
            var completedEvals =
                unitOfWork.EvaluationRepository.Get().Where(e => e.Completed && e.EmployeeID == EmployeeID).ToList();
            if (completedEvals.Count == 0)
            {
                TempData["Success"] = "You have not completed any evaluations.";
                return RedirectToAction("ViewRaters", "Employee");
            }
            else
            {
                var rater = unitOfWork.RaterRepository.GetByID(RaterID);
                var evaluations = unitOfWork.EvaluationRepository.Get().Where(e => e.RaterID == rater.RaterID && e.EmployeeID == EmployeeID).ToList();
                var emailSent = false;
                foreach (var evaluation in evaluations)
                {
                    if (!evaluation.Completed)
                    {

                        await this.ResendEmail(rater, evaluation);
                        emailSent = true;
                    }
                }

                if (!emailSent)
                {
                    TempData["Success"] = "This rater has already completed their evaluation.";
                    return RedirectToAction("ViewRaters", "Employee");
                }
                else
                {
                    TempData["Success"] = "Email successfully sent.";
                    return RedirectToAction("ViewRaters", "Employee");
                }

            }
        }

        public async Task ResendEmail(Rater rater, Evaluation evaluation)
        {
            var message = new SendGridMessage();
            message.AddTo(rater.Email);

            var callbackUrl = Url.Action("Start", "Evaluation", new { id = evaluation.EvaluationID }, protocol: Request.Url.Scheme);
            message.From = new MailAddress(
                "CapstoneEmployeeEvaluation@gmail.com", "Employee Evaluations");
            message.Subject = "Attention Rater! Employee Has Completed an Evaluation.";

            var body =
                "Hello,\r\n\r\n An employee that you are a rater for has completed their evaluation. Please login and fill out the evaluation by the posted deadline date. Thank you. <a href=\"" +
                callbackUrl + "\">Complete Evaluation</a>. \r\n";
            message.Text = body;
            message.Html = body;

            var credentials = new NetworkCredential(
                ConfigurationManager.AppSettings["mailAccount"],
                ConfigurationManager.AppSettings["mailPassword"]
            );

            // Create a Web transport for sending email.
            var transportWeb = new Web(credentials);

            // Send the email.
            if (transportWeb != null)
                await transportWeb.DeliverAsync(message);

        }

        private bool authenticatedAsAdmin()
        {
            if (!(bool)Session["AdminLoggedIn"])
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private bool authenticatedAsEmployee()
        {
            if (!(bool)Session["EmployeeLoggedIn"])
            {
                return false;
            }
            else
            {
                return true;
            }
        }

    }
}