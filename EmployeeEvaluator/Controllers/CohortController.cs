﻿using EmployeeEvaluator.DAL;
using EmployeeEvaluator.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using SendGrid;
using Exceptions;

namespace EmployeeEvaluator.Controllers
{
    public class CohortController : Controller
    {
        private static UnitOfWork unitOfWork;
        private PlaceHolder placeHolder;
        private bool isTest = false;

        public CohortController()
        {
            unitOfWork = new UnitOfWork();
            placeHolder = new PlaceHolder();
        }

        public CohortController(IRepository<Evaluation> evaluationRepository, IRepository<Cohort> cohortRepository, IRepository<Employee> employeeRepository, IRepository<EvalStage> stageRepository, IRepository<MaxRater> maxRaterRepository, IRepository<RaterRole> raterRoleRepository, IRepository<Category> categoryRepository, IRepository<EvalType> typeRepository, IRepository<Rater> raterRepository)
        {
            unitOfWork = new UnitOfWork();
            unitOfWork.EvaluationRepository = evaluationRepository;
            unitOfWork.CohortRepository = cohortRepository;
            unitOfWork.EmployeeRepository = employeeRepository;
            unitOfWork.StageRepository = stageRepository;
            unitOfWork.MaxRaterRepository = maxRaterRepository;
            unitOfWork.RaterRoleRepository = raterRoleRepository;
            unitOfWork.CategoryRepository = categoryRepository;
            unitOfWork.TypeRepository = typeRepository;
            unitOfWork.RaterRepository = raterRepository;
            placeHolder = new PlaceHolder();
            this.isTest = true;
        }
        public ActionResult Index(int? id)
        {
            if (this.authenticatedAsAdmin())
            {
                var viewModel = new CohortIndexData();
                viewModel.Cohorts = unitOfWork.CohortRepository.Get();

                if (id != null)
                {
                    ViewBag.CohortID = id.Value;
                    viewModel.Employees = unitOfWork.EmployeeRepository.Get().Where(i => i.CohortID == id.Value);
                }
                return View(viewModel);
            }
 
            return RedirectToAction("AdminLogin", "Home");

        }

        // Edit: Cohort
        public ActionResult Edit(int? id)
        {
            if (this.authenticatedAsAdmin())
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Cohort cohort = unitOfWork.CohortRepository.GetByID(id);
                AddCohortViewModel model = new AddCohortViewModel {Cohort = cohort};

                if (cohort == null)
                {
                    return HttpNotFound();
                }
                return View(model);
            }
            return RedirectToAction("AdminLogin", "Home");

        }

        [HttpPost]
        public ActionResult Edit([Bind(Include = "CohortID,Name,Stable")] Cohort cohort)
        {
            TempData["Confirmation"] = "";
            if (this.authenticatedAsAdmin())
            {
                var inProgressEvaluations = unitOfWork.EvaluationRepository.Get().Where(e => e.CohortID == cohort.CohortID  && (!e.Completed || e.Pending)).ToList();
                try
                {
                    if (!cohort.Stable && inProgressEvaluations.Count == 0)
                    {
                        unitOfWork.CohortRepository.Update(cohort);
                        unitOfWork.Save();

                        TempData["Confirmation"] = "Cohort successfully updated.";
                    } else if (cohort.Stable)
                    {
                        unitOfWork.CohortRepository.Update(cohort);
                        unitOfWork.Save();

                        TempData["Confirmation"] = "Cohort successfully updated.";
                    }
                    else
                    {
                        TempData["Confirmation"] = "Cannot destabilize a cohort that has evaluations in progress.";
                    }
                    return RedirectToAction("Edit", "Cohort");
                    
                }
                catch (DBConcurrencyException)
                {
                    return View("Error");
                }
            }
            return RedirectToAction("AdminLogin", "Home");
        }

        [HttpGet]
        public ActionResult Create()
        {
            if (this.authenticatedAsAdmin())
            {
                var cohort = new Cohort();
                cohort.CohortID = unitOfWork.CohortRepository.Get().OrderByDescending(c => c.CohortID).FirstOrDefault().CohortID + 1;
                var addModel = new AddCohortViewModel();
                addModel.Cohort = cohort;
                if (TempData.ContainsKey("ModelState"))
                    ModelState.Merge((ModelStateDictionary)TempData["ModelState"]);
                return View(addModel);
            }

            return RedirectToAction("AdminLogin", "Home");

        }

        [HttpPost]
        public ActionResult Create([Bind(Include = "Name, StartDate, EndDate")]Cohort cohort)
        {
            TempData["Confirmation"] = "";

            if (this.authenticatedAsAdmin())
            {
                if (cohort.Name == null || cohort.Name.Equals(""))
                {
                    TempData["ModelState"] = ModelState;
                    return RedirectToAction("Create", "Cohort");
                }

                unitOfWork.CohortRepository.Insert(cohort);
                unitOfWork.Save();

                TempData["Confirmation"] = "Cohort succesfully created.";

                return RedirectToAction("Create", "Cohort");
            }

            return RedirectToAction("AdminLogin", "Home");
        }

        // GET: Cohort/Delete/5
        public ActionResult Delete(int? id)
        {
            if (this.authenticatedAsAdmin())
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Cohort cohort = unitOfWork.CohortRepository.GetByID(id);
                if (cohort == null)
                {
                    return HttpNotFound();
                }
                return View(cohort);
            }

            return RedirectToAction("AdminLogin", "Home");

           
        }

        // POST: Agent/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TempData["Confirmation"] = "";
            if (this.authenticatedAsAdmin())
            {
                Cohort cohort = unitOfWork.CohortRepository.GetByID(id);
                unitOfWork.CohortRepository.Delete(cohort);
                unitOfWork.Save();
                TempData["Confirmation"] = "Cohort successfully deleted.";
                return RedirectToAction("Delete", "Cohort");
            }

            return RedirectToAction("AdminLogin", "Home");
        }

        // GET: Cohort/Details/5
        public ActionResult Details(int? id)
        {
            if (this.authenticatedAsAdmin())
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Cohort cohort = unitOfWork.CohortRepository.GetByID(id);
                if (cohort == null)
                {
                    return HttpNotFound();
                }
                return View(cohort);
            }

                return RedirectToAction("AdminLogin", "Home");
        }

        public ActionResult LoadEmployees()
        {
            if (this.authenticatedAsAdmin())
            {
                return View();
            }
            return RedirectToAction("AdminLogin", "Home");
        }

        [HttpPost]
        public ActionResult LoadEmployees(HttpPostedFileBase FileUpload)
        {
            if (authenticatedAsAdmin())
            {
                DataTable dt = new DataTable();
                try
                {
                    if (FileUpload.ContentLength > 0)
                    {
                        string fileName = Path.GetFileName(FileUpload.FileName);
                        string path = Path.Combine(Server.MapPath("~/App_Data/CSV_Uploads"), fileName);

                        try
                        {
                            FileUpload.SaveAs(path);
                            dt = ProcessCSV(path);

                            ViewData["Feedback"] = ProcessBulkCopy(dt);

                            StringBuilder html = new StringBuilder();

                            //Table start.
                            html.Append("<table border = '1'>");

                            html.Append("<tr>");
                            foreach (DataColumn column in dt.Columns)
                            {
                                if (column.ColumnName != "EmployeeID" && column.ColumnName != "Password")
                                {
                                    html.Append("<th>");
                                    html.Append(column.ColumnName);
                                    html.Append("</th>");
                                }
                            }
                            html.Append("</tr>");

                            foreach (DataRow row in dt.Rows)
                            {

                                html.Append("<tr>");
                                foreach (DataColumn column in dt.Columns)
                                {
                                    if (column.ColumnName != "EmployeeID" && column.ColumnName != "Password")
                                    {
                                        html.Append("<td>");
                                        html.Append(row[column.ColumnName]);
                                        html.Append("</td>");
                                    }
                                }
                                html.Append("</tr>");
                            }

                            //Table end.
                            html.Append("</table>");

                            placeHolder.Controls.Add(new Literal {Text = html.ToString()});

                            TempData["Table"] = html;

                        }
                        catch (Exception ex)
                        {
                            ViewData["Feedback"] = ex.Message;
                        }
                    }
                    else
                    {
                        ViewData["Feedback"] = "Please select a non-empty file";
                    }

                    dt.Dispose();
                }
                catch (NullReferenceException)
                {
                    ViewData["Feedback"] = "Please select a file";
                }


                return View("LoadEmployees", ViewData["Feedback"]);
            }

            return RedirectToAction("AdminLogin", "Home");
        }

        public ActionResult Waiting()
        {
            if (this.authenticatedAsAdmin())
            {
                TempData["Confirmation"] = "";
                var evaluations = unitOfWork.EvaluationRepository.Get();
                var cohortsToView = new List<Cohort>();

                foreach (var evaluation in evaluations)
                {
                    var stage = unitOfWork.StageRepository.GetByID(evaluation.EvalStageID);
                    if (evaluation.Completed && evaluation.CohortID != 0 && DateTime.Compare(evaluation.EndDate, DateTime.Now) < 0 && !stage.StageName.Equals("Summative"))
                    {
                        cohortsToView.Add(unitOfWork.CohortRepository.Get().Where(c => c.CohortID == evaluation.CohortID).FirstOrDefault());
                    }
                }

                if (cohortsToView.Count == 0)
                {
                    TempData["Confirmation"] = "There are no cohorts waiting to move to the next phase.";
                }

                return View(cohortsToView);
            }

            return RedirectToAction("AdminLogin", "Home");
        }

        [HttpGet]
        public ActionResult MoveToNextPhase(int id)
        {
            using (unitOfWork)
            {
                if (this.authenticatedAsAdmin())
                {
                    var evaluation = unitOfWork.EvaluationRepository.Get().Where(e => e.CohortID == id).FirstOrDefault();

                    return View(evaluation);
                }
            }
            
            return RedirectToAction("AdminLogin", "Home");
        }

        [HttpPost]
        public ActionResult MoveToNextPhase([Bind(Include = "EvaluationID,StartDate,EndDate")] Evaluation evaluation)
        {
            using (unitOfWork)
            {
                var evaluationToUpdate = unitOfWork.EvaluationRepository.GetByID(evaluation.EvaluationID);
                var stage = unitOfWork.StageRepository.GetByID(evaluationToUpdate.EvalStageID);
                evaluationToUpdate.StartDate = evaluation.StartDate;
                evaluationToUpdate.EndDate = evaluation.EndDate;

                if (stage.StageName.Equals("Baseline"))
                {
                    evaluationToUpdate.EvalStageID = unitOfWork.StageRepository.Get().Where(s => s.StageName.Equals("Formative")).FirstOrDefault().EvalStageID;
                }
                else if (stage.StageName.Equals("Formative"))
                {
                    evaluationToUpdate.EvalStageID = unitOfWork.StageRepository.Get().Where(s => s.StageName.Equals("Summative")).FirstOrDefault().EvalStageID;
                } else if (stage.StageName.Equals("Summative"))
                {
                    TempData["Confirmation"] = "This cohort's evaluation has completed every stage";
                }
                else
                {
                    TempData["Confirmation"] = "This cohort's evaluation stage is invalid.";
                }
                evaluationToUpdate.Completed = false;

                unitOfWork.EvaluationRepository.Update(evaluationToUpdate);
                unitOfWork.Save();

                return RedirectToAction("Waiting");
            }
        }

        [HttpGet]
        public ActionResult SendBulkEmail(int? id)
        {
            if (this.authenticatedAsAdmin())
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Cohort cohort = unitOfWork.CohortRepository.GetByID(id);
                if (cohort == null)
                {
                    return HttpNotFound();
                }
                return View(cohort);
            }
            return RedirectToAction("AdminLogin", "Home");
        }

        [HttpPost]
        public async Task<ActionResult> SendBulkEmail([Bind(Include = "CohortID")]Cohort cohort)
        {
            TempData["Confirmation"] = "";

            if (this.authenticatedAsAdmin())
            {
                var cohortToEmail = unitOfWork.CohortRepository.GetByID(cohort.CohortID);
                if (cohortToEmail.Stable)
                {
                    TempData["Confirmation"] = "Cohort is already stable, cannot reactivate.";
                    return RedirectToAction("Index", "Cohort");
                }
                    var employees =
                        unitOfWork.EmployeeRepository.Get().ToList().Where(e => e.CohortID == cohort.CohortID);
                    foreach (Employee employee in employees)
                    {
                        if (employee.Active)
                        {
                            await SendEmail(employee);
                        }
                    }
                    TempData["Confirmation"] = "Emails successfully sent.";
                    cohortToEmail.Stable = true;
                    unitOfWork.CohortRepository.Update(cohortToEmail);
                    unitOfWork.Save();
                    return RedirectToAction("SendBulkEmail", "Cohort");
            }
            return RedirectToAction("AdminLogin", "Home");

        }

        [HttpPost]
        public async Task<ActionResult> SendEmail([Bind(Include = "EmployeeID")]Employee employee)
        {
            if (this.authenticatedAsAdmin())
            {
                var emailEmployee = unitOfWork.EmployeeRepository.GetByID(employee.EmployeeID);

                string salt = "92429dsdaa41e930d86c6de4ebda9602d55c39986";
                var bytes = Encoding.UTF8.GetBytes(employee.EmployeeID + salt);
                var hashedID = Convert.ToBase64String((new SHA256Managed()).ComputeHash(bytes));

                Regex rgx = new Regex("[^a-zA-Z0-9 -]");
                hashedID = rgx.Replace(hashedID, "");

                var message = new SendGridMessage();
                message.AddTo(emailEmployee.Email);
                var callbackUrl = Url.Action("SetPassword", "Home", new {id = hashedID}, protocol: Request.Url.Scheme);
                message.From = new System.Net.Mail.MailAddress(
                    "CapstoneEmployeeEvaluation@gmail.com", "Employee Evaluations");
                message.Subject = "Evaluation Login Information";
                var body = "Hello,\r\n\r\n Please create your password to log in to your new account <a href=\"" +
                           callbackUrl + "\">here</a>. \r\n";
                message.Text = body;
                message.Html = body;

                var credentials = new NetworkCredential(
                    ConfigurationManager.AppSettings["mailAccount"],
                    ConfigurationManager.AppSettings["mailPassword"]
                );

                // Create a Web transport for sending email.
                var transportWeb = new Web(credentials);

                // Send the email.
                if (transportWeb != null)
                {
                    try
                    {
                        await transportWeb.DeliverAsync(message);
                    }
                    catch (InvalidApiRequestException ex)
                    {
                        var details = new StringBuilder();

                        details.Append("ResponseStatusCode: " + ex.ResponseStatusCode + ".   ");
                        for (int i = 0; i < ex.Errors.Count(); i++)
                        {
                            details.Append(" -- Error #" + i.ToString() + " : " + ex.Errors[i]);
                        }
                        var error = details.ToString();

                        throw new ApplicationException(details.ToString(), ex);
                    }
                }
                return RedirectToAction("Index", "Cohort");
            }
            return RedirectToAction("AdminLogin", "Home");
        }

        [HttpGet]
        public ActionResult CreateEvaluationPeriod(int? id)
        {
            if (this.authenticatedAsAdmin())
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Cohort cohort = unitOfWork.CohortRepository.GetByID(id);
                var evaluations = unitOfWork.EvaluationRepository.Get().Where(e => e.CohortID == cohort.CohortID).ToList();

                if (cohort.Stable == false)
                {
                    TempData["Confirmation"] = "This cohort is currently unstable. Cannot start evaluation";
                    return RedirectToAction("Index", "Cohort");
                }

                foreach (var evaluation in evaluations)
                {
                    if (DateTime.Compare(evaluation.EndDate, DateTime.Now) > 0 && DateTime.Compare(evaluation.StartDate, DateTime.Now) < 0)
                    {
                        TempData["Confirmation"] = "This cohort has an evaluation in progress. Cannot set another period until it has ended.";
                        return RedirectToAction("Index", "Cohort");
                    }
                }

                if (cohort == null)
                {
                    return HttpNotFound();
                }
                return View(cohort);
            }
            return RedirectToAction("AdminLogin", "Home");
        }

        [HttpPost]
        public async Task<ActionResult> CreateEvaluationPeriod([Bind(Include = "CohortID")]Cohort cohort, DateTime startDate, DateTime endDate)
        {
            TempData["Confirmation"] = "";


                if (this.authenticatedAsAdmin())
                 { 
                    var evaluations = unitOfWork.EvaluationRepository.Get().ToList();
                    foreach (var evaluation in evaluations)
                    {
                        if (evaluation.CohortID == cohort.CohortID)
                        {
                            evaluation.StartDate = startDate;
                            evaluation.EndDate = endDate;
                            unitOfWork.EvaluationRepository.Update(evaluation);
                            unitOfWork.Save();
                        }
                    }
                    var cohortToUpdate = unitOfWork.CohortRepository.GetByID(cohort.CohortID);
                    TempData["Confirmation"] = "Emails successfully sent.";

                    cohortToUpdate.Stable = true;

                    unitOfWork.CohortRepository.Update(cohortToUpdate);
                    unitOfWork.Save();
                    return RedirectToAction("Create", "Evaluation");
                }
          
            return RedirectToAction("AdminLogin", "Home");

        }

        private static DataTable ProcessCSV(string fileName)
        {
            string feedback = string.Empty;
            string line = string.Empty;
            string[] strArray;
            DataTable dt = new DataTable();
            DataRow row;

            StreamReader sr = new StreamReader(fileName);

            line = sr.ReadLine();
            strArray = line.Split(',');

            foreach(String column in strArray)
            {
                if (column == "CohortID" || column == "EmployeeID")
                {
                    dt.Columns.Add(column, typeof(int));
                } else
                {
                    dt.Columns.Add(column, typeof(string));
                }
                
            }
            while ((line = sr.ReadLine()) != null)
            {
                row = dt.NewRow();

                row.ItemArray = line.Split(',');
                dt.Rows.Add(row);
            }
            if (dt.Rows.Count == 0)
            {
                feedback = "Invalid CSV";
            }
            sr.Dispose();
            return dt;

        }

        private static String ProcessBulkCopy(DataTable dt)
        {
            string feedback = string.Empty;
            string connString = ConfigurationManager.ConnectionStrings["EvaluatorContext"].ConnectionString;
            List<int> rowsToRemove = new List<int>();

            var employeeList = unitOfWork.EmployeeRepository.Get().ToList();

            foreach (Employee employee in employeeList)
            {
                foreach (DataRow row in dt.Rows)
                {  
                    if (row[3].Equals(employee.Email))
                    {
                        rowsToRemove.Add(dt.Rows.IndexOf(row));
                    }
                }
            }

            rowsToRemove.Reverse();

            foreach (var duplicateEntry in rowsToRemove)
            {
                dt.Rows.RemoveAt(duplicateEntry);
            }

            if (dt.Rows.Count == 0)
            {
               return feedback = "File only contained duplicate emails";
            }

            using (SqlConnection conn = new SqlConnection(connString))
            {
                using (var copy = new SqlBulkCopy(conn))
                {

                    conn.Open();

                    copy.DestinationTableName = "Employee";
                    copy.BatchSize = dt.Rows.Count;
                    try
                    {
                        copy.WriteToServer(dt);
                        feedback = "Upload complete";
                    }
                    catch (Exception ex)
                    {
                        if (feedback == string.Empty)
                        {
                            feedback = ex.Message;
                        }        
                    }
                }
            }

            return feedback;
        }


        private bool authenticatedAsAdmin()
        {
            if (!(bool)Session["AdminLoggedIn"])
            {
                return false;
            }
            else
            {
                return true;
            }
        }

     }

}