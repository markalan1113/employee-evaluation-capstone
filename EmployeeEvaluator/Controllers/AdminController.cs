﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EmployeeEvaluator.DAL;
using EmployeeEvaluator.Models;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace EmployeeEvaluator.Controllers
{
    public class AdminController : Controller
    {
        private static UnitOfWork unitOfWork;
        private bool isTest = false;

        public AdminController()
        {
            unitOfWork = new UnitOfWork();
        }

        public AdminController(IRepository<Employee> repository)
        {
            unitOfWork = new UnitOfWork();
            unitOfWork.EmployeeRepository = repository;
        }

        public AdminController(IRepository<Evaluation> evaluationRepository, IRepository<Cohort> cohortRepository, IRepository<Employee> employeeRepository, IRepository<EvalStage> stageRepository, IRepository<MaxRater> maxRaterRepository, IRepository<RaterRole> raterRoleRepository, IRepository<Category> categoryRepository, IRepository<EvalType> typeRepository, IRepository<Rater> raterRepository)
        {
            unitOfWork = new UnitOfWork();
            unitOfWork.EvaluationRepository = evaluationRepository;
            unitOfWork.CohortRepository = cohortRepository;
            unitOfWork.EmployeeRepository = employeeRepository;
            unitOfWork.StageRepository = stageRepository;
            unitOfWork.MaxRaterRepository = maxRaterRepository;
            unitOfWork.RaterRoleRepository = raterRoleRepository;
            unitOfWork.CategoryRepository = categoryRepository;
            unitOfWork.TypeRepository = typeRepository;
            unitOfWork.RaterRepository = raterRepository;
            this.isTest = true;
        }

        // GET: Admin
        public ActionResult Index()
        {
            if (this.authenticated())
            {
                return View();
            }
            else
            {
                return RedirectToAction("AdminLogin", "Home");
            }
            
        }

        public ActionResult LogOff()
        {
            Session["AdminLoggedIn"] = false;
            return RedirectToAction("Index", "Home");
        }
        public ActionResult LoadEmployees()
        {
            if (this.authenticated())
            {
                return View();
            }
            else
            {
                return RedirectToAction("AdminLogin", "Home");
            }
        }

        [HttpGet]
        public ActionResult GeneratePDF(int? cohortId, int? employeeId)
        {
            if (this.authenticated())
            {
                var employeeIndexData = new EmployeeIndexData();
                var employees = unitOfWork.EmployeeRepository.Get().Where(e => e.Active).ToList();
                var cohorts = unitOfWork.CohortRepository.Get().ToList();
                employeeIndexData.Cohorts = cohorts;
                foreach (var employee in employees)
                {   
                    employee.Cohort = unitOfWork.CohortRepository.GetByID(employee.CohortID);
                }
                if (cohortId != null)
                {
                    var cohort = unitOfWork.CohortRepository.GetByID(cohortId);
                    if (!cohort.Stable)
                    {
                        TempData["Confirmation"] = "That cohort is not stable.";
                        return RedirectToAction("GeneratePDF", "Admin");
                    }
                    employees = unitOfWork.EmployeeRepository.Get().Where(e => e.Active && e.CohortID == cohort.CohortID).ToList();
                    employeeIndexData.Employees = employees;
                }
                
                if (employeeId != null)
                {
                    var employee = unitOfWork.EmployeeRepository.GetByID(employeeId);
                    var evals = employee.Evaluations.Where(e => e.Completed && e.RaterID == null);
                    employeeIndexData.Evaluations = evals;
                } 
                return View(employeeIndexData);
            }
            else
            {
                return RedirectToAction("AdminLogin", "Home");
            }
        }

        public ActionResult CreatePDFReport(int id, bool inBrowser)
        {
            var evaluation = unitOfWork.EvaluationRepository.GetByID(id);
            if (evaluation == null)
            {
                TempData["Confirmation"] = "Evaluation was not found.";
                return View("Index", "Admin");
            }
            evaluation.EvalStage = unitOfWork.StageRepository.GetByID(evaluation.EvalStageID);
            evaluation.EvalType = unitOfWork.TypeRepository.GetByID(evaluation.EvalTypeID);
            var employee = unitOfWork.EmployeeRepository.GetByID(evaluation.EmployeeID);
            var categories = unitOfWork.CategoryRepository.Get().Where(c => c.EvalTypeID == evaluation.EvalTypeID);
            var supervisorQuestions = new List<Question>();
            var coworkerQuestions = new List<Question>();
            var superviseeQuestions = new List<Question>();
            var raterEvals =
                unitOfWork.EvaluationRepository.Get()
                    .Where(
                        e =>
                            e.EmployeeID == employee.EmployeeID && e.RaterID != null && e.CohortID == employee.CohortID &&
                            e.Completed && e.StartDate == evaluation.StartDate && e.EndDate == evaluation.EndDate &&
                            e.EvalTypeID == evaluation.EvalTypeID && e.EvalStageID == evaluation.EvalStageID)
                    .ToList();
            if(this.isTest)
            {
                return View("Index", "Admin");
            }
            if (raterEvals.Count < employee.Raters.Where(r => r.Active).ToList().Count)
            {
                TempData["Confirmation"] = "Not all raters have completed this evaluation.";
                return RedirectToAction("GeneratePDF", "Admin", new { id = employee.EmployeeID });
            }

            foreach (var eval in raterEvals)
            {
                var role =
                    unitOfWork.RaterRoleRepository.Get()
                        .Where(r => r.RaterRoleID == eval.Rater.RaterRoleID)
                        .FirstOrDefault();
                if (role.RoleName.Equals("Supervisor"))
                {
                    supervisorQuestions =
                        unitOfWork.QuestionRepository.Get()
                            .Where(q => q.InEvaluation && q.EvaluationID == eval.EvaluationID)
                            .ToList();
                }
                else if (role.RoleName.Equals("Coworker"))
                {
                    coworkerQuestions =
                        unitOfWork.QuestionRepository.Get()
                            .Where(q => q.InEvaluation && q.EvaluationID == eval.EvaluationID)
                            .ToList();
                }
                else
                {
                    superviseeQuestions =
                        unitOfWork.QuestionRepository.Get()
                            .Where(q => q.InEvaluation && q.EvaluationID == eval.EvaluationID)
                            .ToList();
                }
            }
            foreach (var category in categories)
            {
                category.Questions =
                    unitOfWork.QuestionRepository.Get()
                        .Where(
                            q =>
                                q.InEvaluation && q.CategoryID == category.CategoryID &&
                                q.EvaluationID == evaluation.EvaluationID)
                        .ToList();
            }

            var pageSize = new Rectangle(0, 0, 900, 500);
            if (evaluation.EvalType.TypeName.Equals("Type 2"))
            {
                pageSize = new Rectangle(0, 0, 900, 700);
            }
            var report = new Document(pageSize, 50, 50, 25, 25);
            try
            {
                var fileName = employee.FirstName + employee.LastName + DateTime.Now.Year + "_" + DateTime.Now.Month +
                               "_" + DateTime.Now.Day + "_" + "EvalReport.pdf";
                var output = new MemoryStream();
                var writer = PdfWriter.GetInstance(report, output);

                report.Open();
                var title =
                    new Paragraph(
                        "Employee " + employee.FirstName + " " + employee.LastName +
                        " Evaluation Report (of Evaluation " +
                        evaluation.EvalType.TypeName + " " + evaluation.EvalStage.StageName + " -- generated on " +
                        DateTime.Now.Date + ")", FontFactory.GetFont("Arial", 10, Font.BOLD));
                title.SpacingAfter = 20f;
                title.Alignment = Element.ALIGN_CENTER;
                report.Add(title);
                float[] widths = new float[] {50, 50, 50, 50, 50, 50};
                PdfPTable table = new PdfPTable(widths);
                table.AddCell("");
                table.AddCell("Self");
                table.AddCell("AVG Supervisor");
                table.AddCell("AVG Co-worker");
                table.AddCell("AVG Supervisees");
                table.AddCell("Average");

                foreach (var category in categories)
                {
                    table.AddCell("");
                    var cell = new PdfPCell(new Phrase(category.CategoryName));
                    cell.Colspan = 5;
                    table.AddCell(cell);
                    foreach (var question in category.Questions)
                    {
                        table.AddCell(question.QuestionText);
                        table.AddCell(Convert.ToString(question.SelectedAnswerValue));
                        double avg = 0;
                        double sum = 0;
                        double numQuestions = 0;
                        double totalSum = 0;
                        double totalNumQuestions = 0;
                        foreach (var supervisorQuestion in supervisorQuestions)
                        {
                            if (supervisorQuestion.QuestionText.Equals(question.QuestionText))
                            {
                                sum += (int)supervisorQuestion.SelectedAnswerValue;
                                numQuestions++;
                            }
                        }
                        avg = sum / numQuestions;
                        avg = Math.Round(avg, 1);
                        totalSum += sum;
                        totalNumQuestions += numQuestions;
                        table.AddCell(Convert.ToString(avg));

                        avg = 0;
                        sum = 0;
                        numQuestions = 0;
                        foreach (var coworkerQuestion in coworkerQuestions)
                        {
                            if (coworkerQuestion.QuestionText.Equals(question.QuestionText))
                            {
                                sum += (int)coworkerQuestion.SelectedAnswerValue;
                                numQuestions++;
                            }
                        }
                        avg = sum / numQuestions;
                        avg = Math.Round(avg, 1);
                        totalSum += sum;
                        totalNumQuestions += numQuestions;
                        table.AddCell(Convert.ToString(avg));

                        avg = 0;
                        sum = 0;
                        numQuestions = 0;
                        foreach (var superviseeQuestion in superviseeQuestions)
                        {
                            if (superviseeQuestion.QuestionText.Equals(question.QuestionText))
                            {
                                sum += (int)superviseeQuestion.SelectedAnswerValue;
                                numQuestions++;
                            }
                        }
                        avg = sum / numQuestions;
                        avg = Math.Round(avg, 1);
                        totalSum += sum;
                        totalNumQuestions += numQuestions;
                        table.AddCell(Convert.ToString(avg));
                        table.AddCell(Convert.ToString(Math.Round((totalSum / totalNumQuestions), 1)));
                    }
                }

                report.Add(table);
                report.Close();
                if (inBrowser)
                {
                    Response.Clear();
                    Response.AddHeader("Content-Disposition", "inline; filename=" + fileName);
                    Response.Expires = 0;
                    Response.Buffer = true;
                    Response.ClearContent();
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Response.ContentType = "application/pdf";
                    Response.BinaryWrite(output.ToArray());
                    output.Close();
                    Response.End();
                }
                else
                {
                    Response.Clear();
                    Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName);
                    Response.Expires = 0;
                    Response.Buffer = true;
                    Response.ClearContent();
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Response.ContentType = "application/pdf";
                    Response.BinaryWrite(output.ToArray());
                    output.Close();
                    Response.End();
                }
                
            }
            catch (Exception e)
            {
                var exception = e;
            }
            
            

            return RedirectToAction("GeneratePDF", "Admin", new { id = employee.EmployeeID });
        }

        public ActionResult ViewEmployeeCompletedEval(int id)
        {
            if (this.authenticated())
            {
                var viewModel = new StartEvaluationViewModel();
                var evaluation = unitOfWork.EvaluationRepository.GetByID(id);
                if (evaluation != null)
                {
                    viewModel.Evaluation = evaluation;
                    viewModel.Categories =
                        unitOfWork.CategoryRepository.Get().Where(c => c.EvalTypeID == evaluation.EvalTypeID).ToList();

                    return View(viewModel);
                }
                TempData["Confirmation"] = "Evaluation not found.";
                return View("Index", "Admin");
            }
            return RedirectToAction("AdminLogin", "Home");
        }

        public ActionResult CurrentEvaluationStages()
        {
            var viewModel = new PendingViewModel();
            var cohorts = unitOfWork.CohortRepository.Get().ToList();
            var types = unitOfWork.TypeRepository.Get().ToList();
            var stages = unitOfWork.StageRepository.Get().ToList();
            var evaluations = new List<Evaluation>();

            viewModel.Cohorts = cohorts;

            foreach (var cohort in cohorts)
            {
                foreach (var type in types)
                {
                    foreach (var stage in stages)
                    {
                        var evaluation =
                            unitOfWork.EvaluationRepository.Get()
                                .Where(
                                    e =>
                                        e.CohortID == cohort.CohortID && e.EvalTypeID == type.EvalTypeID &&
                                        e.EvalStageID == stage.EvalStageID && e.Pending)
                                .FirstOrDefault();
                        if (evaluation != null)
                        {
                            evaluation.EvalType = unitOfWork.TypeRepository.GetByID(evaluation.EvalTypeID);
                            evaluation.EvalStage = unitOfWork.StageRepository.GetByID(evaluation.EvalStageID);
                            evaluations.Add(evaluation);
                        }
                    }
                }
            }

            viewModel.Evaluations = evaluations;

            return View(viewModel);
        }

        public ActionResult SendEmail()
        {
            if (this.authenticated())
            {
                return View();
            }
            else
            {
                return RedirectToAction("AdminLogin", "Home");
            }
        }

        private bool authenticated()
        {
            if (!(bool) Session["AdminLoggedIn"])
            {
                return false;
            }
            else
            {
                return true;
            }
        }

    }
}