﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using EmployeeEvaluator.DAL;
using EmployeeEvaluator.Models;
using System.Data.Entity;

namespace EmployeeEvaluator.Controllers
{
    public class RaterController : Controller
    {
        public UnitOfWork unitOfWork;
        private PlaceHolder placeHolder = new PlaceHolder();
        private bool isTest = false;

        public RaterController()
        {
            unitOfWork = new UnitOfWork();
        }

        public RaterController(IRepository<Rater> repository)
        {
            unitOfWork = new UnitOfWork();
            this.unitOfWork.RaterRepository = repository;
        }
        public RaterController(IRepository<Evaluation> evaluationRepository, IRepository<Cohort> cohortRepository, IRepository<Employee> employeeRepository, IRepository<EvalStage> stageRepository, IRepository<MaxRater> maxRaterRepository, IRepository<RaterRole> raterRoleRepository, IRepository<Category> categoryRepository, IRepository<EvalType> typeRepository, IRepository<Rater> raterRepository)
        {
            unitOfWork = new UnitOfWork();
            unitOfWork.EvaluationRepository = evaluationRepository;
            unitOfWork.CohortRepository = cohortRepository;
            unitOfWork.EmployeeRepository = employeeRepository;
            unitOfWork.StageRepository = stageRepository;
            unitOfWork.MaxRaterRepository = maxRaterRepository;
            unitOfWork.RaterRoleRepository = raterRoleRepository;
            unitOfWork.CategoryRepository = categoryRepository;
            unitOfWork.TypeRepository = typeRepository;
            unitOfWork.RaterRepository = raterRepository;
            this.isTest = true;
        }
        public ActionResult Index()
        {
            using(unitOfWork)
            {
                if (this.authenticatedAsAdmin())
                {
                    var viewModels = new List<RaterRoleViewModel>();
                    
                    var raters = unitOfWork.RaterRepository.Get().ToList();

                    foreach (var rater in raters)
                    {
                        var viewModel = new RaterRoleViewModel();
                        viewModel.Rater = rater;
                        viewModel.RaterRole = unitOfWork.RaterRoleRepository.GetByID(rater.RaterRoleID);
                        viewModels.Add(viewModel);
                    }
                    return View(viewModels);
                }
            }
            
            return RedirectToAction("Index", "Admin");
        }

        [HttpGet]
        public ActionResult Create()
        {
            if (this.authenticatedAsEmployee())
            {
                var rater = new Rater();
                rater.RaterID = unitOfWork.RaterRepository.Get().OrderByDescending(r => r.RaterID).FirstOrDefault().RaterID + 1;
                if (TempData.ContainsKey("ModelState"))
                    ModelState.Merge((ModelStateDictionary)TempData["ModelState"]);
                var addModel = new RaterViewModel();
                addModel.Rater = rater;
                addModel.RaterRoles = unitOfWork.RaterRoleRepository.Get().Where(r => r.RoleName != null).ToList().Select(x => new SelectListItem
                {
                    Value = x.RaterRoleID.ToString(),
                    Text = x.RoleName
                });

                return View(addModel);
            }
            else
            {
                return RedirectToAction("EmployeeLogin", "Home");
            }
        }

        [HttpPost]
        public ActionResult Create([Bind(Include = "FirstName, LastName, Email, RaterRoleID")]Rater rater)
        {
            if (rater.FirstName.Equals("") || rater.LastName.Equals("") || rater.Email.Equals("") || !ModelState.IsValid)
            {
                TempData["ModelState"] = ModelState;

                return RedirectToAction("Create", "Rater");
            }

            var employee = unitOfWork.EmployeeRepository.GetByID(Int32.Parse(Session["LoggedInEmployeeID"].ToString()));
            var employeeRaters = employee.Raters;
            var raterRole = unitOfWork.RaterRoleRepository.GetByID(rater.RaterRoleID);
            rater.Active = true;
            foreach (Rater employeeRater in employeeRaters)
            {
                if (employeeRater.Email == rater.Email && employeeRater.Active)
                {
                    TempData["Confirmation"] = "Employee already has a rater with this email assigned.";
                    return RedirectToAction("Create", "Rater");
                }
            }
            unitOfWork.RaterRepository.Insert(rater);
            unitOfWork.Save();
            employee.Raters.Add(rater);
            rater.Employees.Add(employee);
            unitOfWork.Save();
            unitOfWork.EmployeeRepository.Update(employee);
            unitOfWork.RaterRepository.Update(rater);
            unitOfWork.Save();
            return RedirectToAction("Index", "Employee");
        }

        public ActionResult Delete(int? id)
        {
            using (unitOfWork)
            {
                if (this.authenticatedAsAdmin())
                {
                    if (id == null)
                    {
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    }
                    Rater rater = unitOfWork.RaterRepository.GetByID(id);
                    if (rater == null && this.isTest)
                    {
                        return View("Index", "Home");
                    }
                    if (rater == null)
                    {
                        return HttpNotFound();
                    }
                    var viewModel = new RaterRoleViewModel();
                    viewModel.Rater = rater;
                    viewModel.RaterRole = unitOfWork.RaterRoleRepository.GetByID(rater.RaterRoleID);
                    return View(viewModel);
                }
                else
                {
                    return RedirectToAction("Index", "Rater");
                }
            }
            

        }

        // POST: Agent/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Rater rater = unitOfWork.RaterRepository.GetByID(id);
            unitOfWork.RaterRepository.Delete(rater);
            unitOfWork.Save();
            return RedirectToAction("Index", "Rater");
        }

        // GET: Cohort/Details/5
        public ActionResult Details(int? id)
        {
            if (this.authenticatedAsAdmin())
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Rater rater = unitOfWork.RaterRepository.GetByID(id);
                if(rater == null && this.isTest)
                {
                    return View("Index", "Home");
                }
                if (rater == null)
                {
                    return HttpNotFound();
                }
                var viewModel = new RaterRoleViewModel();
                viewModel.Rater = rater;
                viewModel.RaterRole = unitOfWork.RaterRoleRepository.GetByID(rater.RaterRoleID);
                return View(viewModel);
            }
            else
            {
                return RedirectToAction("Index", "Rater");
            }

        }
        [HttpGet]
        public ActionResult Edit(int? id)
        {
            if (this.authenticatedAsEmployee())
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Rater rater = unitOfWork.RaterRepository.GetByID(id);
                if (rater == null && this.isTest)
                {
                    return View("Index", "Home");
                }
                if (rater == null)
                {
                    return HttpNotFound();
                }
                rater.RaterID = (int)id;
                var editModel = new RaterViewModel();
                editModel.Rater = rater;
                editModel.RaterRoles = unitOfWork.RaterRoleRepository.Get().Where(r => r.RoleName != null).ToList().Select(x => new SelectListItem
                {
                    Value = x.RaterRoleID.ToString(),
                    Text = x.RoleName
                });
            
                return View(editModel);
            }
            else
            {
                return RedirectToAction("EmployeeLogin", "Home");
            }
        }

        [HttpPost]
        public ActionResult Edit([Bind(Include = "RaterID,FirstName,LastName,Email,RaterRoleID")] Rater rater)
        {
            try
            {

                if (ModelState.IsValid && authenticatedAsEmployee())
                {
                    var employee = unitOfWork.EmployeeRepository.GetByID(Int32.Parse(Session["LoggedInEmployeeID"].ToString()));
                    var employeeRaters = employee.Raters;
                    rater.Active = true;

                    foreach (Rater employeeRater in employeeRaters)
                    {
                        if (employeeRater.Email == rater.Email && employeeRater.Active)
                        {
                            TempData["Confirmation"] = "Employee already has a rater with this email assigned.";
                            return RedirectToAction("Edit", "Rater");
                        }
                    }   
                    unitOfWork.RaterRepository.Update(rater);
                    unitOfWork.Save();

                    return RedirectToAction("ViewRaters", "Employee");
                }
            }
            catch (DBConcurrencyException)
            {
                return RedirectToAction("ViewRaters", "Employee");
            }

            return View(rater);
        }

        private bool authenticatedAsAdmin()
        {
            if (!(bool)Session["AdminLoggedIn"])
            {
                return false;
            }
            return true;
        }

        private bool authenticatedAsEmployee()
        {
            if (!(bool)Session["EmployeeLoggedIn"])
            {
                return false;
            }
            return true;
        }
    }
}