﻿using System;
using EmployeeEvaluator.Models;

namespace EmployeeEvaluator.DAL
{
    public class UnitOfWork : IDisposable
    {
        private EvaluatorContext context = new EvaluatorContext();
        private IRepository<Employee> employeeRepository;
        private IRepository<Cohort> cohortRepository;
        private IRepository<Admin> adminRepository;
        private IRepository<Rater> raterRepository;
        private IRepository<Evaluation> evaluationRepository;
        private IRepository<Question> questionRepository;
        private IRepository<RaterRole> raterRoleRepository;
        private IRepository<EvalType> typeRepository;
        private IRepository<EvalStage> stageRepository;
        private IRepository<Category> categoryRepository;
        private IRepository<MaxRater> maxRaterRepository;

        public IRepository<Employee> EmployeeRepository
        {
            get
            {

                if (this.employeeRepository == null)
                {
                    this.employeeRepository = new GenericRepository<Employee>(context);
                }
                return employeeRepository;
            }

            set { this.employeeRepository = value; }
        }

        public IRepository<Cohort> CohortRepository
        {
            get
            {

                if (this.cohortRepository == null)
                {
                    this.cohortRepository = new GenericRepository<Cohort>(context);
                }
                return cohortRepository;
            }
            set { this.cohortRepository = value; }
        }

        public IRepository<Admin> AdminRepository
        {
            get
            {

                if (this.adminRepository == null)
                {
                    this.adminRepository = new GenericRepository<Admin>(context);
                }
                return adminRepository;
            }

            set { this.adminRepository = value; }
        }

        public IRepository<Rater> RaterRepository
        {
            get
            {

                if (this.raterRepository == null)
                {
                    this.raterRepository = new GenericRepository<Rater>(context);
                }
                return raterRepository;
            }
            set { this.raterRepository = value; }
        }

        public IRepository<Evaluation> EvaluationRepository
        {
            get
            {

                if (this.evaluationRepository == null)
                {
                    this.evaluationRepository = new GenericRepository<Evaluation>(context);
                }
                return evaluationRepository;
            }
            set { this.evaluationRepository = value; }
        }

        public IRepository<Question> QuestionRepository
        {
            get
            {

                if (this.questionRepository == null)
                {
                    this.questionRepository = new GenericRepository<Question>(context);
                }
                return questionRepository;
            }
        }

        public IRepository<RaterRole> RaterRoleRepository
        {
            get
            {

                if (this.raterRoleRepository == null)
                {
                    this.raterRoleRepository = new GenericRepository<RaterRole>(context);
                }
                return raterRoleRepository;
            }

            set { this.raterRoleRepository = value;}
        }

        public IRepository<EvalType> TypeRepository
        {
            get
            {

                if (this.typeRepository == null)
                {
                    this.typeRepository = new GenericRepository<EvalType>(context);
                }
                return typeRepository;
            }
            set { this.typeRepository = value; }
        }

        public IRepository<EvalStage> StageRepository
        {
            get
            {

                if (this.stageRepository == null)
                {
                    this.stageRepository = new GenericRepository<EvalStage>(context);
                }
                return stageRepository;
            }

            set { this.stageRepository = value; }
        }

        public IRepository<Category> CategoryRepository
        {
            get
            {

                if (this.categoryRepository == null)
                {
                    this.categoryRepository = new GenericRepository<Category>(context);
                }
                return categoryRepository;
            }

            set { this.categoryRepository = value; }
        }

        public IRepository<MaxRater> MaxRaterRepository
        {
            get
            {

                if (this.maxRaterRepository == null)
                {
                    this.maxRaterRepository = new GenericRepository<MaxRater>(context);
                }
                return maxRaterRepository;
            }

            set { this.maxRaterRepository = value; }
        }

        public void Save()
        {
            context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
