﻿using EmployeeEvaluator.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace EmployeeEvaluator.DAL
{
    public class EvaluatorContext : DbContext
    {

        public EvaluatorContext() : base("EvaluatorContext")
        {
            this.Configuration.LazyLoadingEnabled = true;
            this.Configuration.ProxyCreationEnabled = true;
        }

        public DbSet<Employee> Employees { get; set; }
        public DbSet<Cohort> Cohorts { get; set; }
        public DbSet<Admin> Admins { get; set; }
        public DbSet<Evaluation> Evaluations { get; set; }
        public DbSet<Rater> Raters { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<RaterRole> RaterRoles { get; set; }
        public DbSet<EvalType> EvalTypes { get; set; }
        public DbSet<EvalStage> EvalStages { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<MaxRater> MaxRaters { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}