﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EmployeeEvaluator.Models
{
    public class Question
    {
        [Key]
        public int QuestionID { get; set; }
        
        [Required]
        [DisplayName("Question")]
        public String QuestionText { get; set; } 
        public int? CategoryID { get; set; }
        public int? EvaluationID { get; set; }
        public bool InEvaluation { get; set; }
        public int? SelectedAnswerValue { get; set; }
        public virtual Category Category { get; set; }
        public virtual Evaluation Evaluation { get; set; }
    }
}