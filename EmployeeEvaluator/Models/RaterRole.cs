﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EmployeeEvaluator.Models
{
    public class RaterRole
    {
        [Key]
        [Required]
        public int RaterRoleID { get; set; }
        [DisplayName("Role")]
        [Required]
        public String RoleName { get; set; }
    }
}