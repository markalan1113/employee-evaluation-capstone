﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EmployeeEvaluator.Models
{
    public class RaterViewModel
    {
        public Rater Rater { get; set; }
        public IEnumerable<SelectListItem> RaterRoles { get; set; }
        public RaterRole RaterRole { get; set; }
    }
}