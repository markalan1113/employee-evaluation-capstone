﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmployeeEvaluator.Models
{
    public class CohortEvaluationViewModel
    {
        public List<Cohort> Cohorts { get; set; }
        public List<Evaluation> Evaluations { get; set; }
    }
}