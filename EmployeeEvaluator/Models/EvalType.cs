﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EmployeeEvaluator.Models
{
    public class EvalType
    {
        [Key]
        public int EvalTypeID { get; set; }
        [DisplayName("Type")]
        public string TypeName { get; set; }
    }
}