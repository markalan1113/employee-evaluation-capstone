﻿using System.Collections.Generic;


namespace EmployeeEvaluator.Models
{
    public class CohortIndexData
    {
        public IEnumerable<Cohort> Cohorts { get; set; }
        public IEnumerable<Employee> Employees { get; set; }
    }
}