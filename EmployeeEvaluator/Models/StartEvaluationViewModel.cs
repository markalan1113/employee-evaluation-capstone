﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmployeeEvaluator.Models
{
    public class StartEvaluationViewModel
    {
        public Evaluation Evaluation { get; set; }
        public List<Category> Categories { get; set; }
    }
}