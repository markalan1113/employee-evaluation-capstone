﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using EmployeeEvaluator.Models.Validation;

namespace EmployeeEvaluator.Models
{
    public class Employee
    {
        [Key]
        public int EmployeeID { get; set; }
        [Required]
        [DisplayName("First Name")]
        public string FirstName { get; set; }
        [Required]
        [DisplayName("Last Name")]
        public string LastName { get; set; }
        [Required]
        public string Email { get; set; }
        [DisplayName("Address")]
        [Required]
        public string MailingAddress { get; set; }
        [Required]
        [PhoneNumber]
        [DisplayName("Phone Number")]
        public string Phone { get; set; }
        public string HashedPassword { get; set; }
        public bool Active { get; set; }
        public int? CohortID { get; set; }
        public virtual ICollection<Rater> Raters { get; set; }
        public virtual ICollection<Evaluation> Evaluations { get; set; }

        public virtual Cohort Cohort { get; set; }

        public Employee()
        {
            this.Raters = new List<Rater>();
            this.Evaluations = new List<Evaluation>();
        }
    }
}