﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using EmployeeEvaluator.Models.Validation;

namespace EmployeeEvaluator.Models
{
    public class Rater
    {
        [Key]
        public int RaterID { get; set; }

        [Required]
        [DisplayName("Email")]
        [ValidEmail]
        public String Email { get; set; }
        [Required]
        [DisplayName("First Name")]
        public String FirstName { get; set; }
        [Required]
        [DisplayName("Last Name")]
        public String LastName { get; set; }
        public int RaterRoleID { get; set; }
        public bool Active { get; set; }

        public virtual ICollection<Employee> Employees { get; set; }
        public virtual ICollection<Evaluation> Evaluations { get; set; }

        public Rater()
        {
            this.Employees = new List<Employee>();
            this.Evaluations = new List<Evaluation>();
        }
    }
}