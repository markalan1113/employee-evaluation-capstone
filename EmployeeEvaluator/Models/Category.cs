﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EmployeeEvaluator.Models
{
    public class Category
    {
        [Key]
        public int CategoryID { get; set; }
        public int? EvalTypeID { get; set; }
        public EvalType EvalType { get; set; }
        public String CategoryName { get; set; }
        public String CategoryDescription { get; set; }
        public virtual ICollection<Question> Questions { get; set; }

        public Category()
        {
            Questions = new List<Question>();
        }
    }
}