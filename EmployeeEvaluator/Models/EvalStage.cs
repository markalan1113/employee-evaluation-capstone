﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace EmployeeEvaluator.Models
{
    public class EvalStage
    {
        [Key]
        public int EvalStageID { get; set; }
        [DisplayName("Stage")]
        public string StageName { get; set; }
    }
}