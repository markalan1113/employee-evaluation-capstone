﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using EmployeeEvaluator.Models.Validation;

namespace EmployeeEvaluator.Models
{
    public class Evaluation
    {
        [Key]
        public int EvaluationID { get; set; }
        public int CohortID { get; set; }
        public int? EmployeeID { get; set; }
        public int? RaterID { get; set; }
        public int? EvalTypeID { get; set; }
        public int? EvalStageID { get; set; }
        public bool Completed { get; set; }
        [DisplayName("Start Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Required]
        [DateBefore]
        public DateTime StartDate { get; set; }
        [DisplayName("End Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Required]
        public DateTime EndDate { get; set; }
        public bool Pending { get; set; }
        public virtual EvalType EvalType { get; set; }
        public virtual EvalStage EvalStage { get; set; }
        public virtual Employee Employee { get; set; }
        public virtual Rater Rater { get; set; }

        public virtual ICollection<Question> Questions { get; set; }

        public Evaluation()
        {
            Questions = new List<Question>();
        }
    }
}