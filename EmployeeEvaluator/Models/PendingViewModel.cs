﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmployeeEvaluator.Models
{
    public class PendingViewModel
    {
        public Cohort Cohort { get; set; }
        public List<Cohort> Cohorts { get; set; }
        public Evaluation Evaluation { get; set; }
        public List<Evaluation> Evaluations { get; set; }
    }
}