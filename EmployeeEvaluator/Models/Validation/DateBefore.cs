﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Web;

namespace EmployeeEvaluator.Models.Validation
{
    public class DateBefore : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var endDate = validationContext.ObjectType.GetProperty("EndDate").GetValue(validationContext.ObjectInstance, null);
            if (DateTime.Compare((DateTime) value, (DateTime) endDate) > 0 || DateTime.Compare((DateTime)value, (DateTime)endDate) == 0)
            {
                return new ValidationResult("Start date must be before end date.");
            }
            else
            {
                return ValidationResult.Success;
            }
        }
    }
}