﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;

namespace EmployeeEvaluator.Models.Validation
{
    public class ValidEmail : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            string val = (string)validationContext.ObjectType.GetProperty("Email").GetValue(validationContext.ObjectInstance, null);
            if (val != null)
            {
                string pattern = @"^[a-z][a-z|0-9|]*([_][a-z|0-9]+)*([.][a-z|0-9]+([_][a-z|0-9]+)*)?@[a-z][a-z|0-9|]*\.([a-z][a-z|0-9]*(\.[a-z][a-z|0-9]*)?)$";
                Match match = Regex.Match(val.Trim(), pattern, RegexOptions.IgnoreCase);

                if (match.Success)
                {
                    return ValidationResult.Success;
                }
                return new ValidationResult("Please enter a valid email address.");
            }

            return ValidationResult.Success;
        }
    }
}