﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;
namespace EmployeeEvaluator.Models.Validation
{
    public class PhoneNumber : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            string val = (string)validationContext.ObjectType.GetProperty("Phone").GetValue(validationContext.ObjectInstance, null);
            if (val != null)
            {
                string pattern = @"^[0-9]{3}-[0-9]{3}-[0-9]{4}$";
                Match match = Regex.Match(val.Trim(), pattern, RegexOptions.IgnoreCase);

                if (match.Success)
                {
                    return ValidationResult.Success;
                }
                return new ValidationResult("Please enter a phone number in the format xxx-xxx-xxxx.");
            }
            return ValidationResult.Success;
        }
    }
}