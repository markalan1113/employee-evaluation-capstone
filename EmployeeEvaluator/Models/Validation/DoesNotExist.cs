﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;
using EmployeeEvaluator.DAL;

namespace EmployeeEvaluator.Models.Validation
{
    public class DoesNotExist : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            EvaluatorContext db = new EvaluatorContext();
            var cohort = db.Cohorts.Where(c => c.Name == (string)value).FirstOrDefault();
            if (cohort == null)
            {
                return ValidationResult.Success;
            }
            else
            {
                return new ValidationResult("Cohort name must be unique.");
            }
        }
    }
}