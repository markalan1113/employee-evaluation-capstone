﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmployeeEvaluator.Models
{
    public class MaxRater
    {
        public int MaxRaterID { get; set; }
        public int RaterRoleID { get; set; }
        public int Max { get; set; }
        public virtual RaterRole RaterRole { get; set; }     
    }
}