﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EmployeeEvaluator.Models
{
    public class EvaluationViewModel
    {
        public Evaluation Evaluation { get; set; }
        public IEnumerable<SelectListItem> EvalTypes { get; set; }
        public IEnumerable<SelectListItem> EvalStages { get; set; }
        public EvalType EvalType { get; set; }
        public EvalStage EvalStage { get; set; }
    }
}