﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EmployeeEvaluator.Models
{
    public class Admin
    {
        [Key]
        public int AdminID { get; set; }
        public string Email { get; set; }
        public string HashedPassword { get; set; }   
    }
}