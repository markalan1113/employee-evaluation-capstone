﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using EmployeeEvaluator.Models.Validation;

namespace EmployeeEvaluator.Models
{
    public class Cohort
    {
        [Key]
        [DisplayName("Cohort")]
        public int CohortID { get; set; }
        [Required]
        [DisplayName("Cohort Name")]
        public string Name { get; set; }
        public bool Stable { get; set; }
    }
}