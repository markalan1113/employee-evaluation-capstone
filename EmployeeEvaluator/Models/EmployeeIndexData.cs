﻿using System.Collections.Generic;


namespace EmployeeEvaluator.Models
{
    public class EmployeeIndexData
    {
        public IEnumerable<Cohort> Cohorts { get; set; }
        public IEnumerable<Employee> Employees { get; set; }
        public IEnumerable<Evaluation> Evaluations { get; set; }
    }
}