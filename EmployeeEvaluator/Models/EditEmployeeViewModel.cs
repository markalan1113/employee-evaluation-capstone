﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EmployeeEvaluator.Models
{
    public class EditEmployeeViewModel
    {
        public Employee Employee { get; set; }
        public IEnumerable<SelectListItem> Cohorts { get; set; }
    }
}