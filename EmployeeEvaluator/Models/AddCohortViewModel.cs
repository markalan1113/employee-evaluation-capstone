﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EmployeeEvaluator.Models
{
    public class AddCohortViewModel
    {
        public Cohort Cohort { get; set; }
        public IEnumerable<SelectListItem> Types = new List<SelectListItem> {new SelectListItem {Value = "Type1", Text = "Type 1"}, new SelectListItem() {Value = "Type2", Text = "Type 2"} };
        public IEnumerable<SelectListItem> Stages = new List<SelectListItem> { new SelectListItem { Value = "Baseline", Text = "Baseline" }, new SelectListItem { Value = "Formative", Text = "Formative" }, new SelectListItem {Value = "Summative", Text = "Summative"} };
    }
}