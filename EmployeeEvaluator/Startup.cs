﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(EmployeeEvaluator.Startup))]
namespace EmployeeEvaluator
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
