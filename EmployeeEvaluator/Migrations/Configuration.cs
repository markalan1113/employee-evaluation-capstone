namespace EmployeeEvaluator.Migrations
{
    using Microsoft.AspNet.Identity;
    using Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.Security.Cryptography;
    using System.Text;

    internal sealed class Configuration : DbMigrationsConfiguration<EmployeeEvaluator.DAL.EvaluatorContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(EmployeeEvaluator.DAL.EvaluatorContext context)
        {
            string salt = "92429d82a41e930486c6de5ebda9602d55c39986";
            var password = "password";
            var bytes = Encoding.UTF8.GetBytes(password + salt);
            var hashedPassword = Convert.ToBase64String((new SHA256Managed()).ComputeHash(bytes));

            context.Admins.RemoveRange(context.Admins);
            context.Employees.RemoveRange(context.Employees);
            context.Cohorts.RemoveRange(context.Cohorts);
            context.Questions.RemoveRange(context.Questions);
            context.Evaluations.RemoveRange(context.Evaluations);
            context.Raters.RemoveRange(context.Raters);
            context.RaterRoles.RemoveRange(context.RaterRoles);
            context.EvalTypes.RemoveRange(context.EvalTypes);
            context.EvalStages.RemoveRange(context.EvalStages);
            context.Categories.RemoveRange(context.Categories);
            context.MaxRaters.RemoveRange(context.MaxRaters);

            

            Admin admin1 = new Admin
            {
                AdminID = 1,
                Email = "admin@admin.com",
                HashedPassword = hashedPassword
            };
            Admin admin2 = new Admin
            {
                AdminID = 2,
                Email = "user2@mailinator.com",
                HashedPassword = hashedPassword
            };
            context.Admins.Add(admin1);
            context.Admins.Add(admin2);
            context.SaveChanges();

            Cohort cohort1 = new Cohort
            {
                CohortID = 1,
                Name = "cohort1",
                Stable = false
            };
            Cohort cohort2 = new Cohort
            {
                CohortID = 2,
                Name = "cohort2",
                Stable = false
            };

            context.Cohorts.Add(cohort1);
            context.Cohorts.Add(cohort2);
            context.SaveChanges();

            Employee employee1 = new Employee
            {
                EmployeeID = 1,
                Email = "employee1@mailinator.com",
                FirstName = "John",
                LastName = "Smith",
                MailingAddress = "123 Street",
                Phone = "678-523-5415",
                CohortID = cohort1.CohortID,
                Active = true
            };
            Employee employee2 = new Employee
            {
                EmployeeID = 2,
                Email = "employee2@mailinator.com",
                FirstName = "Bob",
                LastName = "Jones",
                MailingAddress = "234 Street",
                Phone = "678-452-3645",
                CohortID = cohort1.CohortID,
                Active = true
            };
            RaterRole role1 = new RaterRole
            {
                RoleName = "Supervisor"
            };
            RaterRole role2 = new RaterRole
            {
                RoleName = "Coworker"
            };
            RaterRole role3 = new RaterRole
            { 
                RoleName = "Supervisee"
            };
            context.RaterRoles.Add(role1);
            context.RaterRoles.Add(role2);
            context.RaterRoles.Add(role3);
            context.SaveChanges();

            MaxRater maxSupervisor = new MaxRater
            {
                RaterRoleID = role1.RaterRoleID,
                Max = 1
            };
            MaxRater maxSupervisee = new MaxRater
            {
                RaterRoleID = role3.RaterRoleID,
                Max = 2
            };
            MaxRater maxCoworker = new MaxRater
            {
                RaterRoleID = role2.RaterRoleID,
                Max = 2
            };

            context.MaxRaters.Add(maxSupervisor);
            context.MaxRaters.Add(maxSupervisee);
            context.MaxRaters.Add(maxCoworker);
            context.SaveChanges();

            Rater rater1 = new Rater
            {
                Email = "rater1@mailinator.com",
                FirstName = "Rater",
                LastName = "One",
                RaterRoleID = role1.RaterRoleID,
                Active = true
            };
            Rater rater2 = new Rater
            {
                Email = "rater2@mailinator.com",
                FirstName = "Rater",
                LastName = "Two",
                RaterRoleID = role2.RaterRoleID,
                Active = true
            };
            Rater rater3 = new Rater
            {
                Email = "rater3@mailinator.com",
                FirstName = "Rater",
                LastName = "Three",
                RaterRoleID = role2.RaterRoleID,
                Active = true
            };
            Rater rater4 = new Rater
            {
                Email = "rater4@mailinator.com",
                FirstName = "Rater",
                LastName = "Four",
                RaterRoleID = role3.RaterRoleID,
                Active = true
            };
            Rater rater5 = new Rater
            {
                Email = "rater5@mailinator.com",
                FirstName = "Rater",
                LastName = "Five",
                RaterRoleID = role3.RaterRoleID,
                Active = true
            };

            employee1.Raters.Add(rater1);
            employee1.Raters.Add(rater2);
            employee1.Raters.Add(rater3);
            employee1.Raters.Add(rater4);
            employee1.Raters.Add(rater5);
            rater1.Employees.Add(employee1);
            rater2.Employees.Add(employee1);
            rater3.Employees.Add(employee1);
            rater4.Employees.Add(employee1);
            rater5.Employees.Add(employee1);
            context.Employees.Add(employee1);
            context.Employees.Add(employee2);
            context.SaveChanges();

            EvalStage baseline = new EvalStage
            {
                StageName = "Baseline"
            };
            EvalStage formative = new EvalStage
            {
                StageName = "Formative"
            };
            EvalStage summative = new EvalStage
            {
                StageName = "Summative"
            };

            EvalType type1 = new EvalType
            {
                TypeName = "Type 1"
            };
            EvalType type2 = new EvalType
            {
                TypeName = "Type 2"
            };
            context.EvalTypes.Add(type1);
            context.EvalTypes.Add(type2);
            context.EvalStages.Add(baseline);
            context.EvalStages.Add(formative);
            context.EvalStages.Add(summative);
            context.SaveChanges();

            Category type1category1 = new Category
            {
                EvalTypeID = type1.EvalTypeID,
                CategoryName = "Leadership",
                CategoryDescription = "Questions about Leadership."
            };
            Category type1category2 = new Category
            {
                EvalTypeID = type1.EvalTypeID,
                CategoryName = "Work Ethic",
                CategoryDescription = "Questions about Work Ethic."
            };
            Category type1category3 = new Category
            {
                EvalTypeID = type1.EvalTypeID,
                CategoryName = "Punctuality",
                CategoryDescription = "Questions about Punctuality."
            };
            Category type1category4 = new Category
            {
                EvalTypeID = type1.EvalTypeID,
                CategoryName = "Type 1 Category 4",
                CategoryDescription = "Questions about Type 1 Category 4."
            };
            Category type1category5 = new Category
            {
                EvalTypeID = type1.EvalTypeID,
                CategoryName = "Type 1 Category 5",
                CategoryDescription = "Questions about Type 1 Category 5."
            };

            Category type2category1 = new Category
            {
                EvalTypeID = type2.EvalTypeID,
                CategoryName = "Type 2 Category 1",
                CategoryDescription = "Questions about Type 2 Category 1."
            };
            Category type2category2 = new Category
            {
                EvalTypeID = type2.EvalTypeID,
                CategoryName = "Type 2 Category 2",
                CategoryDescription = "Questions about Type 2 Category 2."
            };
            Category type2category3 = new Category
            {
                EvalTypeID = type2.EvalTypeID,
                CategoryName = "Type 2 Category 3",
                CategoryDescription = "Questions about Type 2 Category 3."
            };
            Category type2category4 = new Category
            {
                EvalTypeID = type2.EvalTypeID,
                CategoryName = "Type 2 Category 4",
                CategoryDescription = "Questions about Type 2 Category 4."
            };
            Category type2category5 = new Category
            {
                EvalTypeID = type2.EvalTypeID,
                CategoryName = "Type 2 Category 5",
                CategoryDescription = "Questions about Type 2 Category 5."
            };
            Category type2category6 = new Category
            {
                EvalTypeID = type2.EvalTypeID,
                CategoryName = "Type 2 Category 6",
                CategoryDescription = "Questions about Type 2 Category 6."
            };
            Category type2category7 = new Category
            {
                EvalTypeID = type2.EvalTypeID,
                CategoryName = "Type 2 Category 7",
                CategoryDescription = "Questions about Type 2 Category 7."
            };

            context.Categories.Add(type1category1);
            context.Categories.Add(type1category2);
            context.Categories.Add(type1category3);
            context.Categories.Add(type1category4);
            context.Categories.Add(type1category5);
            context.Categories.Add(type2category1);
            context.Categories.Add(type2category2);
            context.Categories.Add(type2category3);
            context.Categories.Add(type2category4);
            context.Categories.Add(type2category5);
            context.Categories.Add(type2category6);
            context.Categories.Add(type2category7);
            context.SaveChanges();

            Question type1category1question1 = new Question
            {
                QuestionText = "T1C1Q1",
                CategoryID = type1category1.CategoryID,
                InEvaluation = false
            };
            Question type1category1question2 = new Question
            {
                QuestionText = "T1C1Q2",
                CategoryID = type1category1.CategoryID,
                InEvaluation = false
            };
            Question type1category1question3 = new Question
            {
                QuestionText = "T1C1Q3",
                CategoryID = type1category1.CategoryID,
                InEvaluation = false
            };
            Question type1category2question1 = new Question
            {
                QuestionText = "T1C2Q1",
                CategoryID = type1category2.CategoryID,
                InEvaluation = false
            };
            Question type1category2question2 = new Question
            {
                QuestionText = "T1C2Q2",
                CategoryID = type1category2.CategoryID,
                InEvaluation = false
            };
            Question type1category2question3 = new Question
            {
                QuestionText = "T1C2Q3",
                CategoryID = type1category2.CategoryID,
                InEvaluation = false
            };
            Question type1category3question1 = new Question
            {
                QuestionText = "T1C3Q1",
                CategoryID = type1category3.CategoryID,
                InEvaluation = false
            };
            Question type1category3question2 = new Question
            {
                QuestionText = "T1C3Q2",
                CategoryID = type1category3.CategoryID,
                InEvaluation = false
            };
            Question type1category3question3 = new Question
            {
                QuestionText = "T1C3Q3",
                CategoryID = type1category3.CategoryID,
                InEvaluation = false
            };
            Question type1category4question1 = new Question
            {
                QuestionText = "T1C4Q1",
                CategoryID = type1category4.CategoryID,
                InEvaluation = false
            };
            Question type1category4question2 = new Question
            {
                QuestionText = "T1C4Q2",
                CategoryID = type1category4.CategoryID,
                InEvaluation = false
            };
            Question type1category4question3 = new Question
            {
                QuestionText = "T1C4Q3",
                CategoryID = type1category4.CategoryID,
                InEvaluation = false
            };
            Question type1category5question1 = new Question
            {
                QuestionText = "T1C5Q1",
                CategoryID = type1category5.CategoryID,
                InEvaluation = false
            };
            Question type1category5question2 = new Question
            {
                QuestionText = "T1C5Q2",
                CategoryID = type1category5.CategoryID,
                InEvaluation = false
            };
            Question type1category5question3 = new Question
            {
                QuestionText = "T1C5Q3",
                CategoryID = type1category5.CategoryID,
                InEvaluation = false
            };
            Question type2category1question1 = new Question
            {
                QuestionText = "T2C1Q1",
                CategoryID = type2category1.CategoryID,
                InEvaluation = false
            };
            Question type2category1question2 = new Question
            {
                QuestionText = "T2C1Q2",
                CategoryID = type2category1.CategoryID,
                InEvaluation = false
            };
            Question type2category1question3 = new Question
            {
                QuestionText = "T2C1Q3",
                CategoryID = type2category1.CategoryID,
                InEvaluation = false
            };
            Question type2category1question4 = new Question
            {
                QuestionText = "T2C1Q4",
                CategoryID = type2category1.CategoryID,
                InEvaluation = false
            };
            Question type2category2question1 = new Question
            {
                QuestionText = "T2C2Q1",
                CategoryID = type2category2.CategoryID,
                InEvaluation = false
            };
            Question type2category2question2 = new Question
            {
                QuestionText = "T2C2Q2",
                CategoryID = type2category2.CategoryID,
                InEvaluation = false
            };
            Question type2category2question3 = new Question
            {
                QuestionText = "T2C2Q3",
                CategoryID = type2category2.CategoryID,
                InEvaluation = false
            };
            Question type2category2question4 = new Question
            {
                QuestionText = "T2C2Q4",
                CategoryID = type2category2.CategoryID,
                InEvaluation = false
            };

            Question type2category3question1 = new Question
            {
                QuestionText = "T2C3Q1",
                CategoryID = type2category3.CategoryID,
                InEvaluation = false
            };
            Question type2category3question2 = new Question
            {
                QuestionText = "T2C3Q2",
                CategoryID = type2category3.CategoryID,
                InEvaluation = false
            };
            Question type2category3question3 = new Question
            {
                QuestionText = "T2C3Q3",
                CategoryID = type2category3.CategoryID,
                InEvaluation = false
            };
            Question type2category3question4 = new Question
            {
                QuestionText = "T2C3Q4",
                CategoryID = type2category3.CategoryID,
                InEvaluation = false
            };

            Question type2category4question1 = new Question
            {
                QuestionText = "T2C4Q1",
                CategoryID = type2category4.CategoryID,
                InEvaluation = false
            };
            Question type2category4question2 = new Question
            {
                QuestionText = "T2C4Q2",
                CategoryID = type2category4.CategoryID,
                InEvaluation = false
            };
            Question type2category4question3 = new Question
            {
                QuestionText = "T2C4Q3",
                CategoryID = type2category4.CategoryID,
                InEvaluation = false
            };
            Question type2category4question4 = new Question
            {
                QuestionText = "T2C4Q4",
                CategoryID = type2category4.CategoryID,
                InEvaluation = false
            };

            Question type2category5question1 = new Question
            {
                QuestionText = "T2C5Q1",
                CategoryID = type2category5.CategoryID,
                InEvaluation = false
            };
            Question type2category5question2 = new Question
            {
                QuestionText = "T2C5Q2",
                CategoryID = type2category5.CategoryID,
                InEvaluation = false
            };
            Question type2category5question3 = new Question
            {
                QuestionText = "T2C5Q3",
                CategoryID = type2category5.CategoryID,
                InEvaluation = false
            };
            Question type2category5question4 = new Question
            {
                QuestionText = "T2C5Q4",
                CategoryID = type2category5.CategoryID,
                InEvaluation = false
            };

            Question type2category6question1 = new Question
            {
                QuestionText = "T2C6Q1",
                CategoryID = type2category6.CategoryID,
                InEvaluation = false
            };
            Question type2category6question2 = new Question
            {
                QuestionText = "T2C6Q2",
                CategoryID = type2category6.CategoryID,
                InEvaluation = false
            };
            Question type2category6question3 = new Question
            {
                QuestionText = "T2C6Q3",
                CategoryID = type2category6.CategoryID,
                InEvaluation = false
            };
            Question type2category6question4 = new Question
            {
                QuestionText = "T2C6Q4",
                CategoryID = type2category6.CategoryID,
                InEvaluation = false
            };

            Question type2category7question1 = new Question
            {
                QuestionText = "T2C7Q1",
                CategoryID = type2category7.CategoryID,
                InEvaluation = false
            };
            Question type2category7question2 = new Question
            {
                QuestionText = "T2C7Q2",
                CategoryID = type2category7.CategoryID,
                InEvaluation = false
            };
            Question type2category7question3 = new Question
            {
                QuestionText = "T2C7Q3",
                CategoryID = type2category7.CategoryID,
                InEvaluation = false
            };
            Question type2category7question4 = new Question
            {
                QuestionText = "T2C7Q4",
                CategoryID = type2category7.CategoryID,
                InEvaluation = false
            };
            context.Questions.Add(type1category1question1);
            context.SaveChanges();
            context.Questions.Add(type1category1question2);
            context.SaveChanges();
            context.Questions.Add(type1category1question3);
            context.SaveChanges();
            context.Questions.Add(type1category2question1);
            context.SaveChanges();
            context.Questions.Add(type1category2question2);
            context.SaveChanges();
            context.Questions.Add(type1category2question3);
            context.SaveChanges();
            context.Questions.Add(type1category3question1);
            context.SaveChanges();
            context.Questions.Add(type1category3question2);
            context.SaveChanges();
            context.Questions.Add(type1category3question3);
            context.SaveChanges();
            context.Questions.Add(type1category4question1);
            context.SaveChanges();
            context.Questions.Add(type1category4question2);
            context.SaveChanges();
            context.Questions.Add(type1category4question3);
            context.SaveChanges();
            context.Questions.Add(type1category5question1);
            context.SaveChanges();
            context.Questions.Add(type1category5question2);
            context.SaveChanges();
            context.Questions.Add(type1category5question3);
            context.SaveChanges();

            context.Questions.Add(type2category1question1);
            context.SaveChanges();
            context.Questions.Add(type2category1question2);
            context.SaveChanges();
            context.Questions.Add(type2category1question3);
            context.SaveChanges();
            context.Questions.Add(type2category1question4);
            context.SaveChanges();
            context.Questions.Add(type2category2question1);
            context.SaveChanges();
            context.Questions.Add(type2category2question2);
            context.SaveChanges();
            context.Questions.Add(type2category2question3);
            context.SaveChanges();
            context.Questions.Add(type2category2question4);
            context.SaveChanges();
            context.Questions.Add(type2category3question1);
            context.SaveChanges();
            context.Questions.Add(type2category3question2);
            context.SaveChanges();
            context.Questions.Add(type2category3question3);
            context.SaveChanges();
            context.Questions.Add(type2category3question4);
            context.SaveChanges();
            context.Questions.Add(type2category4question1);
            context.SaveChanges();
            context.Questions.Add(type2category4question2);
            context.SaveChanges();
            context.Questions.Add(type2category4question3);
            context.SaveChanges();
            context.Questions.Add(type2category4question4);
            context.SaveChanges();
            context.Questions.Add(type2category5question1);
            context.SaveChanges();
            context.Questions.Add(type2category5question2);
            context.SaveChanges();
            context.Questions.Add(type2category5question3);
            context.SaveChanges();
            context.Questions.Add(type2category5question4);
            context.SaveChanges();
            context.Questions.Add(type2category6question1);
            context.SaveChanges();
            context.Questions.Add(type2category6question2);
            context.SaveChanges();
            context.Questions.Add(type2category6question3);
            context.SaveChanges();
            context.Questions.Add(type2category6question4);
            context.SaveChanges();
            context.Questions.Add(type2category7question1);
            context.SaveChanges();
            context.Questions.Add(type2category7question2);
            context.SaveChanges();
            context.Questions.Add(type2category7question3);
            context.SaveChanges();
            context.Questions.Add(type2category7question4);
            context.SaveChanges();
        }
    }
}