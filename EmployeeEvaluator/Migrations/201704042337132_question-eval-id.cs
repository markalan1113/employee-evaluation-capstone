namespace EmployeeEvaluator.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class questionevalid : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Question", "Evaluation_EvaluationID", "dbo.Evaluation");
            DropIndex("dbo.Question", new[] { "Evaluation_EvaluationID" });
            RenameColumn(table: "dbo.Question", name: "Evaluation_EvaluationID", newName: "EvaluationID");
            AlterColumn("dbo.Question", "EvaluationID", c => c.Int(nullable: false));
            CreateIndex("dbo.Question", "EvaluationID");
            AddForeignKey("dbo.Question", "EvaluationID", "dbo.Evaluation", "EvaluationID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Question", "EvaluationID", "dbo.Evaluation");
            DropIndex("dbo.Question", new[] { "EvaluationID" });
            AlterColumn("dbo.Question", "EvaluationID", c => c.Int());
            RenameColumn(table: "dbo.Question", name: "EvaluationID", newName: "Evaluation_EvaluationID");
            CreateIndex("dbo.Question", "Evaluation_EvaluationID");
            AddForeignKey("dbo.Question", "Evaluation_EvaluationID", "dbo.Evaluation", "EvaluationID");
        }
    }
}
