namespace EmployeeEvaluator.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class evaluations : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Employee", "Rater_RaterID", "dbo.Rater");
            DropIndex("dbo.Employee", new[] { "Rater_RaterID" });
            CreateTable(
                "dbo.Question",
                c => new
                    {
                        QuestionID = c.Int(nullable: false, identity: true),
                        QuestionText = c.String(nullable: false),
                        Evaluation_EvaluationID = c.Int(),
                    })
                .PrimaryKey(t => t.QuestionID)
                .ForeignKey("dbo.Evaluation", t => t.Evaluation_EvaluationID)
                .Index(t => t.Evaluation_EvaluationID);
            
            CreateTable(
                "dbo.RaterEmployee",
                c => new
                    {
                        Rater_RaterID = c.Int(nullable: false),
                        Employee_EmployeeID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Rater_RaterID, t.Employee_EmployeeID })
                .ForeignKey("dbo.Rater", t => t.Rater_RaterID, cascadeDelete: true)
                .ForeignKey("dbo.Employee", t => t.Employee_EmployeeID, cascadeDelete: true)
                .Index(t => t.Rater_RaterID)
                .Index(t => t.Employee_EmployeeID);
            
            AddColumn("dbo.Rater", "Role", c => c.String(nullable: false));
            DropColumn("dbo.Employee", "Rater_RaterID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Employee", "Rater_RaterID", c => c.Int());
            DropForeignKey("dbo.Question", "Evaluation_EvaluationID", "dbo.Evaluation");
            DropForeignKey("dbo.RaterEmployee", "Employee_EmployeeID", "dbo.Employee");
            DropForeignKey("dbo.RaterEmployee", "Rater_RaterID", "dbo.Rater");
            DropIndex("dbo.RaterEmployee", new[] { "Employee_EmployeeID" });
            DropIndex("dbo.RaterEmployee", new[] { "Rater_RaterID" });
            DropIndex("dbo.Question", new[] { "Evaluation_EvaluationID" });
            DropColumn("dbo.Rater", "Role");
            DropTable("dbo.RaterEmployee");
            DropTable("dbo.Question");
            CreateIndex("dbo.Employee", "Rater_RaterID");
            AddForeignKey("dbo.Employee", "Rater_RaterID", "dbo.Rater", "RaterID");
        }
    }
}
