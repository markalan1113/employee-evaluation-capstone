namespace EmployeeEvaluator.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class intnull : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Evaluation", "CompletedEvaluationID", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Evaluation", "CompletedEvaluationID", c => c.Int(nullable: false));
        }
    }
}
