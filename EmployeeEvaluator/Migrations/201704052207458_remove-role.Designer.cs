// <auto-generated />
namespace EmployeeEvaluator.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class removerole : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(removerole));
        
        string IMigrationMetadata.Id
        {
            get { return "201704052207458_remove-role"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
