namespace EmployeeEvaluator.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class added_email_to_rater : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Rater", "Email", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Rater", "Email");
        }
    }
}
