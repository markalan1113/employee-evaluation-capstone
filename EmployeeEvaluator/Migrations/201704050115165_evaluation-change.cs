namespace EmployeeEvaluator.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class evaluationchange : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Evaluation", "InitialEvaluationID", c => c.Int());
            DropColumn("dbo.Evaluation", "Completed");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Evaluation", "Completed", c => c.Boolean(nullable: false));
            DropColumn("dbo.Evaluation", "InitialEvaluationID");
        }
    }
}
