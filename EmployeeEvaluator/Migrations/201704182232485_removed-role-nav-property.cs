namespace EmployeeEvaluator.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removedrolenavproperty : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Rater", "RaterRoleID", "dbo.RaterRole");
            DropIndex("dbo.Rater", new[] { "RaterRoleID" });
            AlterColumn("dbo.Rater", "RaterRoleID", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Rater", "RaterRoleID", c => c.Int());
            CreateIndex("dbo.Rater", "RaterRoleID");
            AddForeignKey("dbo.Rater", "RaterRoleID", "dbo.RaterRole", "RaterRoleID");
        }
    }
}
