namespace EmployeeEvaluator.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class evaluationcompleted : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Evaluation", "Completed", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Evaluation", "Completed");
        }
    }
}
