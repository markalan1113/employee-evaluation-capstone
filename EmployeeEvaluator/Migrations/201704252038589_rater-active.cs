namespace EmployeeEvaluator.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class rateractive : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Rater", "Active", c => c.Boolean(nullable: false, defaultValue: true));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Rater", "Active");
        }
    }
}
