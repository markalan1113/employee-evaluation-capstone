namespace EmployeeEvaluator.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test1 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Evaluation", "EmployeeID", "dbo.Employee");
            DropIndex("dbo.Evaluation", new[] { "EmployeeID" });
            AlterColumn("dbo.Evaluation", "EmployeeID", c => c.Int());
            CreateIndex("dbo.Evaluation", "EmployeeID");
            AddForeignKey("dbo.Evaluation", "EmployeeID", "dbo.Employee", "EmployeeID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Evaluation", "EmployeeID", "dbo.Employee");
            DropIndex("dbo.Evaluation", new[] { "EmployeeID" });
            AlterColumn("dbo.Evaluation", "EmployeeID", c => c.Int(nullable: false));
            CreateIndex("dbo.Evaluation", "EmployeeID");
            AddForeignKey("dbo.Evaluation", "EmployeeID", "dbo.Employee", "EmployeeID", cascadeDelete: true);
        }
    }
}
