namespace EmployeeEvaluator.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class raterrolefknullable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Rater", "RaterRoleID", "dbo.RaterRole");
            DropIndex("dbo.Rater", new[] { "RaterRoleID" });
            AlterColumn("dbo.Rater", "RaterRoleID", c => c.Int());
            CreateIndex("dbo.Rater", "RaterRoleID");
            AddForeignKey("dbo.Rater", "RaterRoleID", "dbo.RaterRole", "RaterRoleID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Rater", "RaterRoleID", "dbo.RaterRole");
            DropIndex("dbo.Rater", new[] { "RaterRoleID" });
            AlterColumn("dbo.Rater", "RaterRoleID", c => c.Int(nullable: false));
            CreateIndex("dbo.Rater", "RaterRoleID");
            AddForeignKey("dbo.Rater", "RaterRoleID", "dbo.RaterRole", "RaterRoleID", cascadeDelete: true);
        }
    }
}
