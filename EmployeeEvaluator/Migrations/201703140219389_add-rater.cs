namespace EmployeeEvaluator.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addrater : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Rater",
                c => new
                    {
                        RaterID = c.Int(nullable: false, identity: true),
                        FirstName = c.String(nullable: false),
                        LastName = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.RaterID);
            
            AddColumn("dbo.Employee", "Rater_RaterID", c => c.Int());
            CreateIndex("dbo.Employee", "Rater_RaterID");
            AddForeignKey("dbo.Employee", "Rater_RaterID", "dbo.Rater", "RaterID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Employee", "Rater_RaterID", "dbo.Rater");
            DropIndex("dbo.Employee", new[] { "Rater_RaterID" });
            DropColumn("dbo.Employee", "Rater_RaterID");
            DropTable("dbo.Rater");
        }
    }
}
