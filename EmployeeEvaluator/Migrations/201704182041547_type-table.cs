namespace EmployeeEvaluator.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class typetable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EvalType",
                c => new
                    {
                        TypeName = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.TypeName);
            
            AddColumn("dbo.Evaluation", "TypeName", c => c.String(maxLength: 128));
            CreateIndex("dbo.Evaluation", "TypeName");
            AddForeignKey("dbo.Evaluation", "TypeName", "dbo.EvalType", "TypeName");
            DropColumn("dbo.Evaluation", "Type");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Evaluation", "Type", c => c.String());
            DropForeignKey("dbo.Evaluation", "TypeName", "dbo.EvalType");
            DropIndex("dbo.Evaluation", new[] { "TypeName" });
            DropColumn("dbo.Evaluation", "TypeName");
            DropTable("dbo.EvalType");
        }
    }
}
