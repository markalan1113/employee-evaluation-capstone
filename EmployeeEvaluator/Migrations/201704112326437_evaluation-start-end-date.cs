namespace EmployeeEvaluator.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class evaluationstartenddate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Evaluation", "Completed", c => c.Boolean(nullable: false));
            AddColumn("dbo.Evaluation", "StartDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Evaluation", "EndDate", c => c.DateTime(nullable: false));
            DropColumn("dbo.Cohort", "StartDate");
            DropColumn("dbo.Cohort", "EndDate");
            DropColumn("dbo.Evaluation", "InitialEvaluationID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Evaluation", "InitialEvaluationID", c => c.Int());
            AddColumn("dbo.Cohort", "EndDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Cohort", "StartDate", c => c.DateTime(nullable: false));
            DropColumn("dbo.Evaluation", "EndDate");
            DropColumn("dbo.Evaluation", "StartDate");
            DropColumn("dbo.Evaluation", "Completed");
        }
    }
}
