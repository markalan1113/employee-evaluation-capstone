namespace EmployeeEvaluator.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class phonenumbervalidation : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Employee", "FirstName", c => c.String(nullable: false));
            AlterColumn("dbo.Employee", "LastName", c => c.String(nullable: false));
            AlterColumn("dbo.Employee", "Email", c => c.String(nullable: false));
            AlterColumn("dbo.Employee", "MailingAddress", c => c.String(nullable: false));
            AlterColumn("dbo.Employee", "Phone", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Employee", "Phone", c => c.String());
            AlterColumn("dbo.Employee", "MailingAddress", c => c.String());
            AlterColumn("dbo.Employee", "Email", c => c.String());
            AlterColumn("dbo.Employee", "LastName", c => c.String());
            AlterColumn("dbo.Employee", "FirstName", c => c.String());
        }
    }
}
