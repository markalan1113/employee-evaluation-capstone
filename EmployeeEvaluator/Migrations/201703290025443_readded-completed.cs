namespace EmployeeEvaluator.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class readdedcompleted : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Evaluation", "CompletedEvaluation", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Evaluation", "CompletedEvaluation");
        }
    }
}
