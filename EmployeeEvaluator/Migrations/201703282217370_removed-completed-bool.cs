namespace EmployeeEvaluator.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removedcompletedbool : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Evaluation", "Completed");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Evaluation", "Completed", c => c.Boolean(nullable: false));
        }
    }
}
