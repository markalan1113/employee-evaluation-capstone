namespace EmployeeEvaluator.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class cohortstable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Cohort", "Stable", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Cohort", "Stable");
        }
    }
}
