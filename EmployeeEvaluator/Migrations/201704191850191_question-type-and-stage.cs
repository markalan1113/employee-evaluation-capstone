namespace EmployeeEvaluator.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class questiontypeandstage : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Question", "EvalTypeID", c => c.Int());
            AddColumn("dbo.Question", "Stage", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Question", "Stage");
            DropColumn("dbo.Question", "EvalTypeID");
        }
    }
}
