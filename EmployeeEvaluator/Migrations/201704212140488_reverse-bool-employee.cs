namespace EmployeeEvaluator.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class reverseboolemployee : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Employee", "Active", c => c.Boolean(nullable: false));
            DropColumn("dbo.Employee", "Disabled");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Employee", "Disabled", c => c.Boolean(nullable: false));
            DropColumn("dbo.Employee", "Active");
        }
    }
}
