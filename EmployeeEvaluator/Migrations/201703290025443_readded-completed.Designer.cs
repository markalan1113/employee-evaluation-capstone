// <auto-generated />
namespace EmployeeEvaluator.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class readdedcompleted : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(readdedcompleted));
        
        string IMigrationMetadata.Id
        {
            get { return "201703290025443_readded-completed"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
