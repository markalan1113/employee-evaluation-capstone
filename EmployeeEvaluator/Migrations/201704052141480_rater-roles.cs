namespace EmployeeEvaluator.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class raterroles : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.RaterRole",
                c => new
                    {
                        RaterRoleID = c.Int(nullable: false, identity: true),
                        RoleName = c.String(),
                    })
                .PrimaryKey(t => t.RaterRoleID);
            
            AddColumn("dbo.Rater", "RaterRoleID", c => c.Int(nullable: false));
            CreateIndex("dbo.Rater", "RaterRoleID");
            AddForeignKey("dbo.Rater", "RaterRoleID", "dbo.RaterRole", "RaterRoleID", cascadeDelete: true);
            DropColumn("dbo.Rater", "Role");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Rater", "Role", c => c.String(nullable: false));
            DropForeignKey("dbo.Rater", "RaterRoleID", "dbo.RaterRole");
            DropIndex("dbo.Rater", new[] { "RaterRoleID" });
            DropColumn("dbo.Rater", "RaterRoleID");
            DropTable("dbo.RaterRole");
        }
    }
}
