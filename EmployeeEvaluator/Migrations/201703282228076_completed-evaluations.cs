namespace EmployeeEvaluator.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class completedevaluations : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Evaluation", "Employee_EmployeeID", c => c.Int());
            AddColumn("dbo.Evaluation", "Rater_RaterID", c => c.Int());
            CreateIndex("dbo.Evaluation", "Employee_EmployeeID");
            CreateIndex("dbo.Evaluation", "Rater_RaterID");
            AddForeignKey("dbo.Evaluation", "Employee_EmployeeID", "dbo.Employee", "EmployeeID");
            AddForeignKey("dbo.Evaluation", "Rater_RaterID", "dbo.Rater", "RaterID");
            DropColumn("dbo.Evaluation", "CompletedEvaluationID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Evaluation", "CompletedEvaluationID", c => c.Int());
            DropForeignKey("dbo.Evaluation", "Rater_RaterID", "dbo.Rater");
            DropForeignKey("dbo.Evaluation", "Employee_EmployeeID", "dbo.Employee");
            DropIndex("dbo.Evaluation", new[] { "Rater_RaterID" });
            DropIndex("dbo.Evaluation", new[] { "Employee_EmployeeID" });
            DropColumn("dbo.Evaluation", "Rater_RaterID");
            DropColumn("dbo.Evaluation", "Employee_EmployeeID");
        }
    }
}
