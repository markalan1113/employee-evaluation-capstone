namespace EmployeeEvaluator.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class rateremployeeid : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Evaluation", "Employee_EmployeeID", "dbo.Employee");
            DropIndex("dbo.Evaluation", new[] { "Employee_EmployeeID" });
            RenameColumn(table: "dbo.Evaluation", name: "Employee_EmployeeID", newName: "EmployeeID");
            RenameColumn(table: "dbo.Evaluation", name: "Rater_RaterID", newName: "RaterID");
            RenameIndex(table: "dbo.Evaluation", name: "IX_Rater_RaterID", newName: "IX_RaterID");
            AlterColumn("dbo.Evaluation", "EmployeeID", c => c.Int(nullable: true));
            CreateIndex("dbo.Evaluation", "EmployeeID");
            AddForeignKey("dbo.Evaluation", "EmployeeID", "dbo.Employee", "EmployeeID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Evaluation", "EmployeeID", "dbo.Employee");
            DropIndex("dbo.Evaluation", new[] { "EmployeeID" });
            AlterColumn("dbo.Evaluation", "EmployeeID", c => c.Int());
            RenameIndex(table: "dbo.Evaluation", name: "IX_RaterID", newName: "IX_Rater_RaterID");
            RenameColumn(table: "dbo.Evaluation", name: "RaterID", newName: "Rater_RaterID");
            RenameColumn(table: "dbo.Evaluation", name: "EmployeeID", newName: "Employee_EmployeeID");
            CreateIndex("dbo.Evaluation", "Employee_EmployeeID");
            AddForeignKey("dbo.Evaluation", "Employee_EmployeeID", "dbo.Employee", "EmployeeID");
        }
    }
}
