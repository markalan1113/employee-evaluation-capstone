namespace EmployeeEvaluator.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removedevaltypenavproperty : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Evaluation", "EvalTypeID", "dbo.EvalType");
            DropIndex("dbo.Evaluation", new[] { "EvalTypeID" });
        }
        
        public override void Down()
        {
            CreateIndex("dbo.Evaluation", "EvalTypeID");
            AddForeignKey("dbo.Evaluation", "EvalTypeID", "dbo.EvalType", "EvalTypeID");
        }
    }
}
