namespace EmployeeEvaluator.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class questionevalidchange : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Question", name: "Evaluation_EvaluationID", newName: "EvaluationID");
            RenameIndex(table: "dbo.Question", name: "IX_Evaluation_EvaluationID", newName: "IX_EvaluationID");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.Question", name: "IX_EvaluationID", newName: "IX_Evaluation_EvaluationID");
            RenameColumn(table: "dbo.Question", name: "EvaluationID", newName: "Evaluation_EvaluationID");
        }
    }
}
