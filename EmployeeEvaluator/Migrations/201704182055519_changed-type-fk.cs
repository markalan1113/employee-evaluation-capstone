namespace EmployeeEvaluator.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changedtypefk : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Evaluation", "TypeName", "dbo.EvalType");
            DropIndex("dbo.Evaluation", new[] { "TypeName" });
            RenameColumn(table: "dbo.Evaluation", name: "TypeName", newName: "EvalTypeID");
            DropPrimaryKey("dbo.EvalType");
            AddColumn("dbo.EvalType", "EvalTypeID", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.Evaluation", "EvalTypeID", c => c.Int(nullable: true));
            AlterColumn("dbo.EvalType", "TypeName", c => c.String());
            AddPrimaryKey("dbo.EvalType", "EvalTypeID");
            CreateIndex("dbo.Evaluation", "EvalTypeID");
            AddForeignKey("dbo.Evaluation", "EvalTypeID", "dbo.EvalType", "EvalTypeID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Evaluation", "EvalTypeID", "dbo.EvalType");
            DropIndex("dbo.Evaluation", new[] { "EvalTypeID" });
            DropPrimaryKey("dbo.EvalType");
            AlterColumn("dbo.EvalType", "TypeName", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.Evaluation", "EvalTypeID", c => c.String(maxLength: 128));
            DropColumn("dbo.EvalType", "EvalTypeID");
            AddPrimaryKey("dbo.EvalType", "TypeName");
            RenameColumn(table: "dbo.Evaluation", name: "EvalTypeID", newName: "TypeName");
            CreateIndex("dbo.Evaluation", "TypeName");
            AddForeignKey("dbo.Evaluation", "TypeName", "dbo.EvalType", "TypeName");
        }
    }
}
