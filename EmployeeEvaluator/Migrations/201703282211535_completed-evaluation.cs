namespace EmployeeEvaluator.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class completedevaluation : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Evaluation", "CompletedEvaluationID", c => c.Int(nullable: true));
            AddColumn("dbo.Answer", "Selected", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Answer", "Selected");
            DropColumn("dbo.Evaluation", "CompletedEvaluationID");
        }
    }
}
