namespace EmployeeEvaluator.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class questionevalidremove : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Question", "EvaluationID", "dbo.Evaluation");
            DropIndex("dbo.Question", new[] { "EvaluationID" });
            RenameColumn(table: "dbo.Question", name: "EvaluationID", newName: "Evaluation_EvaluationID");
            AlterColumn("dbo.Question", "Evaluation_EvaluationID", c => c.Int());
            CreateIndex("dbo.Question", "Evaluation_EvaluationID");
            AddForeignKey("dbo.Question", "Evaluation_EvaluationID", "dbo.Evaluation", "EvaluationID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Question", "Evaluation_EvaluationID", "dbo.Evaluation");
            DropIndex("dbo.Question", new[] { "Evaluation_EvaluationID" });
            AlterColumn("dbo.Question", "Evaluation_EvaluationID", c => c.Int(nullable: false));
            RenameColumn(table: "dbo.Question", name: "Evaluation_EvaluationID", newName: "EvaluationID");
            CreateIndex("dbo.Question", "EvaluationID");
            AddForeignKey("dbo.Question", "EvaluationID", "dbo.Evaluation", "EvaluationID", cascadeDelete: true);
        }
    }
}
