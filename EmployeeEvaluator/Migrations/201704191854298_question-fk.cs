namespace EmployeeEvaluator.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class questionfk : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.Question", "EvalTypeID");
            AddForeignKey("dbo.Question", "EvalTypeID", "dbo.EvalType", "EvalTypeID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Question", "EvalTypeID", "dbo.EvalType");
            DropIndex("dbo.Question", new[] { "EvalTypeID" });
        }
    }
}
