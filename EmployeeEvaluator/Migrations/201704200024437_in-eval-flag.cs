namespace EmployeeEvaluator.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class inevalflag : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Question", "InEvaluation", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Question", "InEvaluation");
        }
    }
}
