namespace EmployeeEvaluator.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class questionanswerraterchanges : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Answer", "Question_QuestionID", "dbo.Question");
            DropForeignKey("dbo.Question", "EvalTypeID", "dbo.EvalType");
            DropIndex("dbo.Answer", new[] { "Question_QuestionID" });
            DropIndex("dbo.Question", new[] { "EvalTypeID" });
            CreateTable(
                "dbo.Category",
                c => new
                    {
                        CategoryID = c.Int(nullable: false, identity: true),
                        EvalTypeID = c.Int(),
                        CategoryName = c.String(),
                        CategoryDescription = c.String(),
                    })
                .PrimaryKey(t => t.CategoryID)
                .ForeignKey("dbo.EvalType", t => t.EvalTypeID)
                .Index(t => t.EvalTypeID);
            
            CreateTable(
                "dbo.MaxRater",
                c => new
                    {
                        MaxRaterID = c.Int(nullable: false, identity: true),
                        RaterRoleID = c.Int(nullable: false),
                        Max = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.MaxRaterID)
                .ForeignKey("dbo.RaterRole", t => t.RaterRoleID, cascadeDelete: true)
                .Index(t => t.RaterRoleID);
            
            AddColumn("dbo.Answer", "Value", c => c.Int(nullable: false));
            AddColumn("dbo.Employee", "MaxSupervisors", c => c.Int(nullable: false));
            AddColumn("dbo.Employee", "MaxSupervisees", c => c.Int(nullable: false));
            AddColumn("dbo.Employee", "MaxCoworkers", c => c.Int(nullable: false));
            AddColumn("dbo.Question", "CategoryID", c => c.Int());
            AddColumn("dbo.Question", "AnswerID", c => c.Int());
            CreateIndex("dbo.Question", "CategoryID");
            CreateIndex("dbo.Question", "AnswerID");
            AddForeignKey("dbo.Question", "AnswerID", "dbo.Answer", "AnswerID");
            AddForeignKey("dbo.Question", "CategoryID", "dbo.Category", "CategoryID");
            DropColumn("dbo.Answer", "AnswerText");
            DropColumn("dbo.Answer", "Selected");
            DropColumn("dbo.Answer", "Question_QuestionID");
            DropColumn("dbo.Question", "EvalTypeID");
            DropColumn("dbo.Question", "Category");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Question", "Category", c => c.String());
            AddColumn("dbo.Question", "EvalTypeID", c => c.Int());
            AddColumn("dbo.Answer", "Question_QuestionID", c => c.Int());
            AddColumn("dbo.Answer", "Selected", c => c.Boolean(nullable: false));
            AddColumn("dbo.Answer", "AnswerText", c => c.String(nullable: false));
            DropForeignKey("dbo.MaxRater", "RaterRoleID", "dbo.RaterRole");
            DropForeignKey("dbo.Question", "CategoryID", "dbo.Category");
            DropForeignKey("dbo.Question", "AnswerID", "dbo.Answer");
            DropForeignKey("dbo.Category", "EvalTypeID", "dbo.EvalType");
            DropIndex("dbo.MaxRater", new[] { "RaterRoleID" });
            DropIndex("dbo.Question", new[] { "AnswerID" });
            DropIndex("dbo.Question", new[] { "CategoryID" });
            DropIndex("dbo.Category", new[] { "EvalTypeID" });
            DropColumn("dbo.Question", "AnswerID");
            DropColumn("dbo.Question", "CategoryID");
            DropColumn("dbo.Employee", "MaxCoworkers");
            DropColumn("dbo.Employee", "MaxSupervisees");
            DropColumn("dbo.Employee", "MaxSupervisors");
            DropColumn("dbo.Answer", "Value");
            DropTable("dbo.MaxRater");
            DropTable("dbo.Category");
            CreateIndex("dbo.Question", "EvalTypeID");
            CreateIndex("dbo.Answer", "Question_QuestionID");
            AddForeignKey("dbo.Question", "EvalTypeID", "dbo.EvalType", "EvalTypeID");
            AddForeignKey("dbo.Answer", "Question_QuestionID", "dbo.Question", "QuestionID");
        }
    }
}
