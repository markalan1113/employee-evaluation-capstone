namespace EmployeeEvaluator.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class answerlistofquestion : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Question", "AnswerID", "dbo.Answer");
            DropIndex("dbo.Question", new[] { "AnswerID" });
            AddColumn("dbo.Answer", "Question_QuestionID", c => c.Int());
            AddColumn("dbo.Question", "SelectedAnswerValue", c => c.Int());
            CreateIndex("dbo.Answer", "Question_QuestionID");
            AddForeignKey("dbo.Answer", "Question_QuestionID", "dbo.Question", "QuestionID");
            DropColumn("dbo.Question", "AnswerID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Question", "AnswerID", c => c.Int());
            DropForeignKey("dbo.Answer", "Question_QuestionID", "dbo.Question");
            DropIndex("dbo.Answer", new[] { "Question_QuestionID" });
            DropColumn("dbo.Question", "SelectedAnswerValue");
            DropColumn("dbo.Answer", "Question_QuestionID");
            CreateIndex("dbo.Question", "AnswerID");
            AddForeignKey("dbo.Question", "AnswerID", "dbo.Answer", "AnswerID");
        }
    }
}
