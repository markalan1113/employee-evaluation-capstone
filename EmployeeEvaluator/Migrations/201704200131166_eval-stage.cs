namespace EmployeeEvaluator.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class evalstage : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EvalStage",
                c => new
                    {
                        EvalStageID = c.Int(nullable: false, identity: true),
                        StageName = c.String(),
                    })
                .PrimaryKey(t => t.EvalStageID);
            
            AddColumn("dbo.Evaluation", "EvalStageID", c => c.Int());
            CreateIndex("dbo.Evaluation", "EvalTypeID");
            CreateIndex("dbo.Evaluation", "EvalStageID");
            AddForeignKey("dbo.Evaluation", "EvalStageID", "dbo.EvalStage", "EvalStageID");
            AddForeignKey("dbo.Evaluation", "EvalTypeID", "dbo.EvalType", "EvalTypeID");
            DropColumn("dbo.Evaluation", "Stage");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Evaluation", "Stage", c => c.String());
            DropForeignKey("dbo.Evaluation", "EvalTypeID", "dbo.EvalType");
            DropForeignKey("dbo.Evaluation", "EvalStageID", "dbo.EvalStage");
            DropIndex("dbo.Evaluation", new[] { "EvalStageID" });
            DropIndex("dbo.Evaluation", new[] { "EvalTypeID" });
            DropColumn("dbo.Evaluation", "EvalStageID");
            DropTable("dbo.EvalStage");
        }
    }
}
