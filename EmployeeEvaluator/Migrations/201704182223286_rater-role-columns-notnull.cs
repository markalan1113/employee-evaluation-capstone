namespace EmployeeEvaluator.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class raterrolecolumnsnotnull : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.RaterRole", "RoleName", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.RaterRole", "RoleName", c => c.String());
        }
    }
}
