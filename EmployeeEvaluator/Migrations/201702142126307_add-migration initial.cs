namespace EmployeeEvaluator.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addmigrationinitial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Evaluation",
                c => new
                    {
                        EvaluationID = c.Int(nullable: false, identity: true),
                        Type = c.String(),
                        Stage = c.String(),
                    })
                .PrimaryKey(t => t.EvaluationID);
            
            AlterColumn("dbo.Cohort", "Name", c => c.String(nullable: false));
            AlterColumn("dbo.Cohort", "Type", c => c.String(nullable: false));
            AlterColumn("dbo.Cohort", "Stage", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Cohort", "Stage", c => c.String());
            AlterColumn("dbo.Cohort", "Type", c => c.String());
            AlterColumn("dbo.Cohort", "Name", c => c.String());
            DropTable("dbo.Evaluation");
        }
    }
}
