namespace EmployeeEvaluator.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addanswer : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Answer",
                c => new
                    {
                        AnswerID = c.Int(nullable: false, identity: true),
                        AnswerText = c.String(nullable: false),
                        Question_QuestionID = c.Int(),
                    })
                .PrimaryKey(t => t.AnswerID)
                .ForeignKey("dbo.Question", t => t.Question_QuestionID)
                .Index(t => t.Question_QuestionID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Answer", "Question_QuestionID", "dbo.Question");
            DropIndex("dbo.Answer", new[] { "Question_QuestionID" });
            DropTable("dbo.Answer");
        }
    }
}
