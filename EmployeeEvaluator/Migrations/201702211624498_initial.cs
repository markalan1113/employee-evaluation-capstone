namespace EmployeeEvaluator.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Admin",
                c => new
                    {
                        AdminID = c.Int(nullable: false, identity: true),
                        Email = c.String(),
                        HashedPassword = c.String(),
                    })
                .PrimaryKey(t => t.AdminID);
            
            CreateTable(
                "dbo.Cohort",
                c => new
                    {
                        CohortID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Type = c.String(nullable: false),
                        Stage = c.String(nullable: false),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.CohortID);
            
            CreateTable(
                "dbo.Employee",
                c => new
                    {
                        EmployeeID = c.Int(nullable: false, identity: true),
                        Email = c.String(),
                        HashedPassword = c.String(),
                        FirstName = c.String(),
                        LastName = c.String(),
                        MailingAddress = c.String(),
                        Phone = c.String(),
                        CohortID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.EmployeeID)
                .ForeignKey("dbo.Cohort", t => t.CohortID, cascadeDelete: true)
                .Index(t => t.CohortID);
            
            CreateTable(
                "dbo.Evaluation",
                c => new
                    {
                        EvaluationID = c.Int(nullable: false, identity: true),
                        Type = c.String(),
                        Stage = c.String(),
                    })
                .PrimaryKey(t => t.EvaluationID);
            
        }
        
        public override void Down()
        {
        }
    }
}
