namespace EmployeeEvaluator.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removedanswertable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Answer", "Question_QuestionID", "dbo.Question");
            DropIndex("dbo.Answer", new[] { "Question_QuestionID" });
            DropTable("dbo.Answer");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Answer",
                c => new
                    {
                        AnswerID = c.Int(nullable: false, identity: true),
                        Value = c.Int(nullable: false),
                        Question_QuestionID = c.Int(),
                    })
                .PrimaryKey(t => t.AnswerID);
            
            CreateIndex("dbo.Answer", "Question_QuestionID");
            AddForeignKey("dbo.Answer", "Question_QuestionID", "dbo.Question", "QuestionID");
        }
    }
}
