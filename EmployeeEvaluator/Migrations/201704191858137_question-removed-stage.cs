namespace EmployeeEvaluator.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class questionremovedstage : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Question", "Stage");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Question", "Stage", c => c.String());
        }
    }
}
