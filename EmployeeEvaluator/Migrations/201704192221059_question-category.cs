namespace EmployeeEvaluator.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class questioncategory : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Question", "Category", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Question", "Category");
        }
    }
}
