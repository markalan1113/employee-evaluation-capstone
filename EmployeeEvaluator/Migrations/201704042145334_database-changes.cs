namespace EmployeeEvaluator.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class databasechanges : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Evaluation", "CohortID", c => c.Int(nullable: false));
            AddColumn("dbo.Evaluation", "Completed", c => c.Boolean(nullable: false));
            DropColumn("dbo.Cohort", "Type");
            DropColumn("dbo.Cohort", "Stage");
            DropColumn("dbo.Evaluation", "CompletedEvaluation");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Evaluation", "CompletedEvaluation", c => c.Int());
            AddColumn("dbo.Cohort", "Stage", c => c.String(nullable: false));
            AddColumn("dbo.Cohort", "Type", c => c.String(nullable: false));
            DropColumn("dbo.Evaluation", "Completed");
            DropColumn("dbo.Evaluation", "CohortID");
        }
    }
}
