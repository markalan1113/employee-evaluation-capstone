namespace EmployeeEvaluator.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Evaluation", "EvalTypeID", "dbo.EvalType");
            DropIndex("dbo.Evaluation", new[] { "EvalTypeID" });
            AlterColumn("dbo.Evaluation", "EvalTypeID", c => c.Int());
            CreateIndex("dbo.Evaluation", "EvalTypeID");
            AddForeignKey("dbo.Evaluation", "EvalTypeID", "dbo.EvalType", "EvalTypeID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Evaluation", "EvalTypeID", "dbo.EvalType");
            DropIndex("dbo.Evaluation", new[] { "EvalTypeID" });
            AlterColumn("dbo.Evaluation", "EvalTypeID", c => c.Int(nullable: false));
            CreateIndex("dbo.Evaluation", "EvalTypeID");
            AddForeignKey("dbo.Evaluation", "EvalTypeID", "dbo.EvalType", "EvalTypeID", cascadeDelete: true);
        }
    }
}
