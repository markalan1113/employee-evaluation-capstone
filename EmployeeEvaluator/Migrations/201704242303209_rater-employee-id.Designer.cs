// <auto-generated />
namespace EmployeeEvaluator.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class rateremployeeid : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(rateremployeeid));
        
        string IMigrationMetadata.Id
        {
            get { return "201704242303209_rater-employee-id"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
