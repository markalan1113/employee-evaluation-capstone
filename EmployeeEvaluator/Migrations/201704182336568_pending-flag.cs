namespace EmployeeEvaluator.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class pendingflag : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Evaluation", "Pending", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Evaluation", "Pending");
        }
    }
}
